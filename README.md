# Purpose
This repository exists as a collective towards upskilling as much as possible, in both Java 
and the Spring framework. Other frameworks and languages may follow. The purpose of the repository is
to collect knowledge about everything the Java language and Spring framework has to offer. In this
way, I intend to learn more about the language, such as small caveats or best practices that I didn't 
already know, and upskill on sections of the language that I don't use that often in my day-to-day experiences.

Most of the examples and information comes from the [Baeldung website](https://www.baeldung.com/)
The rest comes from [Oracle Java tutorials](https://docs.oracle.com/javase/tutorial/). 

## GOF Design Patterns
#### Creational
* [Abstract Factory](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/creational/abstractfactory/)
* [Builder](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/creational/builder/)
* [Factory Method](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/creational/factorymethod/)
* [Prototype](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/creational/prototype/)
* [Singleton](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/creational/singleton/)

#### Behavioural
* [Chain of Responsibility](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/chainofresponsibility/)
* [Command](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/command/)
* [Interpreter](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/interpreter/)
* [Iterator](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/iterator/)
* [Mediator](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/mediator/)
* [Memento](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/memento/)
* [Observer](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/observer/)
* [State](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/state/)
* [Strategy](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/strategy/)
* [Template](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/template/)
* [Visitor](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/behavioural/visitor/)

#### Structural
* [Adapter](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/adapter/)
* [Bridge](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/bridge/)
* [Composite](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/composite/)
* [Decorator](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/decorator/)
* [Facade](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/facade/)
* [Flyweight](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/flyweight/)
* [Proxy](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/gofdesignpatterns/structural/proxy/)


## Java
### OO Principles
- [Inheritance](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/ooprinciples/inheritance/)
- [Abstraction](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/ooprinciples/abstraction/)
- [Polymorphism](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/ooprinciples/polymorphism/)
- [Encapsulation](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/ooprinciples/encapsulation/)
- [SOLID](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/ooprinciples/solid/)

### Oracle tutorials
#### Collections
- Intro
- Interfaces
- Aggregate operations
- Implementations
- Algorithms
- Custom implementations
- Interoperability

#### Date-time APIs
- DateTime

#### Specialised trails
- Generics
- JavaBeans
- JMX
- Security

### Back to basics (https://www.baeldung.com/get-started-with-java-series)
#### Language Basics
- [Syntax](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/syntax/)
- [Primitives](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/primitives/)
- [The main() method](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/mainmethod/)
- [Control structures](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/controlstructures/)
- [Java Loops](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/loops/)
- [Java Packages](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/packages/)
- [Pass by value as a parameter passing mechanism](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/languagebasics/parameterpassing/)

#### Java OOP
- [Classes and Objects](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/oop/classesobjects/)
- [Concrete classes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/oop/concreteclasses/)
- [Access modifiers](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/oop/accessmodifiers/)
- [Constructors](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/oop/constructors/)
- Creating objects
- Abstract classes
- Interfaces
- A guide to inheritance
- Inheritance and composition
- The *this* keyword
- The *super* keyword
- Method overloading and overriding
- The *static* keyword
- Enums
- The *final* keyword

#### Java Strings
- Strings in Java
- Why are String immutable
- Comparing Strings
- Java String conversions
- The *toString()* method

#### Java Exceptions
- Exception handling in Java
- Checked and unchecked exceptions
- Create a custom exception
- Chained exceptions
- Difference between throw and throws
- [Try with resources](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/basics/exceptions/trywithresources/)

#### Other
- [The synchronized keyword](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/synchronised/)
- Java 9 Modularity
- Class loaders
- Java assertions
- Inheritance and composition
- [The volatile keyword](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/volatilekeyword/)
- Wrapper classes
- java.lang.System
- Object type casting
- The finalize method
- Concrete class
- Control structures
- Comparison with lambdas
- Random long, float, integer and double
- Java timer
- Round decimal numbers
- Guide to UUID
- Comparing getPath(), getCanonicalPath() and getAbsolutePath()
- ClassNotFoundException vs NoClassDefFoundError
- Iterating over Enums
- The StackOverflowError
- Converting a stacktrace to a String
- Period and duration in Java
- A regex for matching Date Patterns
- The final keyword
- Exception handling
- Creating a custom exception
- Immutable objects
- Why is String immutable
- The Modulo operator
- The switch statement
- The toString method
- Anonymous classes
- Convert time to milliseconds
- Using the hashcode

### Java 8
- [New features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java8/newfeatures/)
- [Functional interfaces](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java8/functional)
- Streams
- [Lambdas](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java8/lambda_best_practice/)

### Java 9
 - [HttpClient](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java9/httpclient/)
 - [imageAPI](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java9/imageAPI/)
 - [jshell](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java9/jshell)
 - [Language Modifications](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java9/languagemods/)
 - [New APIs](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java9/newapis/)

### Java 10
- [Container Awareness](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java10/containerawareness/)
- [Deprecations and Removals](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java10/deprecationsremovals/)
- [Optional Changes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java10/optionalchanges/)
- [Unmodifiable Collections](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java10/unmodcollection/)
- [var](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java10/var/)

### Java 11
- [Deprecations](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java11/deprecations/)
- [Developer Features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java11/developerfeatures/)
- [Performance Changes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java11/performance/)

### Java 12
- [JVM Changes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java12/jvmchanges/)
- [Language Changes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java12/languagechanges/)
- [Preview Features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java12/previewfeatures/)

### Java 13
- [Miscellaneous changes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java13/miscchanges/)
- [Preview Features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java13/previewfeatures/)
- [ZGC](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java13/zgc/)

### Java 14
- [Deprecated and Removed Features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java14/deprecatedfeatures/)
- [Features from previous versions](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java14/featuresfromprevious/)
- [JVM Features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java14/jvmfeatures/)
- [New Features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java14/newfeatures/)
- [New preview features](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java14/previewfeatures/)

### Java 15
- [Records](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java15/records/)
- [Sealed Classes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java15/sealedclasses/)
- [Pattern matching type checks](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java15/typechecks/)
- [Garbage collectors](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java15/garbagecollectors/)
- [Other changes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java15/otherchanges/)

### Java 16
- [Day Period Support](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java16/dayperiods/)
- [Stream.toList method](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java16/streamtolist/)
- [Records](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java16/records/)
- [Pattern matching for instanceof](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java16/patternmatching/)
- [Sealed classes](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/java16/sealedclasses/)

### Collections
- The Collection interface

#### Lists
- [The List interface](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/listinterface/)
- [The LinkedList](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/linkedlist/)
- [The ArrayList](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/arraylist/)
- Immutable ArrayList
- A guide to CopyOnWriteArrayList
- Multidimensional ArrayLists
- Converting an Iterator to a List
- Get a random item/element from a List
- Partition a List
- Remove all nulls from a List
- Remove all duplicates from a List
- Check if two Lists are equal
- How to find an element in a List
- The UnsupportedOperationException
- Copy a List to another List
- Remove all occurences of a certain value from a List
- Add multiple items to an ArrayList
- Remove the first element from a List
- Ways to iterate over a List
- Intersection of two Lists

#### Sets
- [The TreeSet](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/treeset/)
- [The HashSet](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/hashset/)
- [The Set interface](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/setinterface/)
- [The SortedSet interface](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/sortedsetinterface/)

#### Maps
- The Map interface
- The SortedMap interface
- [The HashMap](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/collections/hashmap/)
- The TreeMap
- TreeMap vs HashMap
- The WeakHashMap
- The ConcurrentMap
- The ConcurrentSkipListMap
- The HashTable
- The LinkedHashMap
- The EnumMap
- Immutable Map implementations
- How to store duplicate keys in a Map
- Initialise a HashMap
- Merging two Maps in Java 8
- Sorting a HashMap
- Comparing two HashMaps

#### Queues
- The Queue interface
- The Deque interface
- The PriorityBlockingQueue
- The BlockingQueue
- The SynchronousQueue
- The TransferQueue
- The DelayQueue
- The ArrayQueue

#### Converting collections
- The difference between Collection.stream().forEach and Collection.forEach()
- Sorting
- Shuffling collections
- Flattening nested collections
- Zipping collections
- Join and split arrays and collections
- Combine multiple collections
- Finding a min or max in a list or collection
- Collect a Java stream into an immutable Collection
- Java 9 convenience factory methods for Collections
- The Iterator
- Getting the size of an Iterator
- Removing elements from a Collection

#### Apache Commons collections
- The Bag
- SetUtils
- The OrderedMap
- The BidiMap
- CollectionUtils 
- MapUtils
- The CircularFifoQueue

#### Guava collections
- The cookbook
- The Multimap
- Join and split collections
- The Table

### Concurrency
#### Basics
- [Overview of the concurrency package](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/overview/)
- [Difference between wait() and sleep()](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/waitsleep/)
- [A guide to Future](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/futures/)
- [Intro to ThreadLocal](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/threadlocal/)
- [Life cycle of a thread](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/threadlifecycle/)
- [How to kill a thread](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/stopping/)
- [Introduction to ThreadPools](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/concurrency/basic/threadpools/)
- Implementing a Runnable vs extending a Thread
- The wait() and notify() methods
- Runnable vs Callable
- The Thread.join() method
- Atomic variables

#### Advanced
- Daemon Threads
- The Java ExecutorService
- Guide to the Fork/Join framework
- Custom Thread pools in Java 8 parallel streams
- Guide to CountDownLatch
- Guide to Locks
- The ExecutorService and waiting for Threads to finish
- Guide to the Java Phaser
- Guide to CompletableFuture
- The CyclicBarrier
- Guide to ThreadLocalRandom
- CyclicBarrier vs CountDownLatch
- What is thread-safety and how do we achieve it
- How to delay code execution

### Streams
#### Basics
- [The Java 8 Stream tutorial](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/streams/basics/apitutorial/)
- [Intro to streams](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/streams/basics/intro/)
- [findFirst() vs findAny()](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/streams/basics/findfirstfindany/)

#### Collectors
- [Guide to Collectors](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/streams/collectors/collectorsguide/)
- [Guide to the groupingBy Collector](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/streams/collectors/groupingby/)
- New stream Collectors in Java 9
- Collect a Java stream to an immutable Collection
- the toMap Collector

#### Operations with streams
- How to break from a stream forEach
- Filtering a stream of Optionals
- Merging streams
- [The difference between map() and flatMap()](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/streams/operations/mapvsflatmap)
- String operations in streams
- How to iterate over a stream with indices
- Iterable to stream
- How to get the last element in a stream
- Converting a string to stream of chars
- The "Stream has already been operated upon or closed" exception
- Java 8 and infinite streams
- How to add a single element to a stream
- Primitive type streams
- DistinctBy in the Stream API
- Java 9 stream improvements
- Introduction to the Spliterator
- How to use if/else logic in streams
- The Java 8 Predicate chain
- Stream filter with lambda expressions
- Summing numbers with streams
- The peek() method

### I/O
- Reading a large file efficiently
- Rename or move a file
- [Create a file](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/io/createfile/)
- Delete a file
- Write to file
- Read from file
- Directory size
- How to read a file
- Getting a file's mime type
- How to get the extension of a file
- Guide to OutputStream
- Guide to BufferedReader
- Conversions
- Introduction to the NIO2 File API
- Java NIO2 Path API
- Introduction to the Java NIO Selector

### Advanced Java
- Working with network interfaces
- Convert Hex to ASCII
- A guide to UDP
- How to get the name of the method being executed
- Changing annotation parameters at runtime
- How to all get all dates between two dates
- Guide to escaping characters in RegEx
- Introduction to Serialization
- Call methods at runtime using reflection
- Dynamic proxies
- A guide to the Java Clock class
- A guide to the ResourceBundle
- Weak references in Java
- Sending emails with Java
- Practical Java examples of the Big O notation
- A guide to Java instrumentation
- Stack memory and heap space
- [Setting the Java version in Maven](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/java/advanced/javaversionmaven/)
- Hashing a password
- Different ways of capturing Java heap dumps
- How to replace many if statements
- Understanding memory leaks
- The equals() and hashCode() contracts
- Generic constructors
- Optional as a return type

## Spring
### Basics
- [Annotations](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/basics/annotations/)
- [Profiles](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/basics/profiles/)
- [Properties](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/basics/properties/)
- [Project configuration](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/basics/projectconfig/)
- [Spring exceptions](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/basics/exceptions/)
- Spring qualifier
- Why Spring?
- getBean()
- What is a Spring Bean?
- Component vs Repository and a Service 
- Core annotations
- Using Spring Value with defaults
- Quick guide to Spring Value
- Introduction to inversion of control and dependency injection
- Constructor dependency injection
- Wiring in Spring
- Spring Autowired
- Spring Bean Scopes

### MVC
- Web annotations
- RequestMapping
- The RequestParam annotations
- Controller and RestController annotations
- RequestBody and ResponseBody
- The MVC tutorial
- The ModelAttribute annotation
- The ViewResolver
- Getting started with forms
- Thymeleaf
- Model, ModelMap and ModelView
- Bootstrapping a web application
- Building a REST application
- Securing a REST application
- Basic and Digest authentication
- REST pagination
- Error handling for REST
- Entity to DTO conversion
- RequestBody and ResponseBody annotations

### Security
#### Core
- [Spring Security with Maven](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/security/withmaven/)
- Retrieve user information
- Authentication provider
- Expressions - hasRole
- Unable to locate Spring NamespaceHandler for XML schema namespace
- No bean named ‘springSecurityFilterChain’ is defined
- security: none, filters none, access permitAll
- Session management
- Spring security LDAP
- Manually authenticate a user
- Extra login fields
- Spring method security
- Security autoconfiguration
- Default password encoder
- Custom AuthenticationFailureHandler
- Find registered filters
- Taglibs
- The Registration Process With Spring Security

#### OAuth2
- WebClient and OAuth2 support
- JWS and JWK in OAuth2
- REST + OAuth2 + Angular
- JWT with OAuth2
- Simple single sign-on
- Testing an OAuth secured API with Spring MVC
- Simple Token revocation

#### with REST
- Fixing 401s with CORS
- Securing a REST service
- Basic authentication
- Digest Authentication
- RestTemplate - Basic authentication
- RestTemplate - Digest authentication

### Spring persistence
#### Core persistence
- Hibernate 4 with Spring
- Hibernate 5 with Spring
- JPA with Spring
- Persistence with Spring Data JPA
- Spring Data Java 8 support
- Spring Data annotations
- Spring Boot with H2 database

#### The DAO
- Mapping entity classes
- The DAO with Spring and Hibernate
- The DAO with Spring and JPA
- Simplify the DAO with Generics
- LIKE queries
- Derived query methods
- Delete and relationships
- JPA and null
- Query parameters usage

#### Advanced persistence
- Disable Spring autoconfig
- DynamicUpdate
- Reactional relational database connectivity
- JDBC authentication
- H2
- Like queries
- Transactions with Spring and JPA
- Hibernate mapping exception: Unknown entity
- Sorting with JPA
- Sorting with Hibernate
- JPA Pagination
- Hibernate Pagination
- Externalise setup data with CSV files
- Auditing
- JPA attribute converters
- Multitenancy with Hibernate 5
- Spring Data JPA query
- Query entities by dates and times
- Limiting query results
- Pagination and sorting
- Configure a database programmatically
- Named entity graphs

#### MongoDB
- Introduction
- Queries in MongoDB
- Indices, annotations and converters
- Custom cascading
- Reactive repositories
- Integration testing
- Transactions

### Spring Email
- Spring email

### Spring Exception Handling
- [Exception Handling](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/spring/exceptionhandling/)

### Random Tech
- [Feign](https://bitbucket.org/gtraas/upskilling/src/master/src/main/java/com/bluesharkdev/upskill/randomtech/feign/)