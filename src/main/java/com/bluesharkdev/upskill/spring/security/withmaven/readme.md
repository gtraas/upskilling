# Spring Security with Maven
## Maven dependencies
### *spring-security-core*
- contains authentication and access control functionality
- has support for standalone (non-web) applications, method level security and JDBC
```
<properties>
    <spring-security.version>5.0.6.RELEASE</spring-security.version>
    <spring.version>5.0.6.RELEASE</spring.version>
</properties>
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-core</artifactId>
    <version>${spring-security.version}</version>
</dependency>
```
- **NB**: Spring and Spring Security are not on the release schedules
    - there isn't always a 1:1 match between them

### *spring-security-web*
- adds web support for Spring security
```
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-web</artifactId>
    <version>${spring-security.version}</version>
</dependency>
```
- contains filters and related web security infrastructure enabling URL access control in a Servlet environment

### Spring Security and older Spring Core dependencies problem
- Spring Security 3.1.x does not depend on Spring 3.1.x
- exhibits a problem for the Maven dependency graph
- older dependencies may find their way to the top of the classpath
    - this is because of how Maven resolves conflicts in versions
    - Maven picks the jar that is closest to the root of the tree
    - in our case, *spring-core* is defined by both *spring-orm* (with the 4.x.RELEASE) but also by *spring-security-core*
    (with the 3.2.8.RELEASE) 
    - thus the order in which *spring-orm* and *spring-security-core* is defined matters
    - the solution is to explicitly define the spring dependencies, and not rely on implicit Maven dependency resolution

### *spring-security-config* and Others
- to use the rich Spring Security XML namespace, the *spring-security-config* dependency is required:
```
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-config</artifactId>
    <version>${spring-security.version}</version>
    <scope>runtime</scope>
</dependency>
```
- no application code should compile against this dependency, so it should be scoped as runtime
