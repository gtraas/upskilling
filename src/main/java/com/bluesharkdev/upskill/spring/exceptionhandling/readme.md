# [Exception handling in Spring Boot](https://reflectoring.io/spring-boot-exception-handling/)
## Spring Boot's default exception handling mechanism

- let's say we have a controller with a get method that throws a custom exception when it can't find the id provided:

```java
@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;

    @GetMapping("/{id}")
    public ResponseEntity getProduct(@PathVariable String id) {
        //this method will throw a NoSuchElementFoundException
        return productService.getProduct(id);
    }
}
```

- if we call */product* with an invalid id, the service throws a *NoSuchElementFoundException* runtime exception
- we get the following response:

```json
{
  "timestamp": "2021-12-05T08:08:28.474+00:00",
  "status": 500,
  "error": "Internal Server Error",
  "path": "/product/1"
}
```

- besides the well-formed error response, the payload actually doesn't give us any useful info
- there's not even a message field at this point
- we can start by fixing the error message issue
- we can use the properties that Spring Boot provides us to customise the error response payload

```properties
server.error.include-message=always
server.error.include-binding-errors=always
server.error.include-stacktrace=on_param
server.error.include-exception=false
```

- using these properties in the *application.properties* file, we can alter and control the error response somewhat
- let's try again and see what we get

```json
{
"timestamp": "2021-12-05T08:19:28.990+00:00",
"status": 500,
"error": "Internal Server Error",
"message": "Item with id 1 not found",
"path": "/product/1"
}
```

- as you can see, we're now getting the error message
- with the property *include_stacktrace* set to on_param, if we add the trace parameter (*?trace=true*), we can see the actual stacktrace in our response
- useful for testing and staging environments, but this should be *include_stacktrace=never* in the production environment
- with the trace parameter active, we see the following in the response:

```json
{
  "timestamp":"2021-12-05T08:22:31.559+00:00",
  "status":500,
  "error":"Internal Server Error",
  "trace":"com.bluesharkdev.upskill.spring.exceptionhandling.example.NoSuchElementFoundException: Item with id 1 not found\r\n\tat com.bluesharkdev.upskill.spring.exceptionhandling.example.ProductController.getProduct(ProductController.java:16)\r\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:77)\r\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:568)\r\n\tat org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:197)\r\n\tat org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:141)\r\n\tat org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:106)\r\n\tat org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:894)\r\n\tat org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n\tat org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n\tat org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1063)\r\n\tat org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:963)\r\n\tat org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n\tat org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:626)\r\n\tat org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:733)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n\tat org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n\tat org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n\tat org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n\tat org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n\tat org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:202)\r\n\tat org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n\tat org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:542)\r\n\tat org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:143)\r\n\tat org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n\tat org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n\tat org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:357)\r\n\tat org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:374)\r\n\tat org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n\tat org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n\tat org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1707)\r\n\tat org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n\tat java.base/java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1136)\r\n\tat java.base/java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:635)\r\n\tat org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n\tat java.base/java.lang.Thread.run(Thread.java:833)\r\n",
  "message":"Item with id 1 not found",
  "path":"/product/1"
}
```

- as you can see, the *trace* output can give away important details about the internal workings of our application
- the status code is return *500* which indicates something is wrong with the server code, but in reality, the issue is on the client side, because the client has provided an incorrect product id
- this is as far as we can go with *server.error.* properties, but there are a number of ways we can improve this situation

## @ResponseStatus

- *@ResponseStatus* allows us to modify the HTTP status of our response
- can be applied in the following places
  - on the exception class itself
  - along with the *@ExceptionHandler* annotation on methods
  - along with the *@ControllerAdvice* annotation on classes
- we'll be looking at the first case only
- so the problem we have is that our error responses are giving the incorrect HTTP status
- to address this, we can simply annotate our exception class with the @ResponseStatus annotation and pass in the desired status code as a parameter

```java
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchElementFoundException extends RuntimeException {
    //....
}
```

- this changes our response to better represent the situation

```json
{
  "timestamp":"2021-12-05T09:35:51.771+00:00",
  "status":404,
  "error":"Not Found",
  "message":"Item with id 1 not found",
  "path":"/product/1"
}
```

- another way to achieve the same thing is to extend the *ResponseStatusException* class:

```java
public class NoSuchElementFoundException extends ResponseStatusException {
    public NoSuchElementFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }    
    
    @Override
    public HttpHeaders getResponseHeaders() {
        // return response headers
    }
}
```

- this approach comes in handy when we want to manipulate the response headers, because we can override the *getResponseHeaders()* method
- *@ResponseStatus* in conjunction with *server.error* allows us to manipulate almost all the fields in the Spring-defined error response payload
- if we want to manipulate the payload itself, we use:

## @ExceptionHandler

- the *@ExceptionHandler* annotation gives us a lot of flexibility in terms of handling exceptions
- to use it, we need to create a method either in the controller itself, or in the *@ControllerAdvice* class and annotate it with *@ExceptionHandler*

```java
@RestController
@RequestMapping("/product")
public class ProductController { 
    
  private final ProductService productService;
  
  //constructor omitted for brevity...

  @GetMapping("/{id}")
  public Response getProduct(@PathVariable String id) {
    return productService.getProduct(id);
  }

  @ExceptionHandler(NoSuchElementFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<String> handleNoSuchElementFoundException(NoSuchElementFoundException exception) {
    return ResponseEntity
        .status(HttpStatus.NOT_FOUND)
        .body(exception.getMessage());
  }

}
```

- the exception handler method takes an exception or list of exceptions as an argument that we want to handle in the defined method
- we annotate the method with *@ExceptionHandler* and *@ResponseStatus* to define the exception and status code we want to return
- if we don't want to use these annotations, simply defining the exception as the method parameter is sufficient

```java
import org.springframework.web.bind.annotation.ExceptionHandler;

@ExceptionHandler
public ResponseEntity<String> handleNoSuchElementFoundException(NoSuchElementFoundException exception) {
    
}
```

- for readability purposes, we should keep the exception class in the annotation
- the *@ResponseStatus* on the handler method is also not required as the HTTP status passed in to the ResponseEntity will take precedence
- again, for readability purposes, we should keep it there
- apart from the exception parameter, we can also have *HttpServletRequest*, *WebRequest* and *HttpSession* types as parameters
- the handler methods can also return a variety of types, including *ResponseEntity*, *String* and even *void*
- we are in complete control of the error payload
- to demonstrate, let's finalise our application
- in case of an error, a client expects 2 things:
  - an error code that tells a client what kind of error has occurred. These can be HttpStatus codes, or internal API defined ones (eg, *E001*)
  - an additional human-readable message that gives more information on the error and even possibly hints at how to fix the problem
- we'll add an optional *stackTrace* field which will help us debug in the dev environments
- we also want to handle validation errors in the response 
- this will be our ErrorResponse object:

```java
@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private final int status;
    private final String message;
    private String stackTrace;
    private List<ValidationError> errors;

    @Getter
    @Setter
    @RequiredArgsConstructor
    private static class ValidationError {
        private final String field;
        private final String message;
    }

    public void addValidationError(String field, String message) {
        if (Objects.isNull(errors)) {
            errors = new ArrayList<>();
        }
        errors.add(new ValidationError(field, message));
    }
}
```

- and to apply this to our ProductController:

```java
@Slf4j
@RestController
@RequestMapping("/product")
public class ProductController {

    private static final String TRACE = "trace";

    @Value("{error.exception.handling.trace:false")
    private boolean printStackTrace;

    @GetMapping("/{id}")
    public ResponseEntity getProduct(@PathVariable("id") Integer id) {
        if (id != 3) {
            throw new NoSuchElementFoundException("Item with id %s not found".formatted(id));
        }
        return ResponseEntity.ok("Product found");
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleNoSuchElementFoundException(NoSuchElementFoundException e, WebRequest request) {
        log.error("Failed to find the requested element", e);
        return buildErrorResponse(e, HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValid(MethodArgumentNotValidException e, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, e, request);

        for (FieldError fieldError: e.getBindingResult().getFieldErrors()) {
            errorResponse.addValidationError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return ResponseEntity.unprocessableEntity().body(errorResponse);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorResponse> handleAllUncaughtExceptions(Exception exception, WebRequest request) {
        log.error("Unknown error occurred", exception);
        return buildErrorResponse(exception, "Unknown error occurred", HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    private ResponseEntity<ErrorResponse> buildErrorResponse(Exception exception, HttpStatus status, WebRequest request) {
        return buildErrorResponse(exception, exception.getMessage(), status, request);
    }

    private ResponseEntity<ErrorResponse> buildErrorResponse(Exception exception, String message, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(status.value(), exception.getMessage());

        if (printStackTrace && isTraceOn(request)) {
            errorResponse.setStackTrace(ExceptionUtils.getStackTrace(exception));
        }

        return ResponseEntity.status(status).body(errorResponse);
    }

    private boolean isTraceOn(WebRequest request) {
        String value[] = request.getParameterValues(TRACE);
        return Objects.nonNull(value) && value.length > 0 && value[0].contentEquals("true");
    }
}
```

### Providing a stacktrace

- providing the stacktrace in the response can save our devs and TAs from crawling through the logs
- although Spring already handles this through its default methods, we're not using the default anymore, so we need to handle it ourselves
- we've put together a double fail-safe here, in that we have a property in the *application.properties* file that will enable or disable stacktraces in the response
- additionally we have to provide the *?trace=true* parameter to our request to see the stacktrace
- one without the other will omit the stacktrace

### Catch all exceptions

- as a cautionary measure, we often surround our top-level method body with a try-catch-catchall handler to avoid any unwanted behaviour
- the *handleAllUncaughtException()* method works similarly
- it will catch all the exceptions for which we don't have a specific handler
- even if we don't have this method, Spring will still handle it
- however the response will be in the default Spring format
- if we want the response to be in our format, we must handle the exceptions ourselves
- a catch-all handler is a good place to log exceptions as they might give insights into a potential bug

### Order of exception handlers

- the order doesn't matter
- Spring looks first for the most specific exception handler method
- if it fails to find one for that exception, it will look for a handler of the parent exception, in this case the *RuntimeException*
- if it still can't be found, the handleAllUncaughtException() method will handle the exception
- the major problem here is that we would need this kind of thing for every controller
- Spring provides us with an elegant solution to this problem with *@ControllerAdvice* annotation

## @ControllerAdvice

- the term "advice" comes from Aspect-Oriented Programming (AOP) which allows us to inject cross-cutting code (or "advice") around existing methods
- a controller advice allows us to intercept and modify the return values of controller methods, in our case, handle exceptions
- controller advice allows us to apply exception handlers to more than one or all controllers in our application

```java
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  public static final String TRACE = "trace";

  @Value("${reflectoring.trace:false}")
  private boolean printStackTrace;

  @Override
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
      //Body omitted as it's similar to the method of same name
      // in ProductController example...  
      //.....
  }

  @ExceptionHandler(ItemNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<Object> handleItemNotFoundException(ItemNotFoundException itemNotFoundException, WebRequest request) {
      //Body omitted as it's similar to the method of same name
      // in ProductController example...  
      //.....  
  }

  @ExceptionHandler(RuntimeException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<Object> handleAllUncaughtException(RuntimeException exception, WebRequest request) {
      //Body omitted as it's similar to the method of same name
      // in ProductController example...  
      //.....
  }
  
  //....

  @Override
  public ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

    return buildErrorResponse(ex,status,request);
  }

}
```

- these handlers will handle exceptions thrown from **all** the controllers in the application, not just ProductController
- if we want to selectively apply or limit the scope of the controller advice, we can use the properties of the annotation
- we can pass a package name or list of package names in the annotation's *value* or *basePackages* parameter

```java
@ControllerAdvice("com.bluesharkdev.controller")
```

- we can also limit the controller advise to be valid for controllers marked with the @Advised annotation

```java
@ControllerAdvice(annotations = Advised.class)
```

### ResponseEntityExceptionHandler

- a convenient base class for controller advise classes
- provides exception handlers for internal Spring exceptions
- if we don't extend it, all Spring exceptions will be directed to *DefaultHandlerExceptionResolver* which returns a *ModelAndView* object
- we want to shape our own errors, so we want to avoid that
- we have overridden two methods
  - *handleMethodArgumentNotValid()*: we've just overridden its behaviour like when we did it in the @ExceptionHandler section
  - *handleExceptionInternal()*: all the handlers in the ResponseEntityExceptionHandler use this function to build the ResponseEntity similar to how we've done it. Since we want to include the http status in our response body, we must override this.
- handling *NoHandlerFoundException* requires some extra steps
- this exception occurs when you try to call an API that doesn't yet exist in the system
- despite us implementing its handler via ResponseEntityExceptionHandler, the exception is directed to DefaultHandlerExceptionResolver
- to redirect it to the advice, we need to add some properties to the properties file

```properties
spring.mvc.throw-exception-if-no-handler-found=true;
spring.web.resources.add-mappings=false;
```

### Some points to keep in mind with @ControllerAdvice

- to keep things simple, only have one @ControllerAdvice class in your project
- it's good to have a single repository of exceptions in the application
- if you must use multiple, make sure they are well-labelled with the *basePackages* or *annotations* property
- Spring can process controller advice classes in any order unless we have annotated it with the *@Order* annotation

## How does Spring process the Exceptions?

![Exception Handling Process](../../../../../../resources/images/spring.exceptionhandling/img.png)