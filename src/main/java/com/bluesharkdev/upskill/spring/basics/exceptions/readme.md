# Spring bean exceptions
## BeanDefinitionStoreException
- this is thrown by a BeanFactory when a bean definition is invalid, or the loading of a bean has been problematic

### Cause: java.io.FileNotFoundException
- there are multiple possible explanations that the BeanDefinitionStoreException could be caused by an IOException

#### IOException parsing the XML document from ServletContext resource
- usually happens in a Spring Web application when a *DispatcherServlet* is set up in the web.xml for Spring MVC
```
<servlet>  
   <servlet-name>mvc</servlet-name>  
   <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>  
</servlet>
```
- by default, Spring will look for a file called (exactly) *springMvcServlet-servlet.xml* in the */WEB-INF* directory
- if it doesn't exist, the following exception will be thrown:
```
org.springframework.beans.factory.BeanDefinitionStoreException: 
Ioexception Parsing Xml Document from Servletcontext Resource [/WEB-INF/mvc-servlet.xml]; 
nested exception is java.io.FileNotFoundException: 
Could not open ServletContext resource [/WEB-INF/mvc-servlet.xml]
```
- the solution is to make sure the file exists under the correct directory

#### IOException parsing the XML document from classpath resource
- usually happens when something in the application points to an XML resource that doesn't exist, or is not placed
where it should be
- for example
```
@Configuration
@ImportResource("beans.xml")
public class SpringConfig {...}
```
- if the file doesn't exist, we get this:
```
org.springframework.beans.factory.BeanDefinitionStoreException: 
Ioexception Parsing Xml Document from Servletcontext Resource [/beans.xml]; 
nested exception is java.io.FileNotFoundException: 
Could not open ServletContext resource [/beans.xml]
```
- the solution is to create the file and place it under the */src/main/resources* folder so that it is placed on the 
classpath and can be found and used by Spring

### Cause: Could not resolve placeholder
- occurs when Spring tries to resolve a property but is not able to
- for example:
```
@Value("${some.property}")
private String someProperty;
```
- the first thing to do is check that the name of the property matches the definition
- then we need to check where the properties file is defined in Spring
    - best practice is to put them under */src/main/resources/*
- lastly we need to check if there are multiple *PropertyPlaceholderConfiguration* beans in the context
    - solution is to collapse them into a single one, or configure the one in the parent context with 
    *ignoreUnresolvablePlaceholders*
    
### Cause: java.lang.NoSuchMethodError
- this error comes in many forms, but a common one is:
```
org.springframework.beans.factory.BeanDefinitionStoreException:
Unexpected exception parsing XML document from ServletContext resource [/WEB-INF/mvc-servlet.xml];
nested exception is java.lang.NoSuchMethodError:
org.springframework.beans.MutablePropertyValues.add (Ljava/lang/String;Ljava/lang/Object;)
Lorg/springframework/beans/MutablePropertyValues;
``` 
- usually happens when there are multiple versions of Spring on the classpath
- solution is to check that all the Spring jars are 3.0 or above, and that they are all the same version
- another example:
```
org.springframework.beans.factory.BeanDefinitionStoreException:
Unexpected exception parsing XML document from class path resource [/WEB-INF/mvc-servlet.xml];
- nested exception is java.lang.NoSuchMethodError:
org.springframework.util.ReflectionUtils.makeAccessible(Ljava/lang/reflect/Constructor;)V
```

### Cause: java.lang.NoClassDefFoundError
- common
```
org.springframework.beans.factory.BeanDefinitionStoreException:
Unexpected exception parsing XML document from ServletContext resource [/WEB-INF/mvc-servlet.xml];
nested exception is java.lang.NoClassDefFoundError: 
org/springframework/transaction/interceptor/TransactionInterceptor
```
- occurs when transactional functionality is configured in the XML configuration
```
<tx:annotation-driven/>
```
- the NoClassDefFoundError means that the Spring Transactional support, *spring-tx*, does not exist on the classpath
- solution is simple... include spring-tx in the Maven pom
```
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-tx</artifactId>
    <version>4.1.0.RELEASE</version>
</dependency>
```
- it's not limited to spring-tx. It could happen to any of the Spring libraries
- another example
```
Exception in thread "main" org.springframework.beans.factory.BeanDefinitionStoreException: 
Unexpected exception parsing XML document from class path resource [/WEB-INF/mvc-servlet.xml]; 
nested exception is java.lang.NoClassDefFoundError: 
org/aopalliance/aop/Advice
```
- here, spring-aop is missing, and will need to be added to the Maven pom
```
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-aop</artifactId>
    <version>4.1.0.RELEASE</version>
</dependency>
``` 

## BeanCreationException
- occurs when the BeanFactory creates beans of the bean definition and encounters a problem

### Cause: org.springframework.beans.factory.NoSuchBeanDefinitionException
- the most common cause of the BeanCreationException is when Spring is trying to inject a bean that doesn't exist
- for example, BeanA is trying to inject BeanB
```
@Component
public class BeanA {
 
    @Autowired
    private BeanB dependency;
    ...
}
```
- if a BeanB is not found in the context, then the following exception will be thrown
```
Error creating bean with name 'beanA': Injection of autowired dependencies failed; 
nested exception is org.springframework.beans.factory.BeanCreationException: 
Could not autowire field: private org.baeldung.web.BeanB org.baeldung.web.BeanA.dependency; 
nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: 
No qualifying bean of type [org.baeldung.web.BeanB] found for dependency: 
expected at least 1 bean which qualifies as autowire candidate for this dependency. 
Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
```
- to diagnose this issue, make sure the bean is declared
    - either in an XML configuration file using the <bean/> element
    - or in a Java @Configuration class via the @Bean annotation
    - or is annotated with: @Component, @Repository, @Service, @Controller
    - classpath scanning is active for that package
    
### Cause: org.springframework.beans.factory.NoUniqueBeanDefinitionException
- another cause is Spring trying to inject a bean by type, namely by its interface, and finding two or more beans 
implementing that interface in the context
- for example, BeanB1 and BeanB2 both implement the same interface:
```
@Component
public class BeanB1 implements IBeanB { ... }
@Component
public class BeanB2 implements IBeanB { ... }
 
@Component
public class BeanA {
 
    @Autowired
    private IBeanB dependency;
    ...
}
```
- this will lead to the following exception:
```
Error creating bean with name 'beanA': Injection of autowired dependencies failed; 
nested exception is org.springframework.beans.factory.BeanCreationException: 
Could not autowire field: private org.baeldung.web.IBeanB org.baeldung.web.BeanA.b; 
nested exception is org.springframework.beans.factory.NoUniqueBeanDefinitionException: 
No qualifying bean of type [org.baeldung.web.IBeanB] is defined: 
expected single matching bean but found 2: beanB1,beanB2
```

### Cause: org.springframework.beans.BeanInstantiationException
#### Custom exception
- this is a bean that throws an exception during its creation process
```
@Component
public class BeanA {
 
    public BeanA() {
        super();
        throw new NullPointerException();
    }
    ...
}
```
- obviously this will fail really fast, and the outcome is the following:
```
Error creating bean with name 'beanA' defined in file [...BeanA.class]: 
Instantiation of bean failed; nested exception is org.springframework.beans.BeanInstantiationException: 
Could not instantiate bean class [org.baeldung.web.BeanA]: 
Constructor threw exception; 
nested exception is java.lang.NullPointerException
```

#### java.lang.InstantiationException
- caused by defining an abstract class as a bean in the XML
- there is no way to do this via @Configuration, and classpath scanning will ignore the abstract class
```
@Component
public abstract class BeanA implements IBeanA { ... }
```
- the xml definition of the bean
```
<bean id="beanA" class="org.baeldung.web.BeanA" />
```
- this will result in the following exception:
```
org.springframework.beans.factory.BeanCreationException: 
Error creating bean with name 'beanA' defined in class path resource [beansInXml.xml]: 
Instantiation of bean failed; 
nested exception is org.springframework.beans.BeanInstantiationException: 
Could not instantiate bean class [org.baeldung.web.BeanA]: 
Is it an abstract class?; 
nested exception is java.lang.InstantiationException
```

#### java.lang.NoSuchMethodException
- if a bean has no default constructor and Spring tries to instantiate it by looking for that constructor, this 
will result in a runtime exception:
```
@Component
public class BeanA implements IBeanA {
 
    public BeanA(final String name) {
        super();
        System.out.println(name);
    }
}
```
- when this is picked up by the classpath scanner, the following happens:
```
Error creating bean with name 'beanA' defined in file [...BeanA.class]: Instantiation of bean failed; 
nested exception is org.springframework.beans.BeanInstantiationException: 
Could not instantiate bean class [org.baeldung.web.BeanA]: 
No default constructor found; 
nested exception is java.lang.NoSuchMethodException: org.baeldung.web.BeanA.<init>()
```
- a similar exception, more difficult to diagnose, may occur when the Spring dependencies on the classpath do not
have the same version
- this may result in a *NoSuchMethodException* because of API changes

### Cause: org.springframework.beans.NotWritablePropertyException
- another possibility is defining a bean, BeanA, with reference to another bean, BeanB, without having the corresponding
setter method
```
@Component
public class BeanA {
    private IBeanB dependency;
    ...
}
@Component
public class BeanB implements IBeanB { ... }
```

## NoSuchBeanDefinitionException
## Unsatisfied dependency in Spring

#Spring Data exceptions
## DataIntegrityViolationException
## NonTransientDataAccessException

# Spring Security exceptions
## No bean named 'springSecurityFilterChain' is defined