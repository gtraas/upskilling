# Spring properties
## Register a properties file via Java annotations
- the @PropertySource annotation is a convenient mechanism for adding property sources to the environment
- can be used in conjunction with Java-based configuration and the @Configuration annotation
```
@Configuration
@PropertySource("classpath:foo.properties")
public class PropertiesWithJavaConfig {
    //...
}
```
- another useful way of registering a new properties file is using a placeholder to dynamically select the right file
at runtime
```
@PropertySource({"classpath:persistence-${envTarget:mysql}.properties"})
```

### Defining multiple property locations
- in accordance with Java 8 standards, we can list multiple *@PropertySource* annotations
```
@PropertySource("classpath:foo.properties")
@PropertySource("classpath:bar.properties")
public class PropertiesWithJavaConfig {
    //...
}
```
- for Java versions below 8, we can use *@PropertySources*
```
@PropertySources({
    @PropertySource("classpath:foo.properties"),
    @PropertySource("classpath:bar.properties")
})
public class PropertiesWithJavaConfig {
    //...
}
```
- in either case, if we have a property name collision, the last read will have precedence.

## Register a properties file in XML
- new properties can be made accessible via the *<context:property-placeholder>* namespace element
```
<context:property-placeholder location="classpath:foo.properties" />
```
- *foo.properties* should be placed under */src/main/resources* so that it is available at runtime
- in the case of multiple <property-placeholder> elements, there are some recommmended best practices
    - the *order* attribute needs to be specified to fix the order in which these are processed
    - all property placeholders minus the last one should have the *ignore-resolvable="true"* to allow the resolution 
    mechanism to pass to others in the context without throwing an exception

### Registering multiple property files in XML
- we can define multiple property files with xml as such:
```
<context:property-placeholder location="classpath:foo.properties, classpath:bar.properties"/>
```
- as with *@PropertySource*, if there is a property name collision, the last source read takes precedence

## Using or injecting properties
- Injecting a property with *@Value* is simple:
```
@Value( "${jdbc.url}" )
private String jdbcUrl;
```
- a default value can also be specified:
```
@Value( "${jdbc.url:aDefaultUrl}" )
private String jdbcUrl;
```
- In XML, it looks like this:
```
<bean id="dataSource">
  <property name="url" value="${jdbc.url}" />
</bean>
```
- we can also obtain the property value via the *Environment* API:
```
@Autowired
private Environment env;
...
dataSource.setUrl(env.getProperty("jdbc.url"));
```
- importantly, using the *<property-placeholder>* will not expose the properties to the Spring *Environment*, and will
result in the *dataSource.url* above being null

## Properties with Spring Boot
- involves less configuration than standard Spring

### *application.properties* - the default property file
- Boot applies its typical convention over configuration approach with properties
- if we put an *application.properties* file in */src/main/resources*, Boot will auto-detect it
- can then inject property values as needed
- with this approach, we don't need to use *@PropertySource* or even provide a path to the properties file
- can also configure a different file at runtime, if needed, as follows:
```
java -jar app.jar --spring.config.location=classpath:/another-location.properties
```

### Environment specific properties files
- there's a built-in mechanism for specifying different environments in Spring Boot
- simply define *application-environment.properties* file under */src/main/resources*, and then set a Spring profile
with the same environment name
- if we define a *staging* environment, we'll have to define a *staging* profile and, of course, an 
*application-staging.properties* file
- this env file will be loaded and take precedence over the default property file. The default file will still be 
loaded, just that if there are property name collisions, the environment-specific property takes precedence

### Test specific properties file
- we may want to use different properties for our tests
- Boot handles this by looking under */src/test/resources* for the property file
- the default file is still injected, but name collisions defer to the test property file

### The *@TestPropertySource* annotation
- if we need more granular control over test properties, we use the *@TestPropertySource* annotation
- allows us to set test properties for a specific test context, taking precedence over the default property sources
```
@RunWith(SpringRunner.class)
@TestPropertySource("/foo.properties")
public class FilePropertyInjectionUnitTest {
 
    @Value("${foo}")
    private String foo;
 
    @Test
    public void whenFilePropertyProvided_thenProperlyInjected() {
        assertThat(foo).isEqualTo("bar");
    }
}
```
- if we don't want to use a file, we can specify names and values directly:
```
@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"foo=bar"})
public class PropertyInjectionUnitTest {
 
    @Value("${foo}")
    private String foo;
 
    @Test
    public void whenPropertyProvided_thenProperlyInjected() {
        assertThat(foo).isEqualTo("bar");
    }
}
```
- can achieve a similar effect using the *properties* argument of the *@SpringBootTest* annotation
```
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"foo=bar"}, classes = SpringBootPropertiesTestApplication.class)
public class SpringBootPropertyInjectionIntegrationTest {
 
    @Value("${foo}")
    private String foo;
 
    @Test
    public void whenSpringBootPropertyProvided_thenProperlyInjected() {
        assertThat(foo).isEqualTo("bar");
    }
}
```

### Hierarchical properties
- if we have properties grouped together, we can make use of the *@ConfigurationProperties* annotation which will
map these property hierarchies into Java objects graphs
- let's take some properties used to configure a database connection:
```
database.url=jdbc:postgresql:/localhost:5432/instance
database.username=foo
database.password=bar
```
- let's use the annotation to map them to a database object:
```
@ConfigurationProperties(prefix = "database")
public class Database {
    String url;
    String username;
    String password;
 
    // standard getters and setters
}
```
- Boot applies its convention over configuration approach again, and automatically maps property names to their 
corresponding fields
- we only need to supply the prefix

### An alternative approach - YAML files
- YAML files are also supported
- the only difference is the file extension and that the *SnakeYAML* library needs to be on the classpath
- YAML is particularly good for hierarchical property storage
- a standard properties files:
```
database.url=jdbc:postgresql:/localhost:5432/instance
database.username=foo
database.password=bar
secret: foo
```
- the YAML equivalent
```
database:
  url: jdbc:postgresql:/localhost:5432/instance
  username: foo
  password: bar
secret: foo
```
- YAML files do not support the *@PropertySource* annotation, so if the use of the annotation was required, it 
would constrain us to using a properties file

### Properties from the command line
- properties can also be passed directly on the command line
```
java -jar app.jar --property="value"
```
- can also do this via system properties which are provided before th *-jar* rather than after it:
```
java -Dproperty.name="value" -jar app.jar
``` 

### Properties from environment variables
- Boot can also detect environment variables
```
export name=value
java -jar app.jar
```

### Randomization of property values
- if we don't want deterinistic property values, *RandomValuePropertySource* can be used to randomize the values
 of the properties
```
random.number=${random.int}
random.long=${random.long}
random.uuid=${random.uuid}
```

## Properties in parent-child contexts
- what happens when your web application has a parent and a child context?
- parent context may have some common core functionality and beans, and the child context may contain servlet 
specific beans
- what's the best way to define the properties files and include them in these contexts?
- how do we best retrieve these properties in Spring?

### If the properties file is defined in XML with *<property-placeholder>*
- If the file is defined in the Parent context:
    - @Value works in Child context: NO
    - @Value works in Parent context: YES
- If the file is defined in the Child context:
    - @Value works in the Child context: YES
    - @Value works in the Parent context: NO
- <property-placeholder> does not expose the properties to the environment, so:
    - environment.getProperty does not work
    
### If the property file is defined in Java with @PropertySource
- If a file is defined in the Parent Context:
    - @Value works in Child context: YES
    - @Value works in Parent context: YES
    - environment.getProperty in Child context: YES
    - environment.getProperty in Parent context: YES
- If a file is defined in the Child Context:
    - @Value works in Child context: YES
    - @Value works in Parent context: NO
    - environment.getProperty in Child context: YES
    - environment.getProperty in Parent context: NO