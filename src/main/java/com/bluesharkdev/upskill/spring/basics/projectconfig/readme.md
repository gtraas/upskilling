# How to set up a project with multiple configurations
## Environment specific configuration
- config should be environment specific
- for this, we're looking at a different database setup for each environment

## The *.properties* files for each environment
- we have 3 environments: dev, staging, production
- thus we need 3 properties files:
    - persistence-dev.properties
    - persistence-staging.properties
    - persistence-production.properties
- usually, these files will be in the */src/main/resources* folder, but wherever they reside, they need to be on 
the classpath
- it's important to have your properties under version control
    - makes configuration more transparent and reproducible

## Spring configuration
- we include the correct file based on environment:
- in XML:
```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xmlns:context="http://www.springframework.org/schema/context"
   xsi:schemaLocation="http://www.springframework.org/schema/beans
      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
      http://www.springframework.org/schema/context
      http://www.springframework.org/schema/context/spring-context-4.0.xsd">
 
      <context:property-placeholder
         location="
         classpath*:*persistence-${envTarget}.properties" />
 
</beans>
```
- in Java
```
@PropertySource({ "classpath:persistence-${envTarget:dev}.properties" })
```
- allows us to have the flexibility of multiple .properties files for specific, focused purposes
- in this case, the persistence Spring config imports persistence config. Likewise, the security config
would import environment-specific security configuration

## Setting the property in each environment
- the final, deployable war will have all properties files
    - in this case, all three persistence properties files are included
- we set the *envTarget*, and thus target the config we want
- this can be set in the OS/environment or as a parameter to the JVM command line
```
-DenvTarget=dev
```

## Testing and Maven
- for integration tests that need persistence enabled, we can just set the *envTarget* property in the pom.xml
```
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-surefire-plugin</artifactId>
   <configuration>
      <systemPropertyVariables>
         <envTarget>h2_test</envTarget>
      </systemPropertyVariables>
   </configuration>
</plugin>
```
- a corresponding *persistence-h2_test.properties* file can be placed in */src/test/resources* so that it **will
only be used for testing**
- it won't be included and deployed in the war at runtime

## Going further
- several ways to build additional flexibility into this solution
- can use more complex encoding for the names
    - i.e. we can specify more information, such as the persistence provider
    - might use one of the following:
        - *persistence-h2.properties* and *persistence-mysql.properties*
        - *persistence-dev_h2.properties*, *persistence-staging_mysql.properties* and 
        *persistence-production_amazonRDS.properties*
    - the advantage of this naming convention is simple transparency
    