# Spring bean annotations
## Component scanning
- if component scanning is enabled, Spring can automatically scan a package for components
- @ComponentScan configures which packages to scan for classes with annotation configurations
- can specify the base packages directly with one of the *value* or *basePackages* arguments
```
@Configuration
@ComponentScan(basePackages = "com.baeldung.annotations")
class VehicleFactoryConfig {}
```
- can also point to classes in the base packages with the *basePackageClasses* argument
```
@Configuration
@ComponentScan(basePackageClasses = VehicleFactoryConfig.class)
class VehicleFactoryConfig {}
```
- both arguments are arrays, so we can provide multiple packages
- if no argument is presented, scanning from the package where the *@ComponentScan* annotated class is present
- *@ComponentScan* leverages Java 8's repeating annotation feature, whic means we can mark a class with multiple
annotations of the same type
```
@Configuration
@ComponentScan(basePackages = "com.baeldung.annotations")
@ComponentScan(basePackageClasses = VehicleFactoryConfig.class)
class VehicleFactoryConfig {}
```
- we could also use *@ComponentScans* to specify multiple *@ComponentScan* configs
```
@Configuration
@ComponentScans({ 
  @ComponentScan(basePackages = "com.baeldung.annotations"), 
  @ComponentScan(basePackageClasses = VehicleFactoryConfig.class)
})
class VehicleFactoryConfig {}
```

## @Component
- class level annotation
- the spring framework automatically detects classes annotated with *@Component* during the component scan
```
@Component
class CarUtility {
    // ...
}
```

## @Repository
- DAO or Repository classes usually represent the database access layer
```
@Repository
class VehicleRepository {
    // ...
}
```
- this annotation has automatic persistence exception translation enabled
- when using a persistence framework like Hibernate, native exceptions thrown within classes annotated with 
*Repository* will be automatically translated into subclasses of Spring's DataAccessException

## @Service
- the business logic of an application resides in the service layer 
```
@Service
public class VehicleService {
    // ...    
}
```

## @Controller
- class-level annotation that tells Spring that this class serves as a Controller in Spring MVC
```
@Controller
public class VehicleController {
    // ...
}
```

## @Configuration
- contain bean definition methods annotated with *@Bean*
```
@Configuration
class VehicleFactoryConfig {
 
    @Bean
    Engine engine() {
        return new Engine();
    }
 
}
```

## Stereotype annotations and AOP
- when we use Spring's stereotype annotations, it's easy to create a pointcut that targets all classes that have a 
particular stereotype
- eg, if we want to measure the time it takes methods to execute in the DAO layer, we can create the following
aspect to take advantage of the *Repository* stereotype:
```
@Aspect
@Component
public class PerformanceAspect {
    @Pointcut("within(@org.springframework.stereotype.Repository *)")
    public void repositoryClassMethods() {};
 
    @Around("repositoryClassMethods()")
    public Object measureMethodExecutionTime(ProceedingJoinPoint joinPoint) 
      throws Throwable {
        long start = System.nanoTime();
        Object returnValue = joinPoint.proceed();
        long end = System.nanoTime();
        String methodName = joinPoint.getSignature().getName();
        System.out.println(
          "Execution of " + methodName + " took " + 
          TimeUnit.NANOSECONDS.toMillis(end - start) + " ms");
        return returnValue;
    }
}
```
- in the above, we created a pointcut that matches all methods in classes annotated with *Repository* 
- we used the *Around* annotation to then target that pointcut and determine the execution time
- using this approach, we can add logging, performance management, auditing and others to each application layer
