# Spring profiles
- allow us to map our beans to different profiles, for example *prod*, *dev* or *test*
- can then activate different profiles in different environments to bootstrap just the beans we need

## Use *@Profile* on a Bean
- use the *@Profile* annotation on a bean to tie that bean to a specific profile
- the annotation takes the name(s) of one or more profiles
```
@Component
@Profile("dev")
public class DevDatasourceConfig {}
``` 
- we can also use the NOT operator to exclude a profile
```
@Component
@Profile("!dev")
public class NotDevDatasourceConfig
```
- this bean will be active for all profiles except *dev*

## Set the Profiles
- the next step is to activate and set the profiles so that the respective beans are registered in the container
- can be in a variety of ways

### Programmatically via *WebApplicationInitializer* Interface
- used to configure the *ServletContext* programmatically
- also a good place to set our active profile programmatically
```
@Configuration
public class MyWebApplicationInitializer implements WebApplicationInitializer {
 
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
  
        servletContext.setInitParameter("spring.profiles.active", "dev");
    }
}
```

### Programmatically via ConfigurableEnvironment
- can also set profiles directly on the environment
```
@Autowired
private ConfigurableEnvironment env;
...
env.setActiveProfiles("someProfile");
```

### Context parameter in web.xml
- profiles can be activated in the web.xml, using a context parameter
```xml
<context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>/WEB-INF/app-config.xml</param-value>
</context-param>
<context-param>
    <param-name>spring.profiles.active</param-name>
    <param-value>dev</param-value>
</context-param>
```

### JVM system parameter
- can be passed in as a JVM parameter
- will be activated during application startup
```
export spring_profiles_active=dev
```

### Maven profile
- by specifying the *spring.profiles.active* config property
```xml
<profiles>
    <profile>
        <id>dev</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
        <properties>
            <spring.profiles.active>dev</spring.profiles.active>
        </properties>
    </profile>
    <profile>
        <id>prod</id>
        <properties>
            <spring.profiles.active>prod</spring.profiles.active>
        </properties>
    </profile>
</profiles>
```
- its vallue will be used to replace the *@spring.profiles.active@* placeholder in *application.properties*
```
spring.profiles.active=@spring.profiles.active@
```
- need to enable resource filtering in the pom
```
<build>
    <resources>
        <resource>
            <directory>src/main/resources</directory>
            <filtering>true</filtering>
        </resource>
    </resources>
    ...
</build>
```
- we also need to add a -P parameter to switch profiles
```
mvn clean package -Pprod
```
- this will package the application for the *prod* profile
- also applies the *spring.profiles.active* value *prod* for the application while it is running

### @ActiveProfile in Tests
- tests make it easy to specify which profiles are active
```
@ActiveProfiles("dev")
```

## The default profile
- any bean that doesn't belong to a specific profile will belong to the default profile
- spring provides a way to set the default profile when no other profile is active
    - use the *spring.profiles.default* property
    
## Getting the active profile

### Using the environment
- can access the active profiles from the *Environment* by injecting it:
```
public class ProfileManager {
    @Autowired
    private Environment environment;
 
    public void getActiveProfiles() {
        for (String profileName : environment.getActiveProfiles()) {
            System.out.println("Currently active profile - " + profileName);
        }  
    }
}
```

### Using *spring.active.profile*
- can access the profiles by injecting the property *spring.active.profile* like this:
```
@Value("${spring.profiles.active}")
private String activeProfile;
```
- the *activeProfile* variable will contain the name of the active profile
- if there are several active profiles, the names will be in a comma seperated list
- we should consider what happens if there is no active profile
    - the absence of an active profile would prevent the application context from being created
    - would result in an IllegalArgumentException owing to the missing placeholder for injecting into the variable
    - to avoid this, we can define a default variable, in this case, an empty string:
```
@Value("${spring.profiles.active:}")
private String activeProfile;
```
- if we have a list of active profiles, we can seperate them out like this:
```
public class ProfileManager {
    @Value("${spring.profiles.active:}")
    private String activeProfiles;
 
    public String getActiveProfiles() {
        for (String profileName : activeProfiles.split(",")) {
            System.out.println("Currently active profile - " + profileName);
        }
    }
}
```

## Example of using profiles
- let's get into a practical example
- let's say we have seperate database configurations for the dev and prod environments
- we'd create a common DatasourceConfig for both of them to implement
```
public interface DatasourceConfig {
    public void setup();
}
```
- the following is the dev environment
```
@Component
@Profile("dev")
public class DevDatasourceConfig implements DatasourceConfig {
    @Override
    public void setup() {
        System.out.println("Setting up datasource for DEV environment. ");
    }
}
```
- this is the prod environment
```
@Component
@Profile("production")
public class ProductionDatasourceConfig implements DatasourceConfig {
    @Override
    public void setup() {
       System.out.println("Setting up datasource for PRODUCTION environment. ");
    }
}
```
- depending on the active profile, Spring will inject either DevDatasourceConfig or ProductionDatasourceConfig

## Profiles in Spring Boot
- in Spring Boot, spring.profiles.active can be a set as a property to define the active profiles
```
spring.profiles.active=dev
```
- to set the profiles programmatically, we can use *SpringApplication*
```
SpringApplication.setAdditionalProfiles("dev");
```
- we can also use maven
```
<plugins>
    <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <configuration>
            <profiles>
                <profile>dev</profile>
            </profiles>
        </configuration>
    </plugin>
    ...
</plugins>
```
- to execute the maven goal:
```
mvn spring-boot:run
```
- the most important change that SpringBoot brings is profile-specific property files
- they have to named in the format *application-[profile].properties*
- Spring Boot will automatically load *application.properties* for all profiles
- for example, we can create a different datasources for dev and production by using two files named
*application-dev.properties* and *application-production.properties*
- in application-production, we point to the live database:
```
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/db
spring.datasource.username=root
spring.datasource.password=root
```
- and in application-dev, we point to the dev database
```
spring.datasource.driver-class-name=org.h2.Driver
spring.datasource.url=jdbc:h2:mem:db;DB_CLOSE_DELAY=-1
spring.datasource.username=sa
spring.datasource.password=sa
```

