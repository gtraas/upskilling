# Feign
## Setup
- first we need the maven dependencies:
```
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-okhttp</artifactId>
    <version>9.3.1</version>
</dependency>
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-gson</artifactId>
    <version>9.3.1</version>
</dependency>
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-slf4j</artifactId>
    <version>9.3.1</version>
</dependency>
```
- the *feign-core* dependency is pulled in from this
- *feign-okhttp* uses Square's *OkHttp* client to make requests
- *feign-gson* uses Google's GSON as the JSON processor
- *feign-slf4j* uses the Simple Logging Facade to log requests

## The Feign client
- we need to create the object we'll be receiving from the API we're using
  - we're using a free GET API: [JsonPlaceholder](https://jsonplaceholder.typicode.com/)
```
public class User {

    @Getter
    @Setter
    private int userId;

    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private boolean completed;

    @Override
    public String toString() {
        return new StringBuilder().append("UserId: ").append(userId).append("\nID: ").append(id).append("\nTitle: ")
                .append(title).append("\nCompleted?:").append(completed).toString();
    }

}
```
- now we define our Feign client:
```
public interface UserClient {
    @RequestLine("GET /todos/{id}")
    User findById(@Param("id") String id);
}
```
- a POST might look something like this:
```
@RequestLine("POST")
@Headers("Content-Type: application/json")
void create(Book book);
```
- feign clients can only handle text-based calls, so no uploading or downloading
- now we need to make use of this client:
```
UserClient userClient = Feign.builder()
        .client(new OkHttpClient())
        .encoder(new GsonEncoder())
        .decoder(new GsonDecoder())
        .logger(new Slf4jLogger(UserClient.class))
        .logLevel(Logger.Level.FULL)
        .target(UserClient.class, "https://jsonplaceholder.typicode.com");

User u = userClient.findById("1");

log.info("User: {}", u);
```