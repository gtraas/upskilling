package com.bluesharkdev.upskill.randomtech.feign;

import feign.Param;
import feign.RequestLine;

public interface UserClient {
    @RequestLine("GET /todos/{id}")
    User findById(@Param("id") String id);
}
