package com.bluesharkdev.upskill.randomtech.feign;

import lombok.Getter;
import lombok.Setter;

public class User {

    @Getter
    @Setter
    private int userId;

    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private boolean completed;

    @Override
    public String toString() {
        return new StringBuilder().append("UserId: ").append(userId).append("\nID: ").append(id).append("\nTitle: ")
                .append(title).append("\nCompleted?:").append(completed).toString();
    }

}
