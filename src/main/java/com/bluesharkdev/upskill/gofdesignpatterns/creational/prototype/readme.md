# Prototype Pattern
## Explanation
The prototype pattern allows us to hide the complexity of making new instances from the client. The concept is to copy 
an existing object rather than creating a new instance from scratch. The existing object acts as a prototype and contains
the state of the object. The newly copied object may change properties only if required. This approach saves costly 
resources and time, especially when object creation is a costly process.

One of the best ways to create an object from an existing object is via the ***clone()*** method.

**Prototype Design Participants**

* **Prototype**: This is the prototype of the actual object
* **Prototype registry**: This is used as registry service to have all prototypes accessible using simple string parameters.
* **Client**: Client is responsible for using the registry service to access prototype instances

## When should we use the Prototype pattern

* when a system should be independent of how its products are created, composed and represented
* when the classes to instantiate are specified at runtime
* to avoid building a class hierarchy of factories that parallels the hierarchy of products
* when instances of a class can have one of only a few different states. It may be more convenient to install a 
corresponding number of prototypes and clone them rather than instantiating the class manually, each time with the appropriate state.

![Prototype](../../../../../../../resources/images/gofpatterns/prototype.png)

## Advantages

* Adding and removing products at runtime - prototypes let you incorporate a new concrete product class into a system 
simply by registering a prototypical instance with the client. That's more flexible than other creational patterns 
because a client can install and remove prototypes at runtime.
* Specifying new objects by varying values - Highly dynamic systems let you define new behaviour through object 
composition by specifying values for an object's variables and not by defining new classes
* Specifying new objects by varying structure - Many applications build objects from parts and subparts. For convenience, 
such applications let you instantiate complex user-defined structures to use a specific subcircuit over and over.
* Reduced subclassing - Factory method often produces a hierarchy of Creator classes that parallels the product class 
hierarchy. The prototype pattern lets you clone a prototype instead of asking a factory method to make a new object. 

## Disadvantages

* Overkill for a project that uses very few objects and/or does not have an underlying emphasis on the extension of prototype chains
* Hides concrete product classes from the client
 


