package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.client;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal.Animal;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color.Color;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.provider.FactoryProvider;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AbstractFactoryClient {

    public static void main(String [] args) {
        log.info(((Animal) FactoryProvider.getFactory("Animal").create("Cow")).makeSound());

        log.info(((Color) FactoryProvider.getFactory("Color").create("Red")).getColor());
    }
}
