# Factory Method Pattern
## Explanation
In the factory method pattern, we create an object without exposing the creation logic to the client, and the client 
uses the same common interface to create new types of objects.

The idea is to use a static member-function to create and return instances, hiding the details of class modules from the 
client.

![Class diagram](../../../../../../../resources/images/gofpatterns/factorymethod.jpg) 

### Pro's

- Useful when the implementation of an interface / abstract class is going to change frequently
- If the initialization process is straightforward and the constructor only requires a few parameters
- Makes testing easy. Can easily mock the interface.
- Can potentially limit the number of instances created.
- Can be used to disambiguate complex constructors

### Cons

- Clients may have to sub-class the creator class just to create a particular concrete product object
- Client now has to deal with another point of evolution
- The factory is bound to client code. It is difficult to use another factory for creating objects

