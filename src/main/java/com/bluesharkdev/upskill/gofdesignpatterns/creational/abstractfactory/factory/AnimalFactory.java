package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.factory;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal.Animal;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal.Cow;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal.Duck;

public class AnimalFactory implements AbstractFactory<Animal> {
    @Override
    public Animal create(String type) {
        if ("Cow".equalsIgnoreCase(type)) {
            return new Cow();
        } else if ("Duck".equalsIgnoreCase(type)) {
            return new Duck();
        }

        return null;
    }
}
