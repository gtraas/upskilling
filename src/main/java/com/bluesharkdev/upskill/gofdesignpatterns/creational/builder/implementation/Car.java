package com.bluesharkdev.upskill.gofdesignpatterns.creational.builder.implementation;

public class Car {
    private int wheels;
    private String color;

    private Car() {

    }

    @Override
    public String toString() {
        return "Car{" +
                "wheels=" + wheels +
                ", color='" + color + "'" +
                "}";
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public static class CarBuilder {
        private int wheels;
        private String color;

        public CarBuilder setWheels(int wheels) {
            this.wheels = wheels;
            return this;
        }

        public CarBuilder setColor(String color) {
            this.color = color;
            return this;
        }

        public Car build() {
            Car car = new Car();
            car.wheels = this.wheels;
            car.color = this.color;
            return car;
        }

    }
}

