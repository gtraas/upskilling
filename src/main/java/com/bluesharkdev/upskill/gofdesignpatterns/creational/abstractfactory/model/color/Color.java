package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color;

public interface Color {

    String getColor();
}
