# Abstract Factory Pattern
## Explanation:
Abstract factory patterns work around a "super-factory" which creates other factories.

Abstract factory pattern implementation provides us with a framework that allows us to create objects that follow a similar pattern. 

At runtime, abstract factories can be coupled with any concrete factory that can create objects of the desired type.

![Class diagram](../../../../../../../resources/images/gofpatterns/AbstractFactoryPattern.png)

* **AbstractFactory** - declares an interface for operations that create abstract product items

* **ConcreteFactory** - Implements the operations declared in the AbstractFactory to create concrete product objects

* **Product** - Defines a product object to be created by the corresponding concrete factory and implements the AbstractProduct interface

* **Client** - Uses only interfaces declared by the AbstractFactory and AbstractProduct classes.

Client software creates a concrete implementation of the abstract factory and then uses the generic interfaces to create the concrete objects that are part of the family of objects.
The client does not know or care which concrete objects it gets from each of these concrete factories since it uses only the generic interfaces of their products.

### Pro's
* Isolation of concrete classes - helps you control the classes of objects that an application creates. Product class 
    names are isolated in the implementation of the concrete factory; they do not appear in client code.
* Exchanging product families easily - The class of a concrete factory appears only once in an application: where it 
    is instantiated. This makes it easy to change the concrete factory an application uses.
* Promoting consistency among products - When product objects in a family are designed to work together, it's 
    important that an application use objects from only one family at a time. Abstract Factory implementations make this easier to enforce.


### Cons

- Ungainly
- Maintenance nightmare
- Difficult to support new kinds of products

