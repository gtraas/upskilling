package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.provider;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.factory.AbstractFactory;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.factory.AnimalFactory;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.factory.ColorFactory;

public class FactoryProvider {

    public static AbstractFactory getFactory(String choice){

        if("Animal".equalsIgnoreCase(choice)){
            return new AnimalFactory();
        }
        else if("Color".equalsIgnoreCase(choice)){
            return new ColorFactory();
        }

        return null;
    }
}
