package com.bluesharkdev.upskill.gofdesignpatterns.creational.prototype.client;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.prototype.implementation.ColorStore;

public class Client {
    public static void main(String [] args) {
        ColorStore.getColor("blue").addColor();
        ColorStore.getColor("red").addColor();
        ColorStore.getColor("red").addColor();
        ColorStore.getColor("blue").addColor();
    }
}
