package com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model;

public class Triangle implements Polygon {
    @Override
    public String getType() {
        return "Triangle";
    }
}
