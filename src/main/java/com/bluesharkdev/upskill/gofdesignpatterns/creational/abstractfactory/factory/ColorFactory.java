package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.factory;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color.Black;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color.Color;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color.Red;

public class ColorFactory implements AbstractFactory<Color> {
    @Override
    public Color create(String type) {
        if ("Black".equalsIgnoreCase(type)) {
            return new Black();
        } else if ("Red".equalsIgnoreCase(type)) {
            return new Red();
        }

        return null;
    }
}
