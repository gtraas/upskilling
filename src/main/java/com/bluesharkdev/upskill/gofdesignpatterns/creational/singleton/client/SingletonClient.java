package com.bluesharkdev.upskill.gofdesignpatterns.creational.singleton.client;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.singleton.implementation.ClassSingleton;

public class SingletonClient {
    public static void main(String [] args) {
        System.out.println("Singleton first use? " + (ClassSingleton.getInstance().getCount() == 1));
        System.out.println("Singleton second use? " + (ClassSingleton.getInstance().getCount() == 2));
    }
}
