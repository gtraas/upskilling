package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color;

public class Red implements Color {
    @Override
    public String getColor() {
        return "Red";
    }
}
