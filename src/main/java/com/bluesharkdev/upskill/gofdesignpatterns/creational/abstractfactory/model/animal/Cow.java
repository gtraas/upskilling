package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal;

public class Cow implements Animal {
    @Override
    public String getType() {
        return "Cow";
    }

    @Override
    public String makeSound() {
        return "Mooooo";
    }
}
