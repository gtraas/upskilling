package com.bluesharkdev.upskill.gofdesignpatterns.creational.prototype.implementation;

public class BlueColor extends Color {

    public BlueColor() {
        this.colorName = "Blue";
    }

    @Override
    public void addColor() {
        System.out.println("Blue added");
    }
}
