package com.bluesharkdev.upskill.gofdesignpatterns.creational.singleton.implementation;

public class ClassSingleton {

    private static ClassSingleton INSTANCE;

    private static int count = 0;

    public static synchronized ClassSingleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClassSingleton();
        }
        count++;
        return INSTANCE;
    }

    public int getCount() {
        return count;
    }
}
