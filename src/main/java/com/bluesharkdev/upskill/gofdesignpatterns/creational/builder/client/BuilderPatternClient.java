package com.bluesharkdev.upskill.gofdesignpatterns.creational.builder.client;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.builder.implementation.Car;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class  BuilderPatternClient {

    public static void main(String [] args) {
        Car car = new Car.CarBuilder().setColor("Red").setWheels(4).build();
        log.info(car.toString());
    }

}
