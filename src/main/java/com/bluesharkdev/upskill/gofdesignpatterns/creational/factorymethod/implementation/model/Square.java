package com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model;

public class Square implements Polygon {
    @Override
    public String getType() {
        return "Square";
    }
}
