package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal;

public class Duck implements Animal {
    @Override
    public String getType() {
        return "Duck";
    }

    @Override
    public String makeSound() {
        return "Quack";
    }
}
