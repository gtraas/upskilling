package com.bluesharkdev.upskill.gofdesignpatterns.creational.prototype.implementation;

public class RedColor extends Color {

    public RedColor() {
        this.colorName = "Red";
    }

    @Override
    public void addColor() {
        System.out.println("Red added");
    }
}
