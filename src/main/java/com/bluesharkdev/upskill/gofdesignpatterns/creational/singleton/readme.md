# Singleton Pattern
## Explanation
A class is defined in such a way that only a single instance of that class is created in the complete execution of a 
program of project. It is used where only a single instance of a class is required to control the action throughout the execution. 

An implementation of the singleton should have the following:

* It should have only 1 instance: this is done by providing an instance of the class from within the class. Outer 
  classes or subclasses should be prevented from creating the class. The constructor is made private.
* It should be globally accessible: this means a static public method giving access to the instance.

Initialisation types

* **Early initialisation**: the class is initialised whether it is to be used or not. The main advantage of this method 
  is its simplicity. You instantiate the class at the time of class-loading. The drawback is that it exists whether or 
  not it is being used or not.
* **Lazy initialisation**: In this method, the class is initialised only when required. Generally this is the preferred 
  method. 

#### Caveats

* It is not multithreaded unless the synchronized keyword is added
* Distributed systems may have multiple singleton instances due to the fact that distributed systems run over multiple 
  JVMs and the definition of a singleton is that only 1 will exist in a given JVM
* Every class loader may have its own version of the singleton
* If nothing holds a reference to the singleton, it may be garbage collected. If the singleton had a state, that state will be lost


