package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.factory;

public interface AbstractFactory<T> {

    T create(String type);

}
