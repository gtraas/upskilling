package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.color;

public class Black implements Color {
    @Override
    public String getColor() {
        return "Black";
    }
}
