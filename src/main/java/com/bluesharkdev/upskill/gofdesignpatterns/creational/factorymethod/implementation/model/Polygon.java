package com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model;

public interface Polygon {
    String getType();
}
