package com.bluesharkdev.upskill.gofdesignpatterns.creational.prototype.implementation;

public abstract class Color implements Cloneable {

    protected String colorName;

    public abstract void addColor();

    public Object clone() {
        Object clone = null;

        try {
            clone = super.clone();
        } catch (CloneNotSupportedException cnse) {
            cnse.printStackTrace();
        }

        return clone;
    }

}
