package com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.factory;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model.Pentagon;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model.Polygon;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model.Square;
import com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model.Triangle;

public class PolygonFactory {

    public Polygon getPolygon(int numSides) {
        if(numSides == 3) {
            return new Triangle();
        }
        if(numSides == 4) {
            return new Square();
        }
        if(numSides == 5) {
            return new Pentagon();
        }
        return null;
    }

}
