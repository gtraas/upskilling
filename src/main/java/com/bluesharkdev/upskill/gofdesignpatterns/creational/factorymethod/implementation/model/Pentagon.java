package com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.model;

public class Pentagon implements Polygon {
    @Override
    public String getType() {
        return "Pentagon";
    }
}
