package com.bluesharkdev.upskill.gofdesignpatterns.creational.abstractfactory.model.animal;

public interface Animal {

    String getType();
    String makeSound();
}
