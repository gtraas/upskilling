package com.bluesharkdev.upskill.gofdesignpatterns.creational.prototype.implementation;

import java.util.HashMap;
import java.util.Map;

public class ColorStore {

    private static Map<String, Color> colorMap = new HashMap<>();

    static
    {
        colorMap.put("red", new RedColor());
        colorMap.put("blue", new BlueColor());
    }

    public static Color getColor(String colorName)
    {
        return (Color) colorMap.get(colorName).clone();
    }

}
