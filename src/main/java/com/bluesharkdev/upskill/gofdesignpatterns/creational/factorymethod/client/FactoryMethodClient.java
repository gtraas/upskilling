package com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.client;

import com.bluesharkdev.upskill.gofdesignpatterns.creational.factorymethod.implementation.factory.PolygonFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FactoryMethodClient {

    public static void main(String [] args) {
        log.info("Triangle: {}", new PolygonFactory().getPolygon(3).getType());
        log.info("Pentagon: {}", new PolygonFactory().getPolygon(5).getType());
    }
}
