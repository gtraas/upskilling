package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.client;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza.Farmhouse;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza.Margherita;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza.Pizza;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.toppings.Barbeque;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.toppings.FreshTomato;

public class DecoratorClient {
    public static void main(String [] args) {
        Pizza pizza = new Margherita();
        System.out.println(pizza.getDescription() + " - Cost: R" + pizza.getCost());

        pizza = new Farmhouse();
        pizza = new FreshTomato(pizza);
        pizza = new Barbeque(pizza);
        System.out.println(pizza.getDescription() + " - Cost: R" + pizza.getCost());

        //Causes NPE because of description
        pizza = new Barbeque(null);
        System.out.println(pizza.getDescription() + " - Cost: R" + pizza.getCost());
    }
}
