# Bridge Pattern
## Explanation

The bridge design pattern allows you to seperate the abstraction from the implementation. This is a design mechanism 
that encapsulates an implementation class inside an interface class.

* The bridge pattern allows the abstraction and the implementation to be developed independently, and the client code 
can only access the abstraction part without being concerned about the implementation.
* The abstraction is an interface or abstract class and the implementor is also an interface or abstract class.
* The abstraction contains a reference to the implementor. Children of the abstraction are referred to a refined 
abstractions and the children of the implementor are concrete implementors. Since we can change the reference to an 
implementor in the abstraction, we are able to change the abstraction's implementor at runtime. Changes to the implementor 
do not affect client code.
* Increases loose coupling between class abstraction and its implementation
 

![Bridge pattern](../../../../../../../resources/images/gofpatterns/bridge.png)

##### Elements of the Bridge Design pattern

* **Abstraction** - core of the design pattern. Contains a reference to the implementor.
* **Refined abstraction** - Extends the abstraction and takes the finer detail one step further. Hides the finer elements from implementors.
* **Implementor** - defines the interface for implementor classes. The interface does not need to correspond directly to the abstraction interface and can be very different. 
* **Concrete Implementor** - Implements the implementor by providing concrete implementation.

#### When we need a bridge pattern

It becomes handy when you must subclass different times in ways that are orthogonal with one another.
In the code example, we may have had a design like the below:

###### Without the Bridge pattern

![Bad design](../../../../../../../resources/images/gofpatterns/bridge1.png)

The above solution has a problem. If you want to change the Bus class, you may end up changing the ProduceBus and AssembleBus as well. If the change is workshop specific, Bike may need to change.

###### With the Bridge pattern

![Good design](../../../../../../../resources/images/gofpatterns/bridge2.png)

### Advantages

* Decouples an abstraction from its implementation so that the two can vary independently
* Used mainly for implementing platform independence
* Adds one more method level redirection to achieve the objective
* Must be designed up front. Difficult to change over later.

