package com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.client;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation.ComputerFacade;

public class FacadeClient {
    public static void main(String [] args) {
        ComputerFacade cf = new ComputerFacade();
        cf.start();
    }
}
