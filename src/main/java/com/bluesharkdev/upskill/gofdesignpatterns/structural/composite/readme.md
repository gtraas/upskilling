## Purpose
* What problems can the Composite design pattern solve?
  * A part-whole hierarchy should be represented so that clients can treat part and whole objects uniformly.
  * A part-whole hierarchy should be represented as tree structure.
* When defining (1) Part objects and (2) Whole objects that act as containers for Part objects, clients must treat them 
separately, which complicates client code.
* What solution does the Composite design pattern describe?
  * Define a unified Component interface for both part (Leaf) objects and whole (Composite) objects.
  * Individual Leaf objects implement the Component interface directly, and Composite objects forward requests to their child components.

## What is it
* The Composite Pattern has four participants:
  * Component – Component declares the interface for objects in the composition and for accessing and managing its child 
  components. It also implements default behavior for the interface common to all classes as appropriate.
  * Leaf – Leaf defines behavior for primitive objects in the composition. It represents leaf objects in the composition.
  * Composite – Composite stores child components and implements child related operations in the component interface.
  * Client – Client manipulates the objects in the composition through the component interface.
* Client use the component class interface to interact with objects in the composition structure. If recipient is a leaf 
then request is handled directly. If recipient is a composite, then it usually forwards request to its child components, 
possibly performing additional operations before and after forwarding

![Composite design pattern](../../../../../../../resources/images/gofpatterns/composite.png)

### When to use
* Composite should be used when clients ignore the difference between compositions of objects and individual objects. 
If programmers find that they are using multiple objects in the same way, and often have nearly identical code to handle 
each of them, then composite is a good choice; it is less complex in this situation to treat primitives and composites as homogeneous.

### When not to use
* Composite Design Pattern makes it harder to restrict the type of components of a composite. So it should not be used 
when you don’t want to represent a full or partial hierarchy of objects.
* Composite Design Pattern can make the design overly general. It makes harder to restrict the components of a composite. 
Sometimes you want a composite to have only certain components. With Composite, you can’t rely on the type system to 
enforce those constraints for you. Instead you’ll have to use run-time checks.


