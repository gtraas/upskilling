package com.bluesharkdev.upskill.gofdesignpatterns.structural.bridge.implementation;

public interface Workshop {
    void work();
}
