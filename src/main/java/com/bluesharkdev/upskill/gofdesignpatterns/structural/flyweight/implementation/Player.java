package com.bluesharkdev.upskill.gofdesignpatterns.structural.flyweight.implementation;

public interface Player {
    public void assignWeapon(String weapon);
    public void mission();
}
