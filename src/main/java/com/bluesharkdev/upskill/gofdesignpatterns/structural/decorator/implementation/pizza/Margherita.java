package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza;

public class Margherita extends Pizza {

    public Margherita() {
        description = "Margherita";
    }

    @Override
    public int getCost() {
        return 100;
    }
}
