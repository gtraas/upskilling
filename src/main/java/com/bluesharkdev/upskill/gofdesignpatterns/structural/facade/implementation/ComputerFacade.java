package com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation.complexity.CPU;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation.complexity.HardDrive;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation.complexity.Memory;

public class ComputerFacade {
    private static final long BOOT_ADDRESS = 12L;
    private static final long BOOT_SECTOR = 15L;
    private static final int SECTOR_SIZE = 200;
    private final CPU processor;
    private final Memory ram;
    private final HardDrive hd;

    public ComputerFacade() {
        this.processor = new CPU();
        this.ram = new Memory();
        this.hd = new HardDrive();
    }

    public void start() {
        processor.freeze();
        ram.load(BOOT_ADDRESS, hd.read(BOOT_SECTOR, SECTOR_SIZE));
        processor.jump(BOOT_ADDRESS);
        processor.execute();
    }
}
