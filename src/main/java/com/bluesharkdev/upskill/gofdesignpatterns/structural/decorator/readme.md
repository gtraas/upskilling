# Decorator Pattern
## Explanation ([taken from GeeksForGeeks.com](https://www.geeksforgeeks.org/decorator-pattern/)  )
Assuming we have a pizza shop that allows clients to order 4 types of pizza
- Peppy Paneer
- Farmhouse
- Margherita
- Chicken Fiesta

We would use inheritance and create a Pizza object for common functionality and then subclass the four pizzas.

![Class diagram](../../../../../../../resources/images/gofpatterns/decorator1.png)

Now let's say that the pizza shop allows clients to add their own toppings, like Capsicum, Jalapeno, etc. How best would 
we accomplish this such that we can get the cost of any particular pizza? 

#### Option 1
We could create a subclass for every different type of pizza, such as the following:

![Class diagram](../../../../../../../resources/images/gofpatterns/decorator2.png)

This is way too complex and creates a maintenance nightmare. If a new pizza is added, we'd have to add so many subclasses 
that this approach is a really bad choice.

#### Option 2
We add instance variables to the base class to represent whether or not each pizza has a topping. Such as:

![Class diagram](../../../../../../../resources/images/gofpatterns/decorator3.png)

The getCost() of the superclass calculates the cost for all the toppings present on the pizza, while the getCost() in 
the subclass adds the cost of that specific pizza.

Example code:
```
//Superclass
public int getCost()
{
    int totalToppingsCost = 0;
    if (hasJalapeno() )
        totalToppingsCost += jalapenoCost;
    if (hasCapsicum() )
        totalToppingsCost += capsicumCost;

    // similarly for other toppings
    return totalToppingsCost;
}

//Sub-class
public int getCost() {
    //price for this pizza = 100
    return super.getCost() + 100;
}
```

This looks fine at first, but there are some problems:
- if the costs change for ingredients, it means a code change. 
- New toppings mean an alteration of the getCost() method. 
- For some pizzas, some toppings may not be appropriate, but the subclass inherits them anyway.
- What if a customer wants double of something? Double bacon?

The above violates the Open-Closed principle: classes should be closed to modification and open to extension.

With the decorator pattern, we don't use subclassing. We take a pizza and decorate it with toppings at runtime.

1. Take a pizza object

2. Decorate it with a capsicum object

3. Decorate it with a cheeseburst object

4. Call getCost() and use delegation rather than inheritance to calculate the toppings cost.

![Decorator](../../../../../../../resources/images/gofpatterns/Decorator4.jpg)

### Properties of the Decorator
- Decorators have the same type as the object they are decorating
- Multiple decorators can decorate/wrap an object
- We can pass the decorated object around instead of the original, since decorators have the same type as the object
- We can decorate objects at runtime

### Class diagram
![Class diagram](../../../../../../../resources/images/gofpatterns/decorator5.jpg)

- Each component can be used on its own, or may be wrapped by a decorator
- Each decorator has an instance variable that holds the reference to the component it decorates (HAS-A relationship)
- The ConcreteComponent is the object we decorate

#### Advantages
- The decorator pattern can be used to extend (decorate) the functionality of a certain object at runtime
- The decorator pattenr is an alternative to subclassing. Subclassing adds behaviour at compile time and the change 
affects all instances of the original class. Decorating can provide new behaviour at runtime to individual objects.
- Decorators offer a pay-as-you-go approach to adding responsibility. Instead of trying to support all foreseeable 
features in a complex, customizable class, you can define a simple class and add functionality incrementally with Decorator objects.

#### Disadvantages
- Decorators can complicate the process of instantiating the component because you not only have to instantiate the object 
but wrap it in a number of decorators
- It can be complicated to have decorators keep track of other decorators, because to look back into multiple layers of 
the decorator chain starts to push the pattern past its true intent.





