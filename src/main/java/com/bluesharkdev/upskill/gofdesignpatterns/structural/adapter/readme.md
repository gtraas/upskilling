# Adapter Pattern
## Explanation
Pretty easy to understand because the world is full of adapters. Consider a USB to Ethernet adapter. We need it when we 
have Ethernet on one end and USB on the other. In concrete terms, adapters are used when we have a class (Client) 
expecting some type of object and we have an object (Adaptee) offering the same features but exposing a different interface.

To use an adapter:

* The client makes a request to the adapter by calling a method on it using the target interface
* The adapter translates that request on the adaptee using the adaptee interface
* Client receives the results of the call and is unaware of the adapter's presence.

The adapter pattern converts the interface of a class into another interface that the client is expecting. 

![Adapter](../../../../../../../resources/images/gofpatterns/adapter.jpg)

### Advantages

* Helps achieve reusability and flexibility
* Client class is not complicated by having to use a different interface and can use polymorphism to swap between 
different implementations of adapters

### Disadvantages

* All requests are forwarded, so there is a slight increase in the overhead.
* Sometimes many adapters are required along an adapter chain to reach the type which is required.



