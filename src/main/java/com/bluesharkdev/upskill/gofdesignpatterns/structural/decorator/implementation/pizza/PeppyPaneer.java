package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza;

public class PeppyPaneer extends Pizza {

    public PeppyPaneer() {
        description = "PeppyPaneer";
    }

    @Override
    public int getCost() {
        return 100;
    }
}
