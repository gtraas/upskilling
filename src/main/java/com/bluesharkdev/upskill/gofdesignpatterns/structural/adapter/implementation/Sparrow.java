package com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation;

public class Sparrow implements Bird {
    @Override
    public void fly() {
        System.out.println("Flying");
    }

    @Override
    public void makeSound() {
        System.out.println("Chirp");
    }
}
