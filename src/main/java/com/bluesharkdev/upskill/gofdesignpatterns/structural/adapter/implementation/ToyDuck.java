package com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation;

public interface ToyDuck {
    void squeak();
}
