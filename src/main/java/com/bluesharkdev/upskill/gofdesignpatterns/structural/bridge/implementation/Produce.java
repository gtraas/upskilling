package com.bluesharkdev.upskill.gofdesignpatterns.structural.bridge.implementation;

public class Produce implements Workshop {
    @Override
    public void work() {
        System.out.println("Produced");
    }
}
