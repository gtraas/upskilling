package com.bluesharkdev.upskill.gofdesignpatterns.structural.composite.client;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.composite.implementation.composite.CompanyDirectory;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.composite.implementation.leaf.Developer;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.composite.implementation.leaf.Manager;

public class CompositeClient {
    public static void main(String [] args) {
        Developer dev1 = new Developer(1L, "Graham", "Senior Java Developer");
        Developer dev2 = new Developer(2L, "Sharon", "Front-end Developer");
        CompanyDirectory developmentDept = new CompanyDirectory();
        developmentDept.addEmployee(dev1);
        developmentDept.addEmployee(dev2);

        Manager man1 = new Manager(3L, "Gideon", "Development Manager");
        Manager man2 = new Manager(4L, "Kathryn", "General Manager");
        CompanyDirectory managers = new CompanyDirectory();
        managers.addEmployee(man1);
        managers.addEmployee(man2);

        CompanyDirectory allStaff = new CompanyDirectory();
        allStaff.addEmployee(developmentDept);
        allStaff.addEmployee(managers);

        allStaff.showEmployeeDetails();

    }
}
