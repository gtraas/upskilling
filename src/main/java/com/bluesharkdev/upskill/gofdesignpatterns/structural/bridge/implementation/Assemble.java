package com.bluesharkdev.upskill.gofdesignpatterns.structural.bridge.implementation;

public class Assemble implements Workshop {
    @Override
    public void work() {
        System.out.print(" and ");
        System.out.println("Assembled");
    }
}
