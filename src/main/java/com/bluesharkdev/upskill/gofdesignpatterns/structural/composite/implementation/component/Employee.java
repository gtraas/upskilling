package com.bluesharkdev.upskill.gofdesignpatterns.structural.composite.implementation.component;

//This is the COMPONENT
public interface Employee {
    void showEmployeeDetails();
}
