package com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation.complexity;

public class Memory {
    public void load(long position, byte[] data) {
        System.out.println("Memory loading from position [" + position + "] with data size " + data.length);
    }
}
