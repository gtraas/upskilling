package com.bluesharkdev.upskill.gofdesignpatterns.structural.facade.implementation.complexity;

public class HardDrive {
    public byte[] read(long lba, int size) {
        System.out.println("HardDrive reading from [" + lba + "] and size " + size);
        return new byte[]{};
    }
}
