package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza;

// Abstract Pizza class (All classes inherit from this)
public abstract class Pizza {

    String description = "Base pizza";

    public String getDescription() {
        return description;
    }

    public abstract int getCost();

}
