# Facade Pattern
## Explanation

The facade patterns purpose is to provide a single simple interface behind which the system complexities remain hidden.

A good example is the face of a building. People walking past on the road can only see the glass face of the building. 
They do not know anything about it, the wiring, the pipes or other complexities. The face, or facade, hides all the inner workings.

A good Java example is the JDBC Connection interface. We don't care about the inner workings of the Connection. We just 
want the connection to the database. The implementation is irrelevant to us, and is implemented by the vendor.

The Facade pattern hides the complexities of the system and provides an interface for the client to access the system. 

![Facade](../../../../../../../resources/images/gofpatterns/facade.png)

#### When to use it

It is useful when you have a complex system and want to provide the client with a simplified entry point. Also useful 
if you want to make an external communication layer over an existing system which is incompatible with the system.





