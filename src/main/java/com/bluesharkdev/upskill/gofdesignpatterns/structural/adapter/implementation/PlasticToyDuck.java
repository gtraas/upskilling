package com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation;

public class PlasticToyDuck implements ToyDuck {
    @Override
    public void squeak() {
        System.out.println("Squeak");
    }
}
