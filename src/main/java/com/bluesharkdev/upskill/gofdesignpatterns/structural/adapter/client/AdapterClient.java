package com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.client;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation.BirdAdapter;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation.PlasticToyDuck;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation.Sparrow;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation.ToyDuck;

public class AdapterClient {
    public static void main(String [] args) {
        Sparrow sparrow = new Sparrow();
        ToyDuck toyDuck = new PlasticToyDuck();

        ToyDuck birdAdapter = new BirdAdapter(sparrow);

        System.out.println("Sparrow:");
        sparrow.fly();
        sparrow.makeSound();

        System.out.println();
        System.out.println("Toy Duck:");
        toyDuck.squeak();

        System.out.println();
        System.out.println("ToyDuck acting like a bird:");
        birdAdapter.squeak();

    }
}
