package com.bluesharkdev.upskill.gofdesignpatterns.structural.proxy.client;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.proxy.implementation.model.Internet;
import com.bluesharkdev.upskill.gofdesignpatterns.structural.proxy.implementation.proxy.ProxyInternet;

public class Client {
    public static void main (String[] args)
    {
        Internet internet = new ProxyInternet();
        try
        {
            internet.connectTo("geeksforgeeks.org");
            internet.connectTo("abc.com");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
