package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.toppings;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza.Pizza;

// The decorator class :  It extends Pizza to be interchangable with it.
// The toppings decorator can also be implemented as an interface
public abstract class ToppingsDecorator extends Pizza {
    public abstract String getDescription();
}
