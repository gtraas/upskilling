package com.bluesharkdev.upskill.gofdesignpatterns.structural.adapter.implementation;

public interface Bird {

    void fly();
    void makeSound();
}
