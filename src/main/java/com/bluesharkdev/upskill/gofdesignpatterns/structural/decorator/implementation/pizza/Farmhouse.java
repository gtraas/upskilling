package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza;

public class Farmhouse extends Pizza {
    public Farmhouse() {
        description = "Farmhouse";
    }

    @Override
    public int getCost() {
        return 200;
    }
}
