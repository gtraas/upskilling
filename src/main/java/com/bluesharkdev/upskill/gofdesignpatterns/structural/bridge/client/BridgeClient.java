package com.bluesharkdev.upskill.gofdesignpatterns.structural.bridge.client;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.bridge.implementation.*;

public class BridgeClient {
    public static void main(String [] args) {
        Vehicle car = new Car(new Produce(), new Assemble());
        car.manufacture();
        Vehicle bike = new Bike(new Produce(), new Assemble());
        bike.manufacture();
    }
}
