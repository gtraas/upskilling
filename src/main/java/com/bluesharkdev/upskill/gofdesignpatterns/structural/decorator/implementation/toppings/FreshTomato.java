package com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.toppings;

import com.bluesharkdev.upskill.gofdesignpatterns.structural.decorator.implementation.pizza.Pizza;

public class FreshTomato extends ToppingsDecorator {

    Pizza pizza;

    public FreshTomato(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + ", Fresh tomato";
    }

    @Override
    public int getCost() {
        return pizza.getCost() + 40;
    }
}
