package com.bluesharkdev.upskill.gofdesignpatterns.structural.proxy.implementation.model;

public interface Internet {
    public void connectTo(String serverhost) throws Exception;
}
