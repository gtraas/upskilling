# Flyweight Pattern
## Explanation

The flyweight pattern is used when we want to create a large number of similar objects. Each flyweight object is immutable.

#### Why do we care about the number of objects in our system?
- Less objects mean less memory used
- Although creating an object is fast in Java, we can still reduce execution time by sharing objects.

In the Flyweight pattern, we use a HashMap to store a reference to the objects that have already been created. The client 
simply passes us a key, and we either retrieve the object from the map, or create and add it.

#### Extrinsic and Intrinsic states
Suppose in a text editor when we enter a character, an object of Character class is created, the attributes of the 
Character class are {name, font, size}. We do not need to create an object every time client enters a character since 
the letter ‘B’ is no different from any other ‘B’ . If the client again types a ‘B’, we simply return the object which 
we have already created before. Now all of these are intrinsic states (name, font, size), since they can be shared among
the different objects as they are similar to each other.

Now we add more attributes to the Character class, row and column. They specify the position of a character in the document. 
Now these attributes will not be similar even for the same characters. Since no two characters will have the same position 
in a document, these states are termed as extrinsic states, and they can’t be shared among objects.

#### Implementation
We implement the creation of Terrorists and Counter Terrorists in the game of Counter Strike. So we have 2 classes: 
one for Terrorist(T) and the other for Counter Terrorist(CT). Whenever a player asks for a weapon, we assign him the 
correct weapon. In the mission, a terrorist’s task is to plant a bomb while the counter terrorists have to diffuse the bomb.

#### Why use Flyweight Design Pattern in this example? 
Here we use the Fly Weight design pattern, since we need to reduce the object count for players. Now we have n number of 
players playing CS 1.6, if we do not follow the Fly Weight Design Pattern then we will have to create n number of objects, 
one for each player. But now we will only have to create 2 objects, one for terrorists and the other for counter terrorists. 
We will reuse them again and again whenever required.

#### Intrinsic State
Here ‘task’ is an intrinsic state for both types of players, since this is always same for T’s/CT’s. We can have some 
other states like their color or any other properties which are similar for all the Terrorists/Counter Terrorists in 
their respective Terrorists/Counter Terrorists class.

#### Extrinsic State
Weapon is an extrinsic state since each player can carry any weapon of his/her choice. Weapon need to be passed as a 
parameter by the client itself.

![Flyweight](../../../../../../../resources/images/gofpatterns/flyweight.jpg)









