package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example1.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example1.implementation.NetOrder;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example1.implementation.OrderProcessTemplate;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example1.implementation.StoreOrder;

public class TemplateClient1 {
    public static void main(String [] args) {
        OrderProcessTemplate netOrder = new NetOrder();
        netOrder.processOrder(true);
        System.out.println();
        OrderProcessTemplate storeOrder = new StoreOrder();
        storeOrder.processOrder(true);
    }
}
