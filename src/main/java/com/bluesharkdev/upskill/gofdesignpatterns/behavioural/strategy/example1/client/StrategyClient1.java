package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example1.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example1.implementation.Discounter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example1.implementation.Discounter.christmas;
import static com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example1.implementation.Discounter.easter;
import static com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example1.implementation.Discounter.newYear;

public class StrategyClient1 {
    public static void main(String [] args) {
        System.out.println("Example 1: Combining all discounts");
        List<Discounter> discounters = Arrays.asList(christmas(), newYear(), easter());

        BigDecimal amount = BigDecimal.valueOf(100);
        System.out.println(amount.toString());

        final Discounter combinedDiscounter = discounters
                .stream()
                .reduce(v -> v, Discounter::combine);

        amount = combinedDiscounter.apply(amount);
        System.out.println(amount.toString());

        System.out.println("Chaining discounts");
        System.out.println(100);
        final Function<BigDecimal, BigDecimal> combinedDiscounters = Discounter
                .christmas()
                .andThen(newYear());

        System.out.println(combinedDiscounters.apply(BigDecimal.valueOf(100)).toString());
    }
}
