package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation;

public class BrakeWithABS implements IBrakeBehaviour {
    public void brake() {
        System.out.println("Brake with ABS applied");
    }
}
