package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example1.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example1.implementation.AlertStateContext;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example1.implementation.Silent;

public class State1Client {
    public static void main(String[] args)
    {
        AlertStateContext stateContext = new AlertStateContext();
        stateContext.alert();
        stateContext.alert();
        stateContext.setState(new Silent());
        stateContext.alert();
        stateContext.alert();
        stateContext.alert();
    }
}
