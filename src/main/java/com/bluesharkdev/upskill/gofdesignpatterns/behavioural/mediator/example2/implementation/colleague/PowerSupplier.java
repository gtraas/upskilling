package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague;

public class PowerSupplier {
    public void turnOn() {
        System.out.println("Power turned on");
    }

    public void turnOff() {
        System.out.println("Power turned off");
    }
}
