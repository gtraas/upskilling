package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation;

public class Sedan extends Car {
    public Sedan() {
        super(new Brake());
    }
}
