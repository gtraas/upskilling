package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.mediator;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague.Flight;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague.Runway;

public interface IATCMediator {
    public void registerRunway(Runway runway);

    public void registerFlight(Flight flight);

    public boolean isLandingOk();

    public void setLandingStatus(boolean status);
}
