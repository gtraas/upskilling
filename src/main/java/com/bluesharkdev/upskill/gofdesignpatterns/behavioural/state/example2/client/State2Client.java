package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example2.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example2.implementation.DeliveredState;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example2.implementation.Package;

public class State2Client {

    public static void main(String [] args) {
        Package pkg = new Package();

        pkg.printStatus();
        pkg.nextState();

        pkg.printStatus();
        pkg.nextState();

        pkg.printStatus();

        pkg.setState(new DeliveredState());
        pkg.printStatus();
        pkg.previousState();

        pkg.printStatus();
    }
}
