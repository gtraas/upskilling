package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation;

public abstract class Car {
    private IBrakeBehaviour brakeBehavior;

    public Car(IBrakeBehaviour brakeBehavior) {
        this.brakeBehavior = brakeBehavior;
    }

    public void applyBrake() {
        brakeBehavior.brake();
    }

    public void setBrakeBehavior(IBrakeBehaviour brakeType) {
        this.brakeBehavior = brakeType;
    }
}
