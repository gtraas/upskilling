package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.mediator;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague.Button;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague.Fan;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague.PowerSupplier;

public class Mediator {
    private Button button;
    private Fan fan;
    private PowerSupplier powerSupplier;

    public void setButton(Button button) {
        this.button = button;
        this.button.setMediator(this);
    }

    public void setFan(Fan fan) {
        this.fan = fan;
        this.fan.setMediator(this);
    }

    public void setPowerSupplier(PowerSupplier powerSupplier) {
        this.powerSupplier = powerSupplier;
    }

    public void press() {
        if (fan.isOn()) {
            fan.turnOff();
        } else {
            fan.turnOn();
        }
    }

    public void start() {
        powerSupplier.turnOn();
    }

    public void stop() {
        powerSupplier.turnOff();
    }
}
