# Command Pattern
## Explanation
Suppose you are building a home automation system. There is a programmable remote which can be used to turn on and off 
various items in your home like lights, stereo, AC etc. It looks something like this
```java
if (buttonPressed == button1)
     lights.on();
```

But we need to keep in mind that turning on some devices like a stereo comprises many steps like setting the cd, 
volume etc. Also we can reassign a button to do something else. By using a simple if-else we are coding to implementation 
rather than interface. Also there is tight coupling.

So what we want to achieve is a design that provides loose coupling and the remote control should not have much information 
about a particular device. The command pattern helps us do that.

**Definition**: The command pattern encapsulates a request as an object, thereby letting us parameterize other objects 
with different requests, *queue* or *log* requests, and support undoable operations.

The definition is a bit confusing at first but let’s step through it. In analogy to our problem, the above remote 
control is the client and the stereo, lights etc. are the receivers. In the command pattern there is a Command object 
that encapsulates a request by binding together a set of actions on a specific receiver. It does so by exposing just one 
method *execute()* that causes some actions to be invoked on the receiver.

Parameterizing other objects with different requests in our analogy means that the button used to turn on the lights can
later be used to turn on stereo or maybe open the garage door.

*queue* or *log* requests, and support undoable operations means that Command’s Execute operation can store state for 
reversing its effects in the Command itself. The Command may have an added unExecute operation that reverses the effects 
of a previous call to execute.It may also support logging changes so that they can be reapplied in case of a system crash.

![Class diagram](../../../../../../../resources/images/gofpatterns/command.png)

### Advantages:

- Makes our code extensible as we can add new commands without changing existing code.
- Reduces coupling the invoker and receiver of a command.

### Disadvantages:

- Increase in the number of classes for each individual command