package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague.Button;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague.Fan;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague.PowerSupplier;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.mediator.Mediator;

/*
    Imagine we're building a simple cooling system that consists of a fan, a power supply, and a button.
    Pressing the button will either turn on or turn off the fan. Before we turn the fan on, we need to
    turn on the power. Similarly, we have to turn off the power right after the fan is turned off.
 */
public class MediatorClient {

    public Button button;
    public Fan fan;
    public PowerSupplier powerSupplier;

    public static void main(String [] args) {
        MediatorClient client = new MediatorClient();
        client.button = new Button();
        client.fan = new Fan();
        client.powerSupplier = new PowerSupplier();
        Mediator mediator = new Mediator();

        mediator.setButton(client.button);
        mediator.setFan(client.fan);
        mediator.setPowerSupplier(client.powerSupplier);

        mediator.press();
        mediator.press();
    }
}
