package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.houseobject.Light;

public class LightOffCommand implements Command {
    Light light;
    public LightOffCommand(Light light)
    {
        this.light = light;
    }
    public void execute()
    {
        light.off();
    }
}
