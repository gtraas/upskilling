package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.interpreter.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.interpreter.implementation.AndExpression;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.interpreter.implementation.Expression;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.interpreter.implementation.OrExpression;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.interpreter.implementation.TerminalExpression;

public class InterpreterClient {
    public static void main(String[] args)
    {
        Expression person1 = new TerminalExpression("Kushagra");
        Expression person2 = new TerminalExpression("Lokesh");
        Expression isSingle = new OrExpression(person1, person2);

        Expression vikram = new TerminalExpression("Vikram");
        Expression committed = new TerminalExpression("Committed");
        Expression isCommitted = new AndExpression(vikram, committed);

        System.out.println(isSingle.interpreter("Kushagra"));
        System.out.println(isSingle.interpreter("Lokesh"));
        System.out.println(isSingle.interpreter("Achint"));

        System.out.println(isCommitted.interpreter("Committed, Vikram"));
        System.out.println(isCommitted.interpreter("Single, Vikram"));

    }
}
