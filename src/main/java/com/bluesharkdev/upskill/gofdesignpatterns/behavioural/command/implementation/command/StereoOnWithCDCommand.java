package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.houseobject.Stereo;

public class StereoOnWithCDCommand implements Command {
    Stereo stereo;
    public StereoOnWithCDCommand(Stereo stereo)
    {
        this.stereo = stereo;
    }
    public void execute()
    {
        stereo.on();
        stereo.setCD();
        stereo.setVolume(11);
    }
}
