package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation;

public class SUV extends Car {
    public SUV() {
        super(new BrakeWithABS());
    }
}
