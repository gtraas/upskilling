package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitor.ShoppingCartVisitor;

public interface ItemElement {
    public int accept(ShoppingCartVisitor visitor);
}
