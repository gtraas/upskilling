package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.Document;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.JsonElement;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.XmlElement;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitor.ElementVisitor;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitor.Visitor;

import java.util.UUID;

public class VisitorClient2 {
    public static void main(String[] args) {

        Visitor v = new ElementVisitor();

        Document d = new Document(generateUuid());
        d.elements.add(new JsonElement(generateUuid()));
        d.elements.add(new JsonElement(generateUuid()));
        d.elements.add(new XmlElement(generateUuid()));

        d.accept(v);
    }

    private static String generateUuid() {
        return UUID.randomUUID()
                .toString();
    }
}
