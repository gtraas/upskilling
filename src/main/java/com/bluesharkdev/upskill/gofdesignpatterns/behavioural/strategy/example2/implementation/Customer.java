package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example2.implementation;

import java.util.ArrayList;

public class Customer {
    private final ArrayList<Integer> drinks = new ArrayList<>();
    private BillingStrategy strategy;
    private String name;

    public Customer(BillingStrategy strategy, String name) {
        this.strategy = strategy;
        this.name = name;
    }

    public void add(int price, int quantity) {
        this.drinks.add(this.strategy.getActPrice(price*quantity));
    }

    // Payment of bill
    public void printBill() {
        int sum = this.drinks.stream().mapToInt(v -> v).sum();
        System.out.println("Total due by " + this.name + ": " + sum / 100.0);
        this.drinks.clear();
    }

    // Set Strategy
    public void setStrategy(BillingStrategy strategy) {
        this.strategy = strategy;
    }
}
