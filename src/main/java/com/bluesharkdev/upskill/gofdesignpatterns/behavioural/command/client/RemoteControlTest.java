package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command.LightOffCommand;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command.LightOnCommand;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command.StereoOffCommand;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command.StereoOnWithCDCommand;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.control.SimpleRemoteControl;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.houseobject.Light;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.houseobject.Stereo;

public class RemoteControlTest {
    public static void main(String[] args)
    {
        SimpleRemoteControl remote = new SimpleRemoteControl();
        Light light = new Light();
        Stereo stereo = new Stereo();

        // we can change command dynamically
        remote.setCommand(new LightOnCommand(light));
        remote.buttonWasPressed();
        remote.setCommand(new StereoOnWithCDCommand(stereo));
        remote.buttonWasPressed();
        remote.setCommand(new StereoOffCommand(stereo));
        remote.buttonWasPressed();
        remote.setCommand(new LightOffCommand(light));
        remote.buttonWasPressed();
    }
}
