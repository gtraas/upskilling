package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable;


import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitor.Visitor;

public abstract class Element {

    public String uuid;

    public Element(String uuid) {
        this.uuid = uuid;
    }

    public abstract void accept(Visitor v);

}
