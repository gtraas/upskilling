package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.implementation.NotificationBar;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.implementation.NotificationCollection;

public class IteratorClient {
    public static void main(String args[])
    {
        NotificationCollection nc = new NotificationCollection();
        NotificationBar nb = new NotificationBar(nc);
        nb.printNotifications();
    }
}
