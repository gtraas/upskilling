package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitor.Visitor;

public class JsonElement extends Element {
    public JsonElement(String uuid) {
        super(uuid);
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
