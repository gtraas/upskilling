package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.houseobject;

public class Light {
    public void on()
    {
        System.out.println("Light is on");
    }
    public void off()
    {
        System.out.println("Light is off");
    }
}
