package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.mediator.Mediator;

public class Button {
    private Mediator mediator;

    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    public void press() {
        this.mediator.press();
    }
}
