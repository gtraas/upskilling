package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example1.implementation;

public class Vibration implements MobileAlertState {
    @Override
    public void alert(AlertStateContext ctx)
    {
        System.out.println("vibration...");
    }
}
