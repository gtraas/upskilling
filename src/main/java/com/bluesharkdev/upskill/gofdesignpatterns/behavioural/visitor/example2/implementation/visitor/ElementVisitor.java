package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitor;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.JsonElement;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.XmlElement;

public class ElementVisitor implements Visitor {

    @Override
    public void visit(XmlElement xe) {
        System.out.println("processing xml element with uuid: " + xe.uuid);
    }

    @Override
    public void visit(JsonElement je) {
        System.out.println("processing json element with uuid: " + je.uuid);
    }
}
