package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitor;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.JsonElement;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example2.implementation.visitable.XmlElement;

public interface Visitor {
    void visit(XmlElement xe);

    void visit(JsonElement je);
}
