package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation;

public interface IBrakeBehaviour {
    public void brake();
}
