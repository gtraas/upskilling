package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example2.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example2.implementation.BillingStrategy;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example2.implementation.Customer;

public class StrategyClient2 {

    public static void main(String[] arguments) {
        // Prepare strategies
        BillingStrategy normalStrategy    = BillingStrategy.normalStrategy();
        BillingStrategy happyHourStrategy = BillingStrategy.happyHourStrategy();

        Customer firstCustomer = new Customer(normalStrategy, "Cust1");

        // Normal billing
        firstCustomer.add(100, 1);

        // Start Happy Hour
        firstCustomer.setStrategy(happyHourStrategy);
        firstCustomer.add(100, 2);

        // New Customer
        Customer secondCustomer = new Customer(happyHourStrategy, "Cust2");
        secondCustomer.add(80, 1);
        // The Customer pays
        firstCustomer.printBill();

        // End Happy Hour
        secondCustomer.setStrategy(normalStrategy);
        secondCustomer.add(130, 2);
        secondCustomer.add(250, 1);
        secondCustomer.printBill();
    }

}
