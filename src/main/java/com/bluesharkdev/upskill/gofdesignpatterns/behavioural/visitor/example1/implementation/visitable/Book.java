package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitor.ShoppingCartVisitor;

public class Book implements ItemElement {
    private int price;
    private String isbnNumber;

    public Book(int cost, String isbn)
    {
        this.price=cost;
        this.isbnNumber=isbn;
    }

    public int getPrice()
    {
        return price;
    }

    public String getIsbnNumber()
    {
        return isbnNumber;
    }

    @Override
    public int accept(ShoppingCartVisitor visitor)
    {
        return visitor.visit(this);
    }
}
