package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.Book;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.Fruit;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.ItemElement;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitor.ShoppingCartVisitor;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitor.ShoppingCartVisitorImpl;

public class VisitorClient1 {

    public static void main(String[] args)
    {
        ItemElement[] items = new ItemElement[]{new Book(20, "1234"),
                new Book(100, "5678"), new Fruit(10, 2, "Banana"),
                new Fruit(5, 5, "Apple")};

        int total = calculatePrice(items);
        System.out.println("Total Cost = "+total);
    }

    private static int calculatePrice(ItemElement[] items)
    {
        ShoppingCartVisitor visitor = new ShoppingCartVisitorImpl();
        int sum=0;
        for(ItemElement item : items)
        {
            sum = sum + item.accept(visitor);
        }
        return sum;
    }

}
