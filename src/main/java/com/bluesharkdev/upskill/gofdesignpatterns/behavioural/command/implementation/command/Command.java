package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command;

public interface Command {

    public void execute();
}
