package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague;

public interface Command {
    void land();
}
