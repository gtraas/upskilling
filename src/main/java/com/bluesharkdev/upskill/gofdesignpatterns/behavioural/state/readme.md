# State Pattern
## Explanation

The main idea of State pattern is to allow the object to change its behavior without changing its class. Also, by 
implementing it, the code should remain cleaner without many if/else statements.

Imagine we have a package which is sent to a post office. The package itself can be ordered, then delivered to a post 
office and finally received by a client. Now, depending on the actual state, we want to print its delivery status.

The simplest approach would be to add some boolean flags and apply simple if/else statements within each of our methods 
in the class. That won't complicate it much in a simple scenario. However, it might complicate and pollute our code 
when we'll get more states to process which will result in even more if/else statements.

Besides, all logic for each of the states would be spread across all methods. Now, this is where the State pattern 
might be considered for use. Thanks to the State design pattern, we can encapsulate the logic in dedicated classes, 
apply the Single Responsibility Principle and Open/Closed Principle, have cleaner and more maintainable code.

![Class diagram](../../../../../../../resources/images/gofpatterns/state.png)

In the UML diagram, we see that Context class has an associated State which is going to change during program execution.

Our context is going to delegate the behavior to the state implementation. In other words, all incoming requests will be 
handled by the concrete implementation of the state.

We see that logic is separated and adding new states is simple – it comes down to adding another State implementation if needed.

### Advantages of State Design Pattern

- With the State pattern, the benefits of implementing polymorphic behaviors are evident, and it is also easier to add states to support additional behavior.
- In the State design pattern, an object’s behavior is the result of the function of its state, and the behavior gets changed at runtime depending on the state. This removes the dependency on the if/else or switch/case conditional logic. For example, in the TV remote scenario, we could have also implemented the behavior by simply writing one class and method that will ask for a parameter and perform an action (switch the TV on/off) with an if/else block.
- The State design pattern also improves Cohesion since state-specific behaviors are aggregated into the ConcreteState classes, which are placed in one location in the code.


### Disadvantages of State Design Pattern

- A State pattern drawback is the payoff when implementing transition between the states. That makes the state hardcoded, which is a bad practice in general. But, depending on our needs and requirements, that might or might not be an issue.

### State vs. Strategy Pattern
Both design patterns are very similar, but their UML diagram is the same, with the idea behind them slightly different.

First, the strategy pattern defines a family of interchangeable algorithms. Generally, they achieve the same goal, but with a different implementation, for example, sorting or rendering algorithms.

In state pattern, the behavior might change completely, based on actual state.

Next, in strategy, the client has to be aware of the possible strategies to use and change them explicitly. Whereas in state pattern, each state is linked to another and create the flow as in Finite State Machine.