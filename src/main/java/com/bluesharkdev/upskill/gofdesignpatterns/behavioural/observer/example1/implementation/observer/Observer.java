package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.observer;

public interface Observer {
    public void update(int runs, int wickets, float overs);
}
