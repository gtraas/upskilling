package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.observer.AverageScoreDisplay;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.observer.CurrentScoreDisplay;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.subject.CricketData;

public class Observer1Client {
    public static void main(String args[])
    {
        // create objects for testing
        AverageScoreDisplay averageScoreDisplay = new AverageScoreDisplay();
        CurrentScoreDisplay currentScoreDisplay = new CurrentScoreDisplay();

        // pass the displays to Cricket data
        CricketData cricketData = new CricketData();

        // register display elements
        cricketData.registerObserver(averageScoreDisplay);
        cricketData.registerObserver(currentScoreDisplay);

        // in real app you would have some logic to
        // call this function when data changes
        cricketData.dataChanged();

        //remove an observer
        cricketData.unregisterObserver(averageScoreDisplay);

        // now only currentScoreDisplay gets the
        // notification
        cricketData.dataChanged();

    }
}
