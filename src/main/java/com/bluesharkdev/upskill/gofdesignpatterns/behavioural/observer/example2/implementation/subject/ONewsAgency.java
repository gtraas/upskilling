package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.subject;

import java.util.Observable;

/*
    Option 2: java.util.Observer
 */
public class ONewsAgency extends Observable {
    private String news;

    public void setNews(String news) {
        this.news = news;
        setChanged();
        notifyObservers(news);
    }
}
