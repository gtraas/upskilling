package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitor;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.Book;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.Fruit;

public class ShoppingCartVisitorImpl implements ShoppingCartVisitor {
    @Override
    public int visit(Book book)
    {
        int cost=0;
        //apply 5$ discount if book price is greater than 50
        if(book.getPrice() > 50)
        {
            cost = book.getPrice()-5;
        }
        else
            cost = book.getPrice();

        System.out.println("Book ISBN::"+book.getIsbnNumber() + " cost ="+cost);
        return cost;
    }

    @Override
    public int visit(Fruit fruit)
    {
        int cost = fruit.getPricePerKg()*fruit.getWeight();
        System.out.println(fruit.getName() + " cost = "+cost);
        return cost;
    }
}
