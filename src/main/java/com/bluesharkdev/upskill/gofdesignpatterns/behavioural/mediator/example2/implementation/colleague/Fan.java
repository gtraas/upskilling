package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.colleague;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example2.implementation.mediator.Mediator;

public class Fan {
    private Mediator mediator;
    private boolean isOn = false;

    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    public boolean isOn() {
        return isOn;
    }

    public void turnOn() {
        this.mediator.start();
        System.out.println("Fan turned on");
        isOn = true;
    }

    public void turnOff() {
        isOn = false;
        System.out.println("Fan turned off");
        this.mediator.stop();
    }
}
