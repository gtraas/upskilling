package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.chain.Chain;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.chain.NegativeProcessor;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.chain.PositiveProcessor;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.chain.ZeroProcessor;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.model.ProcessableNumber;

public class TestChain {
    public static void main(String[] args) {
        //configure Chain of Responsibility
        Chain c1 = new NegativeProcessor();
        Chain c2 = new ZeroProcessor();
        Chain c3 = new PositiveProcessor();
        c1.setNext(c2);
        c2.setNext(c3);

        //calling chain of responsibility
        c1.process(new ProcessableNumber(90));
        c1.process(new ProcessableNumber(-50));
        c1.process(new ProcessableNumber(0));
        c1.process(new ProcessableNumber(91));
    }
}
