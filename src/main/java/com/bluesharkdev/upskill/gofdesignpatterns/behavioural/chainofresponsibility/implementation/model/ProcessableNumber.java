package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.model;

public class ProcessableNumber {
    private int number;

    public ProcessableNumber(int number)
    {
        this.number = number;
    }

    public int getNumber()
    {
        return number;
    }
}
