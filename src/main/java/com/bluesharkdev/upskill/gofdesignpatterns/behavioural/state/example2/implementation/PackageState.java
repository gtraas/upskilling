package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example2.implementation;

public interface PackageState {
    void next(Package pkg);

    void prev(Package pkg);

    void printStatus();
}
