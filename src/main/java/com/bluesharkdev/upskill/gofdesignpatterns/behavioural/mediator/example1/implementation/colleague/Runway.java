package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.mediator.IATCMediator;

public class Runway implements Command {
    private IATCMediator atcMediator;

    public Runway(IATCMediator atcMediator)
    {
        this.atcMediator = atcMediator;
        atcMediator.setLandingStatus(true);
    }

    @Override
    public void land()
    {
        System.out.println("Landing permission granted.");
        atcMediator.setLandingStatus(true);
    }
}
