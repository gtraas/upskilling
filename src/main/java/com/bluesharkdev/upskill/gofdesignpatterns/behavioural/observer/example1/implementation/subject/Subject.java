package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.subject;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.observer.Observer;

public interface Subject {
    public void registerObserver(Observer o);
    public void unregisterObserver(Observer o);
    public void notifyObservers();
}
