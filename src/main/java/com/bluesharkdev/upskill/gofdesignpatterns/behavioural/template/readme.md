# Template Pattern
## Explanation
Template method design pattern defines an algorithm as a skeleton of operations and leaves the details to be implemented 
by the child classes. The overall structure and sequence of the algorithm is preserved by the parent class.
In the template method pattern,we have a preset structure method called the template method which consists of steps. 
These steps can be an abstract method which will be implemented by its subclasses.

This behavioral design pattern is one of the easiest to understand and implement. This design pattern is used popularly 
in framework development. This helps to avoid code duplication also.

![Class diagram](../../../../../../../resources/images/gofpatterns/template.jpg)

- **AbstractClass** contains the templateMethod() which should be made final so that it cannot be overridden. This 
template method makes use of other operations available in order to run the algorithm but is decoupled for the actual 
implementation of these methods. All operations used by this template method are made abstract, so their implementation 
is deferred to subclasses.
- **ConcreteClass** implements all the operations required by the templateMethod that were defined as abstract in the 
parent class. There can be many different ConcreteClasses.

When to use template method

The template method is used in frameworks, where each implements the invariant parts of a domain’s architecture, 
leaving “placeholders” for customization options.

The template method is used for the following reasons :

- Let subclasses implement varying behavior (through method overriding)
- Avoid duplication in the code , the general workflow structure is implemented once in the abstract class’s algorithm,
and necessary variations are implemented in the subclasses.
- Control at what points subclassing is allowed. As opposed to a simple polymorphic override, where the base method 
would be entirely rewritten allowing radical change to the workflow, only the specific details of the workflow are allowed to change.

### Examples in the Java Language
This pattern is widely used in the Java core libraries, for example by java.util.AbstractList, or java.util.AbstractSet.

For instance, Abstract List provides a skeletal implementation of the List interface.

An example of a template method can be the addAll() method, although it's not explicitly defined as final:
```java
public boolean addAll(int index, Collection<? extends E> c) {
    rangeCheckForAdd(index);
    boolean modified = false;
    for (E e : c) {
        add(index++, e);
        modified = true;
    }
    return modified;
}
 ```
Users only need to implement the add() method:

```java
public void add(int index, E element) {
    throw new UnsupportedOperationException();
}
```
Here, it's the responsibility of the programmer to provide an implementation for adding an element to the list at the 
given index (the variant part of the listing algorithm).