# Memento Pattern
## Explanation
The Memento pattern is used to restore the state of an object to a previous state. As your application is progressing, you may want to save checkpoints in your application and restore back to those checkpoints later.

![Class diagram](../../../../../../../resources/images/gofpatterns/memento.png)

### Design components

- **originator**: the object for which the state is to be saved. It creates the memento and uses it in future to undo.
- **memento**: the object that is going to maintain the state of originator. Its just a POJO.
- **caretaker**: the object that keeps track of multiple memento. Like maintaining savepoints.
A Caretaker would like to perform an operation on the Originator while having the ability to rollback. The caretaker calls the createMemento() method on the originator asking the originator to pass it a memento object. At this point the originator creates a memento object saving its internal state and passes the memento to the caretaker. The caretaker maintains the memento object and performs the operation. In case of the need to undo the operation, the caretaker calls the setMemento() method on the originator passing the maintained memento object. The originator would accept the memento, using it to restore its previous state.

### Advantage

We can use Serialization to achieve memento pattern implementation that is more generic rather than Memento pattern where every object needs to have it’s own Memento class implementation.

### Disadvantage

If Originator object is very huge then Memento object size will also be huge and use a lot of memory.








