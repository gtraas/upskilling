package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.implementation;

import java.util.Map;

public class StandardComputer extends Computer {
    public StandardComputer(Map<String, String> computerParts) {
        super(computerParts);
    }
}
