package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.command;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.command.implementation.houseobject.Stereo;

public class StereoOffCommand implements Command {
    Stereo stereo;
    public StereoOffCommand(Stereo stereo)
    {
        this.stereo = stereo;
    }
    public void execute()
    {
        stereo.off();
    }
}
