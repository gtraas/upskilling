package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.implementation;

public interface Collection {
    public Iterator createIterator();
}
