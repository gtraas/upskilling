package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.implementation;

public class Notification {
    String notification;

    public Notification(String notification)
    {
        this.notification = notification;
    }
    public String getNotification()
    {
        return notification;
    }
}
