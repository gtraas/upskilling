package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.mediator.IATCMediator;

public class Flight implements Command {
    private IATCMediator atcMediator;

    public Flight(IATCMediator atcMediator)
    {
        this.atcMediator = atcMediator;
    }

    public void land()
    {
        if (atcMediator.isLandingOk())
        {
            System.out.println("Successfully Landed.");
            atcMediator.setLandingStatus(true);
        }
        else
            System.out.println("Waiting for landing.");
    }

    public void getReady()
    {
        System.out.println("Ready for landing.");
    }
}
