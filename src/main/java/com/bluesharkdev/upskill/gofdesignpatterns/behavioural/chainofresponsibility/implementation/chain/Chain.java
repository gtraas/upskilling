package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.chain;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.model.ProcessableNumber;

public interface Chain {
    public abstract void setNext(Chain nextInChain);
    public abstract void process(ProcessableNumber request);
}
