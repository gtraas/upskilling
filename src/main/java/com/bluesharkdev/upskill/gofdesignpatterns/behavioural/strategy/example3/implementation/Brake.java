package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation;

public class Brake implements IBrakeBehaviour {
    public void brake() {
        System.out.println("Simple Brake applied");
    }
}
