# Mediator Pattern
## Explanation
The Mediator design pattern is one of the most important and widely used behavioral design patterns. The Mediator enables 
decoupling of objects by introducing a layer inbetween so that the interaction between objects happens via the layer. 
If the objects interact with each other directly, the system components are tightly-coupled with each other and this 
makes for higher maintainability costs and difficult extension. The Mediator pattern focuses on providing a mediator 
between objects for communication and help in implementing loose coupling between objects.

An air traffic controller is a great example of the mediator pattern where the airport control room works as a mediator 
for communication between different flights. The mediator works as a router between objects, and it can have its own 
logic to provide a way of communication.

![Class diagram](../../../../../../../resources/images/gofpatterns/mediator.png)

### Design components

- **Mediator**:It defines the interface for communication between colleague objects.
- **ConcreteMediator** : It implements the mediator interface and coordinates communication between colleague objects.
- **Colleague** : It defines the interface for communication with other colleagues
- **ConcreteColleague** : It implements the colleague interface and communicates with other colleagues through its mediator








