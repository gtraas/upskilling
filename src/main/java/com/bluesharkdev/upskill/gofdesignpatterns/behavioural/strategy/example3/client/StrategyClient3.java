package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation.Brake;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation.Car;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation.SUV;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.strategy.example3.implementation.Sedan;

public class StrategyClient3 {
    public static void main(String [] args) {
        Car sedanCar = new Sedan();
        sedanCar.applyBrake();  // This will invoke class "Brake"

        Car suvCar = new SUV();
        suvCar.applyBrake();    // This will invoke class "BrakeWithABS"

        // set brake behavior dynamically
        suvCar.setBrakeBehavior( new Brake() );
        suvCar.applyBrake();    // This will invoke class "Brake"
    }
}
