package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.state.example1.implementation;

public interface MobileAlertState {
    public void alert(AlertStateContext ctx);
}
