# Strategy Pattern
## Explanation
The strategy pattern (also known as the policy pattern) is a software design pattern that enables an algorithm’s 
behavior to be selected at runtime. The strategy pattern

- defines a family of algorithms,
- encapsulates each algorithm, and
- makes the algorithms interchangeable within that family.

![Class diagram](../../../../../../../resources/images/gofpatterns/strategy.png)

Here we rely on composition instead of inheritance for reuse. Context is composed of a Strategy. Instead of implementing 
a behavior the Context delegates it to Strategy. The context would be the class that would require changing behaviors. 
We can change behavior dynamically. Strategy is implemented as interface so that we can change behavior without affecting 
our context.

### Advantages:

- A family of algorithms can be defined as a class hierarchy and can be used interchangeably to alter application behavior without changing its architecture.
- By encapsulating the algorithm separately, new algorithms complying with the same interface can be easily introduced.
- The application can switch strategies at run-time.
- Strategy enables the clients to choose the required algorithm, without using a “switch” statement or a series of “if-else” statements.
- Data structures used for implementing the algorithm are completely encapsulated in Strategy classes. Therefore, the implementation of an algorithm can be changed without affecting the Context class.

With Java 8 lambdas and function composition, the strategy pattern becomes compact. See example 1.

Example 2 and 3 are further examples of leveraging Java 8 lambdas for the strategy pattern