# Iterator Pattern
## Explanation
The Iterator Pattern is a relatively simple and frequently used design pattern. There are a lot of data 
structures/collections available in every language. Each collection must provide an iterator that lets it iterate through 
its objects. However, while doing so it should make sure that it does not expose its implementation.
Suppose we are building an application that requires us to maintain a list of notifications. Eventually, some part of 
your code will need to iterate over all notifications. If we implemented your collection of notifications as an array 
you would iterate over them as follows:

```java
// If a simple array is used to store notifications
for (int i = 0; i < notificationList.length; i++)
     Notification notification = notificationList[i]);

// If an ArrayList is used, then we would iterate over them like this:
for (int i = 0; i < notificationList.size(); i++)
    Notification notification = (Notification)notificationList.get(i); 
```

The Iterator pattern lets us do just that. Formally it is defined as follows:
**The iterator pattern provides a way to access the elements of an aggregate object without exposing its underlying representation.**

![Class diagram](../../../../../../../resources/images/gofpatterns/iterator.png)

Here we have a common interface, Aggregate, for the client as it decouples it from the implementation of your collection 
of objects. The ConcreteAggregate implements createIterator() that returns an iterator for its collection. Each 
ConcreteAggregate’s responsibility is to instantiate a ConcreteIterator that can iterate over its collection of objects. 
The iterator interface provides a set of methods for traversing or modifying the collection that can, in addition to 
next()/hasNext(), also provide functions for search, remove etc.

Let’s understand this through an example. Suppose we are creating a notification bar in our application that displays 
all the notifications which are held in a notification collection. NotificationCollection provides an iterator to 
iterate over its elements without exposing how it has implemented the collection (array in this case) to the Client (NotificationBar).

The class diagram would be:
![Class diagram](../../../../../../../resources/images/gofpatterns/iteratorexample.png)






