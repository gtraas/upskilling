package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.implementation;

//we could just use java.util.Iterator as well
public interface Iterator {
    // indicates whether there are more elements to iterate over
    boolean hasNext();

    // returns the next element
    Object next();
}
