package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.implementation;

import java.util.Map;

public class HighEndComputer extends Computer {
    public HighEndComputer(Map<String, String> computerParts) {
        super(computerParts);
    }
}
