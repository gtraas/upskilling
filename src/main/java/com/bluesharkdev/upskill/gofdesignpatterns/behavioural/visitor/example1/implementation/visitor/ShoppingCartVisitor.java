package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitor;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.Book;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.visitor.example1.implementation.visitable.Fruit;

public interface ShoppingCartVisitor {
    int visit(Book book);
    int visit(Fruit fruit);
}
