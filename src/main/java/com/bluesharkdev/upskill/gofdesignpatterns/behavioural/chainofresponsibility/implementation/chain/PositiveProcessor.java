package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.chain;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.chainofresponsibility.implementation.model.ProcessableNumber;

public class PositiveProcessor implements Chain {
    private Chain nextInChain;

    public void setNext(Chain c)
    {
        nextInChain = c;
    }

    public void process(ProcessableNumber request)
    {
        if (request.getNumber() > 0)
        {
            System.out.println("PositiveProcessor : " + request.getNumber());
        }
        else
        {
            nextInChain.process(request);
        }
    }
}
