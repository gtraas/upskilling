package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.observer.NewsChannel;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.observer.ONewsChannel;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.observer.PCLNewsChannel;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.subject.NewsAgency;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.subject.ONewsAgency;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.subject.PCLNewsAgency;

public class Observer2Client {

    public static void main(String [] args) {
        Observer2Client.doOption1();
        Observer2Client.doOption2();
        Observer2Client.doOption3();
    }

    private static void doOption1() {

        NewsAgency observable = new NewsAgency();
        NewsChannel observer = new NewsChannel();

        observable.addObserver(observer);

        observable.setNews("news - option 1 works!");
        System.out.println(observer.getNews());
    }

    private static void doOption2() {
        ONewsAgency observable = new ONewsAgency();
        ONewsChannel observer = new ONewsChannel();

        observable.addObserver(observer);

        observable.setNews("news - option 2 also works");
        System.out.println(observer.getNews());
    }

    private static void doOption3() {
        PCLNewsAgency observable = new PCLNewsAgency();
        PCLNewsChannel observer = new PCLNewsChannel();

        observable.addPropertyChangeListener(observer);

        observable.setNews("news - option 3 is probably the best option");
        System.out.println(observer.getNews());
    }
}
