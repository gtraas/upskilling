package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example2.implementation.observer;

//option 1 - scratch-built
public interface Channel {
    public void update(Object o);
}
