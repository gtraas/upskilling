package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.interpreter.implementation;

public interface Expression {
    boolean interpreter(String con);
}
