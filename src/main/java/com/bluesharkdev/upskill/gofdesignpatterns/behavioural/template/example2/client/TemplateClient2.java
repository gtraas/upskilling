package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.implementation.Computer;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.implementation.ComputerBuilder;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.implementation.HighEndComputerBuilder;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.template.example2.implementation.StandardComputerBuilder;

public class TemplateClient2 {
    public static void main(String [] args) {
        ComputerBuilder standardComputerBuilder = new StandardComputerBuilder();
        Computer standardComputer = standardComputerBuilder.buildComputer();
        standardComputer.getComputerParts().forEach((k, v) -> System.out.println("Part : " + k + " Value : " + v));

        ComputerBuilder highEndComputerBuilder = new HighEndComputerBuilder();
        Computer highEndComputer = highEndComputerBuilder.buildComputer();
        highEndComputer.getComputerParts().forEach((k, v) -> System.out.println("Part : " + k + " Value : " + v));
    }
}
