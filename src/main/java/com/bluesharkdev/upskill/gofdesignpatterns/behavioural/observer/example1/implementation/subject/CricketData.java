package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.subject;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.observer.example1.implementation.observer.Observer;

import java.util.ArrayList;
import java.util.Iterator;

public class CricketData implements Subject {

    int runs = 90;
    int wickets = 2;
    float overs = 10;
    ArrayList<Observer> observerList;

    public CricketData() {
        observerList = new ArrayList<Observer>();
    }

    @Override
    public void registerObserver(Observer o) {
        observerList.add(o);
    }

    @Override
    public void unregisterObserver(Observer o) {
        observerList.remove(observerList.indexOf(o));
    }

    @Override
    public void notifyObservers()
    {
        for (Iterator<Observer> it =
             observerList.iterator(); it.hasNext();)
        {
            Observer o = it.next();
            o.update(runs,wickets,overs);
        }
    }

    // get latest runs from stadium
    private int getLatestRuns()
    {
        int temp = runs;
        runs += 1;
        return temp;
    }

    // get latest wickets from stadium
    private int getLatestWickets()
    {
        return wickets;
    }

    // get latest overs from stadium
    private float getLatestOvers()
    {
        float temp = overs;
        overs += 0.1;
        return temp;
    }

    // This method is used update displays
    // when data changes
    public void dataChanged()
    {
        //get latest data
        runs = getLatestRuns();
        wickets = getLatestWickets();
        overs = getLatestOvers();

        notifyObservers();
    }
}
