package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.client;

import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague.Flight;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.colleague.Runway;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.mediator.ATCMediator;
import com.bluesharkdev.upskill.gofdesignpatterns.behavioural.mediator.example1.implementation.mediator.IATCMediator;

public class MediatorClient {
    public static void main(String args[])
    {

        IATCMediator atcMediator = new ATCMediator();
        Flight sparrow101 = new Flight(atcMediator);
        Runway mainRunway = new Runway(atcMediator);
        atcMediator.registerFlight(sparrow101);
        atcMediator.registerRunway(mainRunway);
        sparrow101.getReady();
        mainRunway.land();
        sparrow101.land();

    }
}
