package com.bluesharkdev.upskill.gofdesignpatterns.behavioural.iterator.implementation;

//Contains collection of notifications as an object of NotificationCollection
//This is the client of the Iterator pattern
public class NotificationBar {
    NotificationCollection notifications;

    public NotificationBar(NotificationCollection notifications)
    {
        this.notifications = notifications;
    }

    public void printNotifications()
    {
        Iterator iterator = notifications.createIterator();
        System.out.println("-------NOTIFICATION BAR------------");
        while (iterator.hasNext())
        {
            Notification n = (Notification) iterator.next();
            System.out.println(n.getNotification());
        }
    }
}
