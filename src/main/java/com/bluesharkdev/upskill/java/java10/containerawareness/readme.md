# Java 10 New Features
## Container awareness

- JVMs are now aware of when they're being run in a container
- will extract container-specific config instead of querying the operating system itself
- applies to data like number of CPUs and total memory allocated to the container
- only available for the Linux systems
- can be disabled from the command line with JVM option 
```
-XX:-UseContainerSupport
```
- change also adds a JVM option that provides an ability to specify the number of CPUs that the JVM will use:
```
-XX:ActiveProcessorCount=count
```
- 3 new JVM options have been added to allow Docker container users more fine-grained control over the amount of system memory that will be used for the Java heap
```
-XX:InitialRAMPercentage
-XX:MaxRAMPercentage
-XX:MinRAMPercentage
```