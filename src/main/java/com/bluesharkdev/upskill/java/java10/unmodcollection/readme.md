# Java 10 New Features
## Unmodifiable Collections
### *copyOf()*

- *java.util.List*, *java.util.Map* and java,util.Set* each get a new static method *copyOf(Collection)*
- returns an unmodifiable copy of the given Collection
```java
List<Integer> copyList = List.copyOf(someList);
copyList.add(4); //throws UnsupportedOperationException
```

### *toUnmodifiable()*

- *java.util.stream.Collectors* get additional methods to collect a *Stream* into an unmodifiable List, Set or Map
```java
List<Integer> evenList = someIntList.stream()
        .filter(i -> i % 2 == 0)
        .collect(Collectors.toUnmodifiableList());
evenList.add(4); //throws UnsupportedOperationException
```
