# Java 10 New Features
## Local Variable type inference

- until Java 9, we had to mention the type of a local variable explicitly and ensure it was compatible with the initialiser used to initialise it
```java
String message = "Java 10 var";
``` 
- In Java 10, we can now do this:
```java
var message = "Java 10";
```
- the compiler infers the type of the variable from the type of the initialiser on the right hand side
- in the above example, the type of *message* would be String
- this is only available for local variables
- not valid for member variables, method parameters or return types
- helps in reducing boilerplate code
```java
Map<Integer, String> map = new HashMap<>();
```
can be written as
```java
var idToNameMap = new HashMap<Integer, String>();
```
- also helps to focus on the name of the variable
- var is not a keyword, but a reserved type name

  - ensures backwards compatibility for programs using *var* as a function or variable name

### Illegal use of *var*

- *var* won't work without the initialiser
```java
var n; //error: cannot use 'var' on variable without initialiser
```
- won't work if initialised with null either
```java
var emptyList = null; //error: variable initialiser is 'null'
```
- won't work for non-local variables
```java
public var greeting = "hello"; //error: 'var' is not allowed here
```
- Lambda expressions need explicit target types, so *var* can't be used
```java
var p = (String s) -> s.length() > 10; //error: lambda expression needs an explicit target type
```
- same for the array initialiser
```java
var arr = {1, 2, 3}; //error: array initialiser needs an explicit type
```

### Guidelines for using *var*

- there are situations where *var* can be used, but probably shouldn't be. 
- code may become less readable
```java
var result = obj.process();
```
- use of *var* here is legal, but it is difficult to understand the type returned by *process()*, making the code less readable
- another situation is where we have streams with a long pipeline
```java
var x = emp.getProjects().stream()
        .findFirst()
        .map(String::length)
        .orElse(0);
```
- usage of var may also give unexpected results
- if we use it with the diamond operator
```java
var emptyList = new ArrayList<>();
```
- type of *emptyList* is now *ArrayList< Object >* rather than *List< Object >*
- if we want it to be ArrayList<Employee>, we need to be explicit
```java
var emptyList = new ArrayList<Employee>();
```
- using *var* with non-denotable types can lead to unexpected errors
- eg, if we use *var* with an anonymous inner class
```java
var obj = new Object() {};
```
- when we try to assign another Object to obj, we'll get an error
```java
obj = new Object(); //error: Object cannot be converted to <anonymous Object>
```
