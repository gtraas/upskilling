# Java 10 New Features
## Changes to Optional

- *java.util.Optional*, *java.util.OptionalDouble*, *java.util.OptionalInt* and *java.util.OptionalLong* each got a new method *orElseThrow()* which takes no argument and throws a *NoSuchElementException* if no value is present
```java
Integer firstEven = someIntList.stream()
        .filter(i -> i % 2 == 0)
        .findFirst()
        .orElseThrow();
```
- it is synonymous with, and now the preferred alternative to the *get()* method.