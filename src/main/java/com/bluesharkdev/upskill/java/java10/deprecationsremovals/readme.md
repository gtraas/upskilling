# Java 10 New Features
## Deprecations and removals
### Command Line Options and Tools

- the tool *javah* has been removed, which generated C headers and source files which were required to implement native methods
- *javac -h* can be used instead
- *policytool* was the UI-based tool for policy file creation and management
- this has been removed, and users can use a simple text editor for this action
- removed *java -Xprof* option.
- was used to profile the running program and send profiling data to stdout.
- users should use the *jmap* tool instead

### Deprecations

- the *java.security.acl* package has been marked for removal in a future version
- has been replaced by *java.security.Policy* and related classes
- *java.security.{Certificate,Identity,IdentityScope,Signer}* are marked for removal