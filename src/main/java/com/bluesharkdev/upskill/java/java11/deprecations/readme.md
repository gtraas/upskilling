# Java 11 New Features
## Removed and deprecated features
### Java EE and CORBA

- java SE doesn't need to include the EE components since they are available elsewhere
- in 11, it has been completely removed
  - Java API for XML-based Web Services (*java.xml.ws*)
  - Java Architecture for XML binding (*java.xml.bind*)
  - JavaBeans Activation Framework (*java.activation*)
  - Common Annotations (*java.xml.ws.annotation*)
  - Common Object Request Broker Architecture (*java.corba*)
  - JavaTransaction API (*java.transaction*)

### JMC and JavaFX

- JDK Mission Control is no longer included in the JDK
  - a standalone version is available
- Same is true of JavaFX modules
  - available as a separate set of modules outside of the JDK

### Deprecated modules

- Nashorn Javascript engine including the JJS tool
- Pack200 compression scheme for JAR files