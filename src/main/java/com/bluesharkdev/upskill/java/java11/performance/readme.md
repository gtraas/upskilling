# Java 11 New Features
## Performance enhancements
### No-op Garbage Collector

- new garbage collector called *Epsilon* is available as experimental
- it's called no-op because it allocates memory but does not actually collect any garbage.
- Epsilon is therefore applicable for simulating out of memory errors
- won't be suitable for production
- useful for
  - performance testing
  - memory pressure testing
  - VM interface testing
  - extremely short-lived jobs
- use *-XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC* to enable it

### Flight Recorder

- the Java Flight Recorder (JFR) is now available in OpenJDK
- profiling tool used to gather diagnostics and profiling data from a running applicaiton
- to start a 120 second flight record, we use:
  - *-XX:StartFlightRecording=duration=120s,settings=profile,filename=java-demo-app.jfr*
- can use JFR in prod since its performance is below 1%
- once time elapses, the recorded data is stored in the specified file
- in order to analyse and visualise the data, need to use another tool called JDK Mission Control
