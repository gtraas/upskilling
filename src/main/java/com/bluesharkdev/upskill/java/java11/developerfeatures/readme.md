# Java 11 New Features
## Developer Features
### New String methods

- new String methods have been added
- *isBlank*, *lines*, *strip*, *stripLeading*, *stripTrailing* and *repeat*
- methods remove a lot of boilerplate code
- strip and its associated methods are similar to *trim()*, but have finer control and Unicode support

### New File methods

- now easier to read and write strings to and from files using the new static methods
- *Files.writeString()* and *Files.readString()*
```java
Path filePath = Files.writeString(Files.createTempFile(tempDir, "demo", ".txt"), "Sample text");
String fileContent = Files.readString(filePath);
```

### Collection to an Array

- *java.util.Collection*interface has a new default *toArray* method which takes an *IntFunction* argument.
- makes it easier to create an array of the right type from a collection
```java
List sampleList = Arrays.asList("Java", "Kotlin");
String[] sampleArray = sampleList.toArray(String[]::new);
```

### The Not Predicate Method

- a static *not* method has been added to the *Predicate* interface
- can use it to negate an existing predicate, much like the *negate* method
```java
List<String> sampleList = Arrays.asList("Java", "\n \n", "Kotlin", " ");
List withoutBlanks = sampleList.asStream()
        .filter(Predicate.not(String::isBlank))
        .collect(Collectors.toList());
assertThat(withoutBlanks).containsExactly("Java", "Kotlin");
```

### Local-variable syntax for Lambda

- support for using local variable syntax was added in 11
- can make use of this feature to apply modifiers to our local variables, for example:
```java
List<String> sampleList = Arrays.asList("Java", "Kotlin");
String resultString = sampleList.stream()
        .map((@Nonnull var x) -> x.toUpperCase())
        .collect(Collectors.joining(", "));
assertThat(resultString).isEqualTo("JAVA, KOTLIN");
```

### Http Client

- the new HttpClient from Java 9 has become a standard feature now in Java 11
- improves overall performance and provides support for both Http/1.1 and Http/2
```java
HttpClient client = HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP-2)
        .connectTimeout(Duration.ofSeconds(20))
        .build()

HttpRequest request = HttpRequest.newBuilder()
        .GET()
        .uri(URI.create("http://localhost:" + port))
        .build();

HttpResponse response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
```

### Nest based access control

- 11 introduces the notion of nestmates and the associated rules within the JVM
- a nest of classes implies both the outer/main class and all its nested classes
```java
assertThat(MainClass.class.isNestmateOf(MainClass.NestedClass.class)).isTrue();
```

- nested classes are linked to the *NestMembers* attribute, while the outer class is linked to the *NestHost* attribute
```java
assertThat(MainClass.NestedClass.class.getNestHost()).isEqualTo(Main.class);
```

- previously, JVM access allowed access to private members between nestmates, but the reflection API denied the same access
- 11 fixes this issue, and provides means to query the new class file attributes using the reflection API
```java
Set<String> nestedMembers = Arrays.asStream(MainClass.NestedClass.class.getNestMembers())
        .map(Class::getName)
        .collect(Collectors.toSet());
```

### Running java files

- we no longer need to use javac to compile
- we can just use java
- so instead of 
```java
$ javac HelloWorld.java
$ java HelloWorld
Hello Java 8!
```
- we can use
```java
$ java HelloWorld.java
Hello Java 11
```
