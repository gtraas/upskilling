package com.bluesharkdev.upskill.java.java11.developerfeatures;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DeveloperFeatures {

    public static void main(String[] args) {
        System.out.println("New String methods");
        System.out.println("Lines example");
        String lines = "This is \n an example \n \n \n of \n a string with many \n lines";
        List<String> list = lines.lines()
                .filter(line -> !line.isBlank())
                .map(String::strip)
                .collect(Collectors.toList());
        System.out.println("Should be 5 lines, are : " + list.size());


        System.out.println("isBlank example");
        String s = "";
        System.out.println("s == \"\". s.isBlank = " + s.isBlank());
        s = "    ";
        System.out.println("s == \"    \". s.isBlank = " + s.isBlank());

        System.out.println("Strip example");

        s = "  hello  ";
        System.out.println("s == \"  hello  \". s.strip contains blank spaces? = " + s.strip().contains(" "));
        System.out.println("s == \"  hello  \". s.stripTrailing starts with blank spaces? = " + s.stripTrailing().startsWith(" "));
        System.out.println("s == \"  hello  \". s.stripTrailing ends with blank spaces? = " + s.stripTrailing().endsWith(" "));

        System.out.println("s == \"  hello  \". s.stripLeading starts with blank spaces? = " + s.stripLeading().startsWith(" "));
        System.out.println("s == \"  hello  \". s.stripLeading ends with blank spaces? = " + s.stripLeading().endsWith(" "));

        System.out.println("Repeat example");
        s = "abc";
        System.out.println("s == abc. s.repeat(2) = " + s.repeat(2));

        System.out.println("\nPredicate not method");
        List<String> sampleList = Arrays.asList("Java", "Scala", "Kotlin", " ");
        Integer size = sampleList.stream()
                .filter(Predicate.not(String::isBlank))
                .collect(Collectors.toList())
                .size();

        System.out.println("Size of new list should be 3, is " + size);

        System.out.println("\nLocal variable support within lambdas");
        sampleList = Arrays.asList("Java", "Kotlin");
        String resultString = sampleList.stream()
                .map((@Nonnull var x) -> x.toUpperCase())
                .collect(Collectors.joining(", "));
        System.out.println("resultString should be \"JAVA, KOTLIN\", is " + resultString);

        System.out.println("\nNest based access control");

        System.out.println("Is DeveloperFeatures a nest mate of NestedNestedClass? : " + DeveloperFeatures.class.isNestmateOf(NestedClass.NestedNestedClass.class));
        System.out.println("Is DeveloperFeatures the nestHost of NestedNestedClass? : " + NestedClass.NestedNestedClass.class.getNestHost().toString());

    }

    class NestedClass {

        class NestedNestedClass {

        }

    }
}
