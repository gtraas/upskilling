package com.bluesharkdev.upskill.java.streams.collectors.groupingby;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.IntSummaryStatistics;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BlogPost {
    String title;
    String author;
    BlogPostType type;
    int likes;
    record AuthPostTypeLikes(String author, BlogPostType type, int likes) {};
    record PostCountTitleLikesStatistics(long count, String titles, IntSummaryStatistics stats) {};
    record TitlesBoundedSumOfLikes(String titles, int boundedSumOfLikes) {};

}
