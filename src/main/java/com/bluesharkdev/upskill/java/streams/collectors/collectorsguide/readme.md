# A guide to collectors
## Collectors

- assume we've done a static import of *import static java.util.stream.Collectors.**
- in the examples that follow, we're using the following list as a source for our stream

```java
List<String> givenList = Arrays.asList("a", "bb", "ccc", "dd");
```

### Collectors.toList()

- can be used for collecting all elements in the Stream into a List instance
- we don't have to provide any particular List implementation with this method
- if we want more control over the specific List implementation, we can use *toCollection()*

```java
List<String> result = givenList.stream().collect(toList());
```

#### Collectors.toUnmodifiableList

- in Java 10, we were able to accumulate Stream elements into an unmodifiable list

```java
List<String> result = givenList.stream().collect(toUnmodifiableList());
```

- if we try to modify the list now, we'll get an *UnsupportedOperationException*

### Collectors.toSet

- can be used for collecting all elements to a Set
- again, we don'ts have control over the particular Set implementation
- also, in Java, a *toUnmodifiableSet* was introduced with behaviour the same as List

```java
Set<String> result = givenList.stream().collect(toSet());
```

### Collectors.toCollection

- if we want to use a custom implementaion for the toSet or toList method, we must use toCollection instead
- let's push the elements into a LinkedList specifically rather than relying on the compiler to choose the List implementation

```java
List<String> result = givenList.stream().collect(toCollection(LinkedList::new));
```

- this will not work with immutable collections 
- if we want to use immutable collections, we need to write our own custom Collector or use the method *collectingAndThen*

### Collectors.toMap

- used to collect Stream elements into a Map object
- we need two functions
  - keyMapper
  - valueMapper
- we use keyMapper to extract a Map key from a Stream element and valueMapper to extract a value associated with the key
- Let's collect some elements into a Map that stores strings as keys, and their lengths as values:

```java
Map<String, Integer> result = givenList.stream().collect(toMap(Function.identity(), String::length));
```

- *Function.identity()* is a shortcut for a method that accepts and returns the same value
- what happens if the collection contains duplicate elements?
- unlike toSet, toMap will not just replace the duplicate
- it will throw a IllegalStateException
- it doesn't even evaluate if the values are equal. If it sees a duplicate key, it immediately throws the IllegalStateException
- in these cases, we can use the toMap with an extra function parameter

```java
Map<String, Integer> result = givenList.stream
        .collect(toMap(Function.identity(), String::length, (item, identicalItem) -> item));
```

- the third argument here is BinaryOperator, where we can specify how we want to handle duplicates
- like the other two, this one also has a *toUnmodifiableMap* function, introduced in Java 10

### Collectors.collectingAndThen

- this is a special collector that allows us to perform another action on a result straight after collecting ends

```java
int size = givenList.stream().collect(collectingAndThen(toList(), List::size));
```

### Collectors.joining()

- used to join Stream elements
- the basic use:

```java
String result = givenList.stream().collect(joining());
```

- results in "abbcccdd"
- can also specify custom seperators, prefixes and postfixes

```java
String result = givenList.stream().collect(joining(" "));
// results in "a bb ccc dd"

String result = givenList.stream().collect(joining(" ", "PRE-", "-POST"));
// results in "PRE-a bb ccc dd-POST
```

### Collectors.counting()

- simple collector that allows counting the Stream elements:

```java
Long count = givenList.stream().collect(counting()); 
```

### Collectors.summarizingDouble/Int/Long

- collector that returns a special class containing statistical information about numerical data in a stream of extracted elements

```java
DoubleSummaryStatistics statistics = givenList.stream().collect(summarizingDouble(String::length));
```

- the Int/Double/LongSummaryStatistics class provides a few methods that may be useful for basic statistics
- it has *getAverage()*, *getCount()*, *getMax()*, *getMin()* and *getSum()*

### Collectors.averagingDouble/Int/Long

- collector that simply returns an average of extracted elements

```java
Double result = givenList.stream().collect(averagingDouble(String::length));
```

### Collectors.summingInt/Double/Long

- collector that returns the sum of the extracted element

```java
Double result = givenList.stream().collect(summingDouble(String::length));
```

### Collectors.maxBy / minBy

- return the max / min element according to a provided Comparator
- can pick the biggest element by doing:

```java
Optional<String> result = givenList.stream().collect(maxBy(Comparator.naturalOrder()))
```

- the return of an *Optional* forces users to rethink the empty collection case

### Collectors.groupingBy

- used for grouping objects by some property, and then storing the results in a Map instance
- the second argument of the groupingBy method is a Collector, so we can use any Collector
- to group the Strings by length, and the store them in a set, we can do this:

```java
Map<Integer, Set<String>> result = givenList.stream().collect(groupingBy(String::length, toSet()));
```

- another example from the [JavaDocs](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Collectors.html)
- compute the sum of salaries per department:

```java
Map<Department, Integer> totalByDept = employees.stream().collect(groupingBy(Employee::getDepartment, summingInt(Employee::getSalary)));
```

### Collectors.partitioningBy

- specialised groupingBy that accepts a Predicate and then collects Stream elements into a Map instance that stores Boolean values as the keys, and collections as values
- under the "true" key, we find a collection of values that match the predicate
- under the "false" key, we find a collection of values that do not match the predicate

```java
Map<Boolean, List<String>> result = givenList.stream().collect(partitioningBy(s -> s.length() > 2));
```

- the output of a toString on the resulting Map would produce this:

```
{false=["a","bb","dd"], true=["ccc"]}
```

### Collectors.teeing

- let's find the min and max numbers from a given Stream using the collectors we've learned thus far

```java
List<Integer> numbers = Arrays.asList(42, 4, 2, 24);
Optional<Integer> min = numbers.stream().collect(minBy(Integer::compareTo));
Optional<Integer> max = numbers.stream().collect(maxBy(Integer::compareTo));
//do something useful with min and max, say find the difference between them
```

- before Java 12, we needed to operate on the Stream twice, store our variables somewhere and then combine the results afterwards
- in Java 12, a new Collector was introduced that could take care of all this for us

```java
List<Integer> numbers = Arrays.asList(42, 4, 2, 24);

Integer difference = numbers.stream().collect(
    teeing(
        minBy(Integer::compareTo),
        maxBy(Integer::compareTo),
        (min, max) -> max.get() - min.get()));
```

## Custom Collectors

- if we want to write our own Collector, need to implement the Collector interface and specify the three generic parameters

```java
public interface Collector<T, A, R> {}
```

1) **T** - the type of objects that will be available for collection
2) **A** - the type of a mutable accumulator object
3) **R** - the return type of the result

- let's write one to get an idea
- it will collect elements into an immutable Set
- start by specifying the right types:

```java
private class ImmutableSetCollector<T> implements Collector<T>, ImmutableSet.Builder<T>, ImmutableSet<T>> {}
```

- we need a *mutable* collection for internal collection handling, so we can't use ImmutableSet.
- instead we need to use some other mutable collection
- in this case, we will use ImmutableSet.Builder
- so we need to implement 5 methods

```java
Supplier<ImmutableSet.Builder<T>> supplier();
BiConsumer<ImmutableSet.Builder<T>, T> accumulator();
BinaryOperator<ImmutableSet.Builder<T>> combiner();
Function<ImmutableSet.Builder<T>, ImmutableSet<T>> finisher();
Set<Characteristics> characteristics()
```

- the **supplier()** method return a Supplier instance that generates an empty accumulator instance

```java
@Override
public Supplier<ImmutableSet.Builder<T>> supplier() {
    return ImmutableSet::builder;
}
```

- the **accumulator** method returns a function that is used for adding a new element to an existing accumulator object

```java
@Override
public BiConsumer<ImmutableSet.Builder<T>, T> accumulator() {
    return ImmutableSet.Builder::add;
}
```

- the **combiner** method returns a function for merging two accumulators together

```java
@Override
pulic BinaryOperator<ImmutableSet.Builder<T>> combiner() {
    return (left, right) -> left.addAll(right);
}
```

- the **finisher** method returns a function that is used for converting the accumulator to the final result type

```java
@Override
public Function<ImmutableSet.Builder<T>, ImmutableSet<T>> finisher() {
    return ImmutableSet.Builder::build;
}
```

- the **characteristics** method is used to provide the Stream with some information about internal optimizations
- in this case, we have a set, so we don't care about the order of elements, so we'll use Characteristics.UNORDERED

```java
@Override
public Set<Characteristics> characteristics() {
    return Sets.immutableEnumSet(Characteristics.UNORDERED);
}
```

- and this is how we'll use it:

```java
ImmutableSet<String> result = givenList.stream().collect(toImmutableSet());
```