# Intro to Streams

## Stream API

### Stream creation

- can be created from different element sources with the help of *stream()* or *of()*

```java
String[] arr = new String[]{"a", "b", "c"};
String<String> stream = Arrays.stream(arr);
stream = Stream.of("a", "b", "c");
```

- a *stream()* default method is added to the Collection interface to allow the creation of *Stream<T>* using any collection

```java
Stream<String> stream = list.stream();
```

### Multi-threading with streams

- the Streams API simplifies multithreading by providing a parallelStream method that runs operations over stream's elements in parallel

```java
list.parallelStream().forEach(element -> doWork(element));
```

## Stream operations

- two types
- **intermediate** operations return Stream<T>
- **terminal** return a result of definite type
- intermediate operations can be chained
- operations on streams do not alter the source
- quick example

```java
long count = list.stream().distinct().count();
```

- *distinct()* represents an intermediate operation which creates a new stream of unique elements from the previous stream
- *count()* is a terminal operation which returns the streams size

### Iterating

- stream api helps to replace the *for*, *for-each* and *while* loops
- for example:

```java
for (String string: list) {
    if (string.contains("a")) {
        return true;
    }
}
```

- can be replaced by 

```java
boolean exists = list.stream().anyMatch(element -> element.contains("a"));
```

### Filtering

- *filter()* method allows us to pick a stream of elements that satisfy a predicate
- consider the following list:

```java
ArrayList<String> list = new ArrayList<>();
list.add("One");
list.add("OneAndOnly");
list.add("Derek");
list.add("Change");
list.add("factory");
list.add("justBefore");
list.add("Italy");
list.add("Italy");
list.add("Thursday");
list.add("");
list.add("");
```

- the following code creates a *Stream<String>* from the list, finds all elements in the stream containing "d", and creates a new stream containing only the filtered elements

```java
Stream<String> stream = list.stream().filter(element -> element.contains("d"));
```

### Mapping

- to convert elements in a stream by applying a function to them and collect the new elements into a stream, we use the *map* method

```java
List<String> uris = new ArrayList<>();
uris.add("C:\\My.txt");
Stream<Path> stream = uris.stream().map(uri -> Paths.get(uri));
```

- the code above converts Stream<String> to Stream<Path> by applying a lambda expression to every element of the initial Stream
- if you have a stream where every element contains its own sequence of elements, and you want to create a stream of inner elements, use a flatmap

```java
List<Detail> details = new ArrayList<>();
details.add(new Detail());
Stream<String> stream = details.stream().flatMap(detail -> detail.getParts().stream());
```

### Matching

- the API gives a series of useful methods to validate elements based on a given predicate
- can use one of
- *anyMatch()*
- *allMatch()*
- *noneMatch()*
- they all return boolean

```java
boolean isValid = list.stream().anyMatch(element -> element.contains("h"));
isValid = list.stream().allMatch(element -> element.contains("h"));
isValid = list.stream().noneMatch(element -> element.contains("h"));
```

- for empty streams, allMatch method with **any** predicate will return true
- anyMatch will always return false for empty lists

### Reducing

- the API allows the reducing of a sequence of elements to a single value according to a specified function
- the *reduce* method takes 2 parameters, an initial value, and an accumulator function

```java
List<Integer> integers = Arrays.asList(1, 1, 1);
Integer reduced = integers.stream().reduce(23, (a, b) -> a + b);
```

### Collecting

- reduction can also be provided by the *collect()* method

```java
List<String> resultList = list.stream().map(element -> element.toUpperCase()).collect(Collectors.toList());
```