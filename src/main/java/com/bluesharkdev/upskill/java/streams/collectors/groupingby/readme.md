# A guide to groupingBy
## groupingBy collectors

- the static factory methods *Collectors.groupingBy()* and *Collectors.concurrentGroupingBy()* provide us with functionality similar to the *GROUP BY* clause in the SQL language
- use them for grouping objects by some property and storing the results in a Map instance
- overloaded methods for groupingBy are:
- first with a calssification function

```java
static <T, K> Collector<T, ?, Map<K, List<T>>> groupingBy(Function<? super T, ? extends K> classifier)
```

- second with a classification function and a second collector as method parameters

```java
static <T, K, A, D> Collector<T, ?, Map<K, D>> groupingBy(
        Function<? super T, ? extends K> classifier,
        Collector<? super T, A, D> downstream)
```

- finally, a classification function, a supplier method to provide a specific Map implementation, and a second collector

```java
static <T,K,D,A,M extends Map<K,D>> Collector<T,?,M> groupingBy(
        Function<? super T, ? extends K> classifier,
        Supplier<M> mapFactory,
        Collector<? super T,A,D> downstream)
```

## Code setup

- we're going to define a BlogPost that we can use as an example

```java
class BlogPost {
    String title;
    String author;
    BlogPostType type;
    int likes;
}

enum BlogPostType {
    NEWS,
    REVIEWS,
    GUIDE
}

//then a list of actual blog objects
List<BlogPost> posts = Arrays.asList(....);
```

- let's also define a Tuple class that will be used to group blog posts by a combination of their type **and** author attributes

```java
class Tuple {
    BlogPostType type;
    String author;
}
```

## Simple grouping by a single column

- let's start with the simplest groupingBy method, which only takes a classification function as its parameter
- this classification function is applied to every element of the stream
- we use the value returned by the function as the key to the map that we get
- to group the blog posts in the blog post list by their type:

```java
Map<BlogPostType, List<BlogPost>> postsPerType = posts.stream().collect(groupingBy(BlogPost::getType));
```

## groupingBy with a Complex Map key type

- classification function is not limited to a scalar or String value
- it can be any object that correctly implements the necessary equals and hashcode methods
- using the Apache Commons *Pair* instance

```java
Map<Pair<BlogPostType, String>>, List<BlogPost>> postsPerTypeAndAuthor = posts.stream()
        .collect(groupingBy(post -> new ImmutablePair<>(post.getType(), post.getAuthor())));
```

- we can also use our *Tuple* object to do this:

```java
Map<Tuple, List<BlogPost>> postsPerTypeAndAuthor = posts.stream()
        .collect(groupingBy(post -> new Tuple(post.getType(), post.getAuthor())));
```

- Java 16 introduced records, which we can use in place of the Tuple.
- let's modify our BlogPost object to include a record:

```java
public class BlogPost {
    private String title;
    private String author;
    private BlogPostType type;
    private int likes;
    record AuthPostTypeLikes(String author, BlogPostType type, int likes) {};
    
    ...
    
}
```

- we can now use the record to group by

```java
Map<BlogPost.AuthPostTypeLikes, List<BlogPost>> postsPerTypeAuthorAndLikes = posts.stream()
        .collect(groupingBy(post -> new BlogPost.AuthPostTypeLikes(post.getAuthor(), post.getType(), post.getLikes())));
```

## Modifying the returned Map value type

- the second overload of the *groupingBy* takes an additional collector, that is applied to the results of the first collector
- when we specify a classification and no downstream collector, the *toList()* collector is used behind the scenes
- let's use the toSet() collector to get a Set instead of a List

```java
Map<BlogPostType, Set<BlogPost>> postPerType = posts.stream().collect(groupingBy(BlogPost::getType, toSet()));
```

## Grouping by multiple fields

- a different application of the downstream collector is to do a secondary groupingBy to the results of the first group

```java
Map<String, Map<BlogPostType, List<BlogPost>>> results = posts.stream().collect(
        groupingBy(BlogPost::getAuthor, groupingBy(BlogPost::getType)));
```

## Getting the sum from grouped results

- to calculate the sum of the likes for each type of blog post:

```java
Map<BlogPostType, Integer> likesPerType = posts.stream().collect(groupingBy(BlogPost::getType, summingInt(BlogPost::getLikes)));
```

## Getting the min or max from a grouped result

- maxBy and minBy take into account the possibility that the stream might be empty, so they return Optionals

```java
Map<BlogPostType, Optional<BlogPost>> maxLikesPerType = posts.stream().collect(groupingBy(BlogPost::getType), maxBy(BlogPost::getLikes));
```

## Getting a summary for an attribute from grouped results

```java
Map<BlogPostType, IntSummaryStatistics> statsPerBlogPostType = posts.stream().collect(groupingBy(BlogPost::getType, summarizingInt(BlogPost::getLikes)));
```

## Aggregating over multiple fields

- we can aggregate over multiple fields with two approaches
- the first is to use the *collectingAndThen* collector as the downstream collector
- let's group author and for each one, we count the number of titles, list the titles and get some summary statistics on the likes 
- we need a new record in BlogPost

```java
public class BlogPost {
    //...
    record PostCountTitlesLikesStatistics(long postCount, String titles, IntSummaryStatistics likesStats) {};
}
```

- implementation of the groupingBy and collectingAndThen will be:

```java
Map<String, BlogPost.PostCountTitlesLikesStatistics> postsPerAuthor = 
    posts.stream().collect(
            groupingBy(BlogPost::getAuthor, 
            collectingAndThen(toList, list -> {
                long count = list.stream().map(BlogPost::getTitle).collect(counting());
                String titles = list.stream().map(BlogPost::getTitle).collect(joining(" : "));
                IntSummaryStatistics stats = list.stream().collect(summarizingInt(BlogPost::getLikes));
                return new PostCountTitlesLikesStatistics(count, titles, stats);
        })));
```

- the second approach is a more sophisticated approach using the toMap collector
- we want to group the BlogPost elements by author and then concatenate the string titles with an upper bounded sum of *like* scores
- first we create a record tht is going to encapsulate our aggregated result

```java
public class BlogPost {
    //...
    record TitlesBoundedSumOfLikes(String titles, int boundedSumOfLikes) {};
    
}
```

- then we group and accumulate the stream as follows:

```java
int maxValLikes = 17;
Map<String, BlogPost.TitlesBoundedSumOfLikes> postsPerAuthor = posts.stream()
        .collect(toMap(BlogPost::getAuthor, post -> {
            int likes = (post.getLikes > maxValLikes ? maxValLikes : post.getLikes);
            return new BlogPost.TitlesBoundedSumOfLikes(post.getTitle, likes);
        }, (u1, u2) -> {
            int likes = (u2.boundedSumOfLikes() > maxValLikes) ? maxValLikes : u2.boundedSumOfLikes();
            return new BlogPost.TitlesBoundedSumOfLikes(u1.titles.toUpperCase() + " : " + u2.titles.toUpperCase(),
                u1.boundedSumOfLikes() + likes);
        }));
```

- the first parameter of *toMap* groups the values applying *BlogPost::getAuthor()*
- the second parameter transforms the values of the map using the lambda function to convert each BlogPost into a TitlesBoundedSumOfLikes record
- the third parameter deals with duplicate elements for a given key

## Mapping grouped results to a different type

```java
Map<BlogPostType, String> postsPerType = posts.stream().collect(
        groupingBy(BlogPost::getType, mapping(BlogPost::getTitle, joining(", ", "Post titles [", "]"))));
``` 

## Modifying the return Map type

- we can specify which type of Map we want by using a Supplier parameter

```java
EnumMap<BlogPostType, List<BlogPost>> postsPerType = posts.stream()
        .collect(groupingBy(BlogPost::getType, () -> new EnumMap<>(BlogPostType.class), toList()));
```

# Concurrent groupingBy Collector

- the groupingByConcurrent collector is similar to groupingBy, it just leverages multi-core architecture
- it has the same arguments as the groupingBy
- it's return type is ConcurrentMap or a subclass of ConcurrentMap
- to do a grouping concurrently, it needs to have a parallel stream as a source

```java
ConcurrentMap<BlogPostType, List<BlogPost>> postPerType = posts.parallelStream().collect(groupingByConcurrent(BlogPost::getType));
```

- if we choose to pass in the Supplier, we need to make sure that the Supplier returns an instance of ConcurrentMap or a subclass