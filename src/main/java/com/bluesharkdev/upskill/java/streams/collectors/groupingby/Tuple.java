package com.bluesharkdev.upskill.java.streams.collectors.groupingby;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Tuple {
    BlogPostType type;
    String author;
}
