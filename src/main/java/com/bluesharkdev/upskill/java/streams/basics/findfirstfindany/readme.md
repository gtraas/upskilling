# Streams
## findFirst vs findAny
### findAny

- allows us to find any element in the stream
- use it when we're looking for an element without any interest in the encounter order
- returns an Optional instance, which is empty if the stream is empty

```
@Test
public void createStream_whenFindAnyResultIsPresent_thenCorrect() {
    List<String> list = Arrays.asList("A", "B", "C", "D");
    
    Optional<String> result = list.stream().findAny();
    
    assertTrue(result.isPresent());
    assertEquals(result.get(), anyOf(is("A"), is("B"), is("C"), is("D"));
}
```

- in a non-parallel operation, findAny will return the first element in the stream
- there is no guarantee of this
- in a parallel operation, the result cannot be reliably determined

```java
@Test
public void createParallelStream_whenFindAnyResultIsPresent_thenCorrect() {
    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
    Optional<Integer> result = list 
        .stream().parallel()
        .filter(n -> n < 4).findAny();
    
    assertTrue(result.isPresent());
    assertThat(result.get(), anyOf(is(1), is(2), is(3)));
}
```

### findFirst

- finds the first element in a stream
- use this method when we are specifically interested in order
- when there is no encounter order, this will follow the behaviour of findAny
- according to the documentation:

    - "Streams may or may not have a defined encounter order. It depends on the source and the intermediate operations."

- return type is an Optional, which is empty if the stream is empty

```java
@Test
public void createStream_whenFindFirstResultIsPResent_thenCorrect() {
    List<String> list = Arrays.asList("A", "B", "C", "D");
    
    Optional<String> result = list.stream().findFirst();
    
    assertTrue(result.isPresent());
    assertEquals(result.get(), is("A"));

}
```

- behaviour of findFirst does not change in a parallel scenario
- if an encounter order exists, it will always behave deterministically (given the same inputs, will always return the same output)