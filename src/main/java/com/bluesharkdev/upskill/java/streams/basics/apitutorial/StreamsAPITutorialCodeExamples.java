package com.bluesharkdev.upskill.java.streams.basics.apitutorial;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.*;

public class StreamsAPITutorialCodeExamples {

    private int counter = 0;
    List<Product> productList;

    public static void main(String[] args) {
        StreamsAPITutorialCodeExamples eg = new StreamsAPITutorialCodeExamples();

        eg.productList = Arrays.asList(
                new Product("Potatoes", 23), new Product("Orange", 14),
                new Product("Lemon", 13), new Product("Bread", 23),
                new Product("Sugar", 14)
        );

        System.out.println("Creating streams");
        eg.creatingStreams();
        System.out.println("Referencing a stream");
        eg.referencingAStream();
        System.out.println("Stream pipeline");
        eg.streamPipeline();
        System.out.println("Lazy invocation");
        eg.lazyInvocation();
        System.out.println("Order of Execution");
        eg.orderOfExecution();
        System.out.println("Stream reduction - reduce()");
        eg.reductionReduce();
        System.out.println("Stream reduction - collect()");
        eg.reductionCollect();
        System.out.println("Parallel Streams");
        eg.parallelStreams();
    }

    private void parallelStreams() {
        //the parallelStream method
        Stream<Product> streamParallel = productList.parallelStream();
        boolean isParallel = streamParallel.isParallel();
        boolean bigPrice = streamParallel
                .map(element -> element.price * 12)
                .anyMatch(price -> price > 200);

        //use the parallel method for stream that don't originate from collections or arrays
        IntStream intStream = IntStream.range(0, 150).parallel();
        isParallel = intStream.isParallel();

        //turning a parallel stream back into a sequential stream
        intStream = intStream.sequential();

    }

    private void reductionCollect() {
        //collecting to a Collection
        List<String> nameList = productList.stream().map(Product::name).collect(Collectors.toList());

        //reducing to a String
        String names = productList.stream().map(Product::name).collect(Collectors.joining(",", "[", "]"));
        System.out.println(names);

        //processing the average value
        double average = productList.stream().collect(Collectors.averagingInt(Product::price));

        //summing the price
        int sum = productList.stream().collect(Collectors.summingInt(Product::price));

        //collecting summary statistics
        IntSummaryStatistics stats = productList.stream().collect(Collectors.summarizingInt(Product::price));

        //grouping elements according to a function
        Map<Integer, List<Product>> collectorByName = productList.stream().collect(Collectors.groupingBy(Product::price));

        //dividing stream elements by a predicate
        Map<Boolean, List<Product>> collectorByPredicate = productList.stream().collect(Collectors.partitioningBy(element -> element.price > 15));

        //pushing the collector to do additional trasnformation
        Set<Product> unmodifiableSet = productList.stream().collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));

        //custom collector
        Collector<Product, ?, LinkedList<Product>> toLinkedList =
                Collector.of(LinkedList::new, LinkedList::add,
                        (first, second) -> {
                            first.addAll(second);
                            return first;
                        }
                );

        LinkedList<Product> linkedListOfProduct = productList.stream().collect(toLinkedList);
    }

    private record Product(String name, int price) {}

    private void reductionReduce() {
        OptionalInt reduced = IntStream.range(1, 4).reduce((a, b) -> a + b);
        System.out.println("Reduced: %s".formatted(reduced.getAsInt()));

        int reducedTwoParams = IntStream.range(1, 4).reduce(10, (a, b) -> a + b);
        System.out.println("Reduced with identity: %s".formatted(reducedTwoParams));

        int reducedParams = Stream.of(1, 2, 3)
                .reduce(10, (a, b) -> a + b, (a, b) -> {
                    System.out.println("combiner called with no parallel stream");
                    return a + b;
                });
        System.out.printf("Reduced with identity, combiner, not parallel: %s%n".formatted(reducedParams));

        int reducedParallel = Arrays.asList(1, 2, 3).parallelStream()
                .reduce(10, (a, b) -> a + b, (a, b) -> {
                    System.out.println("combiner called, parallel stream");
                    return a + b;
                });
        System.out.println("Reduced with identity, combiner, parallel stream: %s".formatted(reducedParallel));
    }

    private void orderOfExecution() {

        List<String> list = Arrays.asList("abc1", "abc2", "abc3");

        counter = 0;
        long size = list.stream().map(element -> {
            wasCalled();
            return element.substring(0, 3);
        }).skip(2).count();

        System.out.println(counter);

        counter = 0;
        size = list.stream().skip(2).map(element -> {
            wasCalled();
            return element.substring(0, 3);
        }).count();

        System.out.println(counter);
    }

    private void lazyInvocation() {
        //intermediate methods are invoked lazily only when terminal methods are applied to the stream
        //first example demonstrating the lack of invocation of intermediate methods when there's no terminal operation
        List<String> list = Arrays.asList("abc1", "abc2", "abc3");
        Stream<String> stream = list.stream().filter(element -> {
            wasCalled();
            return element.contains("2");
        });

        //running her will show that counter remains 0 despite us having asked for wasCalled to be invoked 3 times
        System.out.println(counter);

        //let's rework this to show lazy invocation at work
        Optional<String> optional = list.stream().filter(element -> {
            System.out.println("filter() was called");
            return element.contains("2");
        }).map(element -> {
            System.out.println("map() was called");
            return element.toUpperCase();
        }).findFirst();


    }

    //part of the lazyInvocation example
    private void wasCalled() {
        System.out.println("wasCalled");
        counter++;
    }

    private void streamPipeline() {
        //proper stream pipeline is 1: source, 2: intermediate operation(s), 3: terminal operation
        Stream<String> onceModifiedStream = Stream.of("abcd", "bbcd", "cbcd").skip(1);

        //can chain the intermediate operations
        Stream<String> twiceModifiedStream = Stream.of("abcd", "bbcd", "cbcd")
                .skip(1)
                .map(element -> element.substring(0, 3));

        //use the pipeline for the most correct usage
        List<String> stringList = Arrays.asList("abc1", "abc2", "abc3");
        long size = stringList.stream() //source
                    .skip(1)            //intermediate operation 1
                    .map(element -> element.substring(0, 3)) //intermediate operation 2
                    .sorted()           //intermediate operation 3
                    .count();           //terminal operation
    }

    private void referencingAStream() {
        //this is bad code, but illustrate the point that we can't reuse a stream:
        Stream<String> streamOfStrings = Stream.of("a", "b", "c").filter(element -> element.contains("b"));

        Optional<String> stringAny = streamOfStrings.findAny();

        try {
            Optional<String> stringFirst = streamOfStrings.findFirst();
        } catch (IllegalStateException e) {
            System.out.println("We can't reuse a stream once a terminal operation has been applied");
        }

        //this is the better way of 're-using' a stream
        List<String> filteredStringList = Stream.of("a", "b", "c").filter(element -> element.contains("b"))
                .collect(Collectors.toList());
        Optional<String> stringAny2 = filteredStringList.stream().findAny();
        Optional<String> stringFirst2 = filteredStringList.stream().findFirst();

    }

    private void creatingStreams() {
        System.out.println("Creating Streams\n\n");
        //Create empty stream
        Stream<String> emptyStream = Stream.empty();

        //Create stream from Collection
        List<String> listOfStrings = List.of("A", "B", "C");
        Stream<String> streamFromCollection = listOfStrings.stream();

        //With a builder
        Stream<String> streamFromBuilder = Stream.<String>builder().add("a").add("b").add("c").build();

        //With a generator
        Stream<String> streamFromGenerator = Stream.generate(() -> "element").limit(10);

        //With an iterator
        Stream<Integer> streamFromIterator = Stream.iterate(40, n -> n + 2).limit(10);

        //Stream of primitives
        IntStream intStream = IntStream.range(1, 3);
        LongStream longStream = LongStream.rangeClosed(1, 3);
        DoubleStream doubleStream = new Random().doubles(3);

        //Stream of string
        IntStream streamOfChars = "abc".chars();
        Stream<String> streamOfStrings = Pattern.compile(", ").splitAsStream("a, b, c");

        //Stream of file
        Path path = Paths.get("c:\\file.txt");
        try {
            Stream<String> streamFromFile = Files.lines(path);
            Stream<String> streamFromFileWithCharset = Files.lines(path, Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println("We don't have a file of that name. Expected exception");
        }
    }
}
