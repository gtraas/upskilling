package com.bluesharkdev.upskill.java.streams.collectors.groupingby;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.bluesharkdev.upskill.java.streams.collectors.groupingby.BlogPostType.*;
import static java.util.stream.Collectors.*;

public class GroupingByExample {

    public static void main(String[] args) {

        List<BlogPost> posts = Arrays.asList(
                new BlogPost("title", "author1", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title2", "author2", NEWS, new Random().nextInt(100)),
                new BlogPost("title3", "author3", GUIDE, new Random().nextInt(100)),
                new BlogPost("title4", "author4", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title5", "author1", GUIDE, new Random().nextInt(100)),
                new BlogPost("title6", "author2", NEWS, new Random().nextInt(100)),
                new BlogPost("title7", "author3", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title8", "author4", NEWS, new Random().nextInt(100)),
                new BlogPost("title9", "author1", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title12", "author2", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title25", "author3", GUIDE, new Random().nextInt(100)),
                new BlogPost("title54", "author4", NEWS, new Random().nextInt(100)),
                new BlogPost("title7", "author5", NEWS, new Random().nextInt(100)),
                new BlogPost("title123", "author1", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title865", "author2", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title324", "author3", NEWS, new Random().nextInt(100)),
                new BlogPost("title542", "author4", GUIDE, new Random().nextInt(100)),
                new BlogPost("title908", "author5", NEWS, new Random().nextInt(100)),
                new BlogPost("title876", "author6", REVIEWS, new Random().nextInt(100)),
                new BlogPost("title123", "author3", GUIDE, new Random().nextInt(100))

        );

        //simply grouping by blog post type
        Map<BlogPostType, List<BlogPost>> postsPerType = posts.stream().collect(groupingBy(BlogPost::getType));

        //grouping with an Apache Commons Pair object
        Map<Pair<BlogPostType, String>, List<BlogPost>> postsPerTypeAndAuthorPair =
                posts.stream().collect(groupingBy(post -> new ImmutablePair<>(post.getType(), post.getAuthor())));

        //grouping with our custom Tuple object
        Map<Tuple, List<BlogPost>> postsPerTypeAndAuthor = posts.stream()
                .collect(groupingBy(post -> new Tuple(post.getType(), post.getAuthor())));

        //grouping with Java 16 record
        Map<BlogPost.AuthPostTypeLikes, List<BlogPost>> postPerTypeAndAuthorRecord =
                posts.stream()
                        .collect(groupingBy(post -> new BlogPost.AuthPostTypeLikes(post.getAuthor(), post.getType(), post.getLikes())));

        //modifying the returned map value
        Map<BlogPostType, Set<BlogPost>> modifyReturnValue = posts.stream().collect(groupingBy(BlogPost::getType, toSet()));

        //grouping by multiple fields
        Map<String, Map<BlogPostType, List<BlogPost>>> groupByMultipleFields = posts.stream().collect(
                groupingBy(BlogPost::getAuthor, groupingBy(BlogPost::getType)));

        //calculating the number of likes per blog type
        Map<BlogPostType, Integer> summingLikesPerType = posts.stream().collect(groupingBy(BlogPost::getType, summingInt(BlogPost::getLikes)));

        //getting the min or max from grouped results
        Map<BlogPostType, Optional<BlogPost>> getMaxFromGroupedResult = posts.stream().collect(
                groupingBy(BlogPost::getType, maxBy(Comparator.comparingInt(BlogPost::getLikes))));

        //getting summary statistics for an attribute from grouped results
        Map<BlogPostType, IntSummaryStatistics> getSummaryStatistics = posts.stream().collect(
                groupingBy(BlogPost::getType, summarizingInt(BlogPost::getLikes)));

        //aggregating across multiple fields: Approach 1 - records
        Map<String, BlogPost.PostCountTitleLikesStatistics> aggregatingApproach1 =
                posts.stream().collect(groupingBy(BlogPost::getAuthor,
                        collectingAndThen(toList(), list -> {
                            long count = list.stream().map(BlogPost::getTitle).collect(counting());
                            String titles = list.stream().map(BlogPost::getTitle).collect(joining(" : "));
                            IntSummaryStatistics stats = list.stream().collect(summarizingInt(BlogPost::getLikes));
                            return new BlogPost.PostCountTitleLikesStatistics(count, titles, stats);
                        })));

        //aggregating across multiple fields: Approach 2 - maps
        int maxValLikes = 17;
        Map<String, BlogPost.TitlesBoundedSumOfLikes> aggregatingApproach2 =
                posts.stream().collect(
                        toMap(BlogPost::getAuthor, post -> {
                            int likes = (post.getLikes() > maxValLikes ? maxValLikes : post.getLikes());
                            return new BlogPost.TitlesBoundedSumOfLikes(post.getTitle(), likes);
                        }, (u1, u2) -> {
                           int likes = (u2.boundedSumOfLikes() > maxValLikes ? maxValLikes : u2.boundedSumOfLikes());
                           return new BlogPost.TitlesBoundedSumOfLikes(u1.titles().toUpperCase() + " : " + u2.titles().toUpperCase(),
                                   u1.boundedSumOfLikes() + likes);
                        }));

        //mapping results to a different type
        Map<BlogPostType, String> mapResultsToDifferentType = posts.stream().collect(
                groupingBy(BlogPost::getType, mapping(BlogPost::getTitle, joining(", ", "Titles [", "]"))));

        //modifying the return map type
        EnumMap<BlogPostType, List<BlogPost>> enumMap = posts.stream().collect(
                groupingBy(BlogPost::getType, () -> new EnumMap<>(BlogPostType.class), toList()));


        //concurrent groupingBy
        ConcurrentMap<BlogPostType, List<BlogPost>> concurrentGrouping = posts.stream()
                .collect(groupingByConcurrent(BlogPost::getType));
    }

}
