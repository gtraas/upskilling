# Java 8 Streams API
## Creation

- once created, a stream will not modify its source, allowing creation of multiple instances from a single source

### Empty Stream
- creating an empty Stream:

```java
Stream<String> emptyStream = Stream.empty();
```

- often use the *empty()* method to avoid returning null for streams with no elements

```java
public Stream<String> streamOf(List<String> list) {
    return list == null ? Stream.empty() : list.stream();
}
```

### Stream from a Collection

- can also create a stream from any Collection

```java
Collection<String> collection = Arrays.asList("a", "b", "c");
Stream<String> stream = collection.stream();
```

### Stream of Array

- an array can be converted into a stream

```java
Stream<String> streamofArray = Stream.of("a", "b", "c");
```

- can also create a stream from an existing array

```java
String[] arr = new String[]{"a", "b", "c"};
String<String> streamOfArrayFull = Arrays.stream(arr);
String<String> streamOfArrayPart = Arrays.stream(arr, 1, 3);
```

### Stream.builder

- when a builder is used, the type should be explicity added on the right, otherwise the stream will create Objects

```java
Stream<String> streamFromBuilder = Stream.<String>builder().add("a").add("b").add("c").build();
```

### Stream.generate

- the *generate()* method accepts a *Supplier<T>* for element generation.
- as the resulting stream is infinite, a limit must be supplied, otherwise it will generate the stream up to the memory limit

```java
Stream<String> streamGenerated = Stream.generate(() -> "element").limit(10);
```

- this generates a sequence of ten strings with the value "element".

### Stream.iterate

```java
Steam<Integer> streamIterated = Stream.iterate(40, n -> n + 2).limit(20);
```

- the first element is the first parameter, os in this case *40*
- when creating the next element, the function is applied, so our secon element will be *42*
- this is another infinite stream generator, so the limit is important

### Stream of primitives

- because *Stream<T>* is a generic interface, three new special interfaces were created:
  - IntStream
  - LongStream
  - DoubleStream
- the new interfaces alleviates the need for unnecessary auto-boxing

```java
IntStream intStream = IntStream.range(1, 3);
LongStream longStream = LongStream.rangeClosed(1, 3);
```
- the *range(int startInclusive, int endExclusive)* method creates an ordered stream from the first parameter to the second parameter
- It increments the value of subsequent elements with the step being 1
- the result doesn't include the final parameter as this the the exclusive upper bound
- the *rangeClosed(long startInclusive, long endInclusive)* method does the same thing as *range*, except the final parameter is included
- the *Random* class also provides a useful way of creating a stream

```java
DoubleStream doubleStream = new Random().doubles(3);
```

### Stream of Strings

- with the helps of the *chars()* method of the String class, we can create a String stream
- there is no CharsStream, so we need to use IntStream

```java
IntStream streamOfChars = "abc".chars();
```

- the next example breaks a string into sub-strings based on a regex

```java
Stream<String> streamOfString = Pattern.compile(", ").splitAsStream("a, b, c");
```

### Stream of File

- the Java NIO class *Files* allows us to create a Stream<String> of a text file through the *lines()* method
- every line becomes an element of the stream:

```java
Path path = Paths.get("c:\\file.txt")
Stream<String> streamOfStrings = Files.lines(path);
Stream<String> streamWithCharset = Files.lines(path, Charset.forName("UTF-8"));
```

## Referencing a Stream

- we can instantiate a stream, and have an accessible reference to it as long as we don't execute a terinal operation on it
- the following is bad practice but for illustration purposes:

```java
Stream<String> stream = Stream.of("a", "b", "c").filter(element -> element.contains("b"));
Optional<String> anyElement = stream.findAny();
```

- if we try to access this stream again, we'll get the *IllegalStateException*

```java
Optional<String> firstElement = stream.findFirst();
```

- Because the IllegalStateException is a RuntimeException, the compiler won't signal a problem here.
- streams **cannot** be reused
- we can get around this by using a collector

```java
List<String> elements = Stream.of("a", "b", "c").filter(element -> element.contains("b"))
        .collect(Collectors.toList());
Optional<String> anyElement = elements.stream().findAny();
Optional<String> firstElement = elements.stream().findFirst();
```

## Stream Pipeline

- the stream pipeline consists of a three parts
  - **source**
  - **intermediate operation(s)**
  - **terminal operation**
- intermediate operations return a new modified stream
- for example; to create a new stream of the existing one, with one element skipped, we can:

```java
Stream<String> modifiedStream = Stream.of("abcd", "bbcd", "cbcd").skip(1);
```

- if we need more than one intermediate operations, we can chain them together
- for example:
```java
Stream<String> twiceModifiedStream = Stream.of("abcd", "bbcd", "cbcd").skip(1).map(element -> element.substring(0, 3));
```

- the *map* function takes a lambda expression as a parameter
- a stream by itself is useless, we need to apply a terminal operation to it
- **can only use 1 terminal operation on a stream**
- the correct way to use streams is in the stream pipeline (source, intermediate operation(s), terminal operation)

```java
List<String> list = Arrays.asList("abc1", "abc2", "abc3");
long size = list.stream().skip(1).map(element -> element.substring(0, 3)).sorted().count();
```

## Lazy Invocation

- intermediate operations are **lazy**
- they are only invoked if necessary for terminal operation execution
- let's set up an example

```java
private long counter;

private void wasCalled() {
    counter++;
}
```

- now let's call this method from the intermediate operation *filter*:

```java
List<String> list = Arrays.asList("abc1", "abc2", "abc3");
counter = 0;
Stream<String> stream = list.stream().filter(element -> {
    wasCalled();
    return element.contains("2");
});
```

- because we have a source of three elements, we would expect the *wasCalled* method to be called 3 times, and the value of counter to be 3
- however, running this has no effect on counter at all.
- *wasCalled* is never invoked
- the reason is that there is a missing terminal operation on our stream
- let's rewrite our code by adding a *map()* and a *findFirst()*

```java
Optional<String> stream = list.stream().filter(element -> {
    log.info("filter() was called");
    return element.contains("2");
}).map(element -> {
    log.info("map() was called");
    return element.toUpperCase();
}).findFirst();
```

- resulting log shows we called the filter method twice and the map method once
- this is because the pipeline executes vertically
- to explain this, the first element didn't satisfy the filter predicate (1st call to filter).
- the second element does satisfy the filter predicate (2nd call to filter)
- without calling the filter method for the third element, we went down to the map method
- we then go down to the findFirst() method which satisfies by just one element, and thus the pipeline completes
- so lazy invocation allows us to avoid two unnecessary method calls, one for filter on our 3rd element, and one for the map method if our third element had satisfied the filter predicate.

## Order of execution

- the correct order of chaining operations is one of the most important aspects of the stream pipeline when it comes to performance

```java
long size = list.stream().map(element -> {
    wasCalled();
    return element.substring(0, 3);
}).skip().count();
```

- execution of this code results in the counter being increased to 3
- means we used the mpa method 3 times, but the value of size is 1.
- resulting stream had 1 element, even though we executed the expensive map function for no reason two out of three times
- if we change the order of the skip() and map() methods, the counter will only increase by 1.
- means we'll only call the expensive map method once

```java
long size = list.stream().skip(2).map(element -> {
    wasCalled();
    return element.substring(0, 3);
}).count();
```

- **intermediate operations which reduce the size of the stream should be placed before operations which are applying to each element**
- need to keep methods such as *skip()*, *filter()* and *distinct()* at the top of the pipeline

## Stream reduction

- API has many terminal operations which aggregate a stream to type or primitive type, such as *count()*, *max()* and *min()*
- if we need to customise the Stream's reduction mechanism, we can use the *reduce()* or *collect()* methods

### ***reduce()***

- there are three variations of this method, differing by their signatures and return types
- can have the following parameters:
  - **identity**: the initial value for the accumulator, or a default value if the stream is empty and there is nothing to accumulate
  - **accumulator**: a function which specifies the logic of the aggregation of elements. As the accumulator creates a new value for each step of reducing, the quantity of new values equals the stream's size and only the last value is useful. This is not good for performance
  - **combiner**: a function which aggregates the results of the accumulator. We can only call the combiner in a parallel mode to reduce the results of accumulators from different threads
- let's look at these methods in action

```java
OptionalInt reduced = IntStream.range(1, 4).reduce((a, b) -> a + b);
```

- reduced = 6 (1 + 2 + 3)

```java
int reducedTwoParams = IntStream.range(1, 4).reduce(10, (a, b) -> a + b);
```

- reduced = 16 (10 + 1 + 2 + 3)

```java
int reducedStreams = Stream.of(1, 2, 3)
        .reduce(10, (a, b) -> a + b, (a, b) -> {
           log.info("combiner called");
           return a + b;
});
```

- result here will be the same as the previous one, so 16. The combiner was not called because we need the stream to be parallel, like this:

```java
int reducedParallel = Arrays.asList(1, 2, 3).parallelStream()
        .reduce(10, (a + b) -> a + b, (a, b) -> {
            log.info("combiner was called");
            return a + b;
});
```

- the result here is 36, and the combiner is called twice
- the reduction works by the following algorithm
  - the accumulator runs 3 times, adding each element to the *identity*, 10
  - these actions are done in parallel
  - as a result they have (10 + 1 = 11; 10 + 2 = 12; 10 + 3 = 13)
  - the combiner now merges the three results
  - it needs two iterations (12 + 13 = 25; 25 + 11 = 36)

### ***collect()***

- reduction of a stream can also be done with the collect() method
- accepts a type of Collector
- in this section, we'll use this list as the base for our streams:

```java
List<Product> productList = Arrays.asList(new Product(23, "Potatoes"),
        new Product(14, "Orange"), new Product(13, "Lemon"),
        new Product(23, "Bread"), new Product(14, "Sugar"));
```

- Converting a stream to a Collection:

```java
List<String> collectorCollection = productList.stream().map(Product::getName).collect(Collectors.toList());
```

- reducing to a string

```java
String listToString = productList.stream().map(Product::getName)
        .collect(Collectors.joining(", ", "[", "]"));
```

- the joiner method can have from 1 to 3 parameters (delimiter, suffix, prefix)
- most convenient thing about the joiner is that the developer doesn't need to check if the stream ends to apply the suffix and not the delimiter
- the Collector does that for you
- processing the average value of all numeric elements in a stream

```java
double averagePrice = productList.stream().collect(Collectors.averagingInt(Product::getPrice));
```

- processing the sum of all numeric elements of the stream:

```java
int summingPrice = productList.stream().collect(Collectors.summingInt(Product::getPrice));
```

- the methods *averagingXXX()*, *summingXXX()* and *summarizingXXX()* work with primitives(int, long and double) and their wrapper classes(Integer, Double and Long)
- one more powerful feature of this collector is the mapping, meaning the developer doesn't need to provide a seperate mapping instruction
- collecting statistical information about the stream

```java
IntSummaryStatistics stats = productList.stream().collect(Collectors.summarizingInt(Product::getPrice));
```

- this summary statistics would produce a min, max, sum, count and average value
- the toString method would print out: *IntSummaryStatistics{count=5, sum=86, min=13, average=17.20000, max=23]*

- grouping of stream's elements according to the specified function

```java
Map<Integer, List<Product>> collectorMapOfLists = productList.stream()
        .collect(Collectors.groupingBy(Product::getPrice));
```

- the stream was reduced to a map, which groups products by their price.
- dividing a stream's elements into groups according to a predicate

```java
Map<Boolean, List<Product>> partitionedMap = productList.stream()
        .collect(Collectors.partitioningBy(element -> element.getPrice > 15));
```

- pushing the collector to perform additional transformations

```java
Set<Product> unmodifiableSet = productList.stream()
        .collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));
```

- in this case, the collector has converted the list into a Set, and then created an unmodifiable Set out of it
- creating a custom collector

```java
Collector<Product, ?, LinkedList<Product>> toLinkedList = 
    Collector.of(LinkedList::new, LinkedList::add, (first, second) -> {
        first.addAll(second);
        return first;
    });

LinkedList<Product> linkedListOfPersons = productList.stream().collect(toLinkedList);
```

- here, an instance of Collectors got reduced to a LinkedList<Product>

## Parallel Streams

- before java 8, parallelization was complex
- with the emergence of the *ExecutorService* and the *ForkJoin*, this process has been simplified. Even more since we can do it in a functional way
- the API allows us to create parallel streams which perform operations in parallel mode
- when the source of the stream is a *Collection* or *array*, we can use *parallelStream()*

```java
Stream<Product> streamOfCollection = productList.parallelStream();
boolean isParallel = streamOfCollection.isParallel();
boolean bigPrice = streamOfCollection
        .map(product -> product.getPrice() * 12)
        .anyMatch(price -> price > 200);
```

- if the source is not a *Collection* or *array*, we should use *parallel()*
```java
IntStream intStreamParallel = IntStream.range(1, 150).parallel();
boolean isParallel = intStreamParallel.isParallel();
```

- under the hood, Stream API automatically uses the ForkJoin framework to execute operations in parallel.
- by default, the common thread pool is used and there's no way to assign a custom thread pool to it
- when using streams in parallel, avoid blocking operations
- also best to use parallel mode when tasks need a similar amount of time to execute
- if one task takes much longer than the others, it can slow down the complete app
- a stream in parallel mode can be converted back to sequential mode by using the *sequential()* method
- 
```java
IntStream intStreamSequential = intStreamParallel.sequential()
boolean isParallel = intStreamSequential.isParallel();
```