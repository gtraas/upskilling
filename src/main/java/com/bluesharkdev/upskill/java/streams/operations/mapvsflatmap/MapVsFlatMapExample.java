package com.bluesharkdev.upskill.java.streams.operations.mapvsflatmap;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapVsFlatMapExample {

    public static void main(String[] args) {

        //map vs flatmap on optionals
        System.out.println("map() vs flatMap() on Optionals");
        Optional<Optional<String>> veryNestedOptional = Optional.of("string").map(s -> Optional.of("STRING"));

        System.out.println(veryNestedOptional.get());

        Optional<String> optional = Optional.of("string").flatMap(s -> Optional.of("STRING"));

        System.out.println(optional.get());

        //map vs flatmap on streams
        System.out.println("\nmap() vs flatMap() on Streams");
        List<String> listOfString = Stream.of("a", "b")
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(listOfString);

        List<List<String>> list = Arrays.asList(
                Arrays.asList("a"),
                Arrays.asList("b"));

        listOfString = list.stream()
                .flatMap(Collection::stream)
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(listOfString);
    }
}
