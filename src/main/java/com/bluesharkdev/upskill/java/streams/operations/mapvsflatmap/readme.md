# Streams
## map() vs flatMap()

- the *map()* and *flatMap()* APIs stem from functional languages
- in Java 8, we find them in *Optional*, *Stream* and in *CompletableFuture* (with a different name)
- *Streams* represent a sequence of objects where optionals are classes that represent a value that can be present or absent
- *map()* and *flatMap()* have the same return type, but are very different
- in summary, flatMap can take nested Streams and make them into a 1 dimensional collection, whereas map cannot
- flatMap will take a Collection<Collection<T>> and turn it into a Collection<T>

```java
//if we have 
List<List<String>> items = List.of(List.of("Laptop", "Phone"), List.of("Mouse", "Keyboard"));

//map will work with a <Stream<List<Item>>
//flatMap will convert <Stream<List<Item>> into Stream<Item>, to which we can then apply map functions
```

## Map and FlatMap in Optionals

- map method works well with optionals if it has the same return type that we need

```java
Optional<String> s = Optional.of("test");
assertEquals(Optional.of("TEST"), s.map(String::toUpperCase));
```

- in more complex cases, we might be given a function that return an Optional too
- in such cases, map would lead to a nested structure, as the map() implementation does an additional wrapping internally
- another example:

```java
assertEquals(Optional.of(Optional.of("STRING")), Optional.of("string").map(s -> Optional.of("STRING")));
```

- we end up with a nested structure of Optional<Optional<String>>
- this is cumbersome, and not something that's desireable in the code
- this is where flatmap comes in
- the same code as above, but using flatMap instead of map, eliminates the nested structure and gives us a flat representation.

```java
assertEquals(Optional.of("STRING"), Optional.of("String").flatMap(s -> Optional.of("STRING"));
```

## Map and FlatMap in Streams

- they work similarly in streams as in optionals
- the *map()* method wraps the underlying sequence in a *Stream* interface, whereas *flatMap* method allows avoiding nested *Stream<Stream<R>>* structures
- in this example *map()* produces a Stream consisting of the results of applying the toUpperCase method to the elements

```java
List<String> myList = Stream.of("a", "b")
        .map(String::uppercase)
        .collect(Collectors.toList());
assertEquals(asList("A", "B"), myList);
```

- map works well in this simple case, but waht if the structure is more complicated?

```java
List<List<String>> list = Arrays.asList(
        Arrays.asList("a"),
        Arrays.asList("b")
        );
```

- we would use flatMap here, since it removes the extraneous wrapping

```java
list.stream().flatMap(Collection::stream).collect(Collectors.toList());
```