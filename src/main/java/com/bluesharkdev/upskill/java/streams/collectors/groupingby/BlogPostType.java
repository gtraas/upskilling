package com.bluesharkdev.upskill.java.streams.collectors.groupingby;

enum BlogPostType {
    REVIEWS,
    NEWS,
    GUIDE
}
