package com.bluesharkdev.upskill.java.streams.collectors.collectorsguide;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static java.util.stream.Collectors.*;

public class StreamsCollectorsGuideExample {

    public static void main(String[] args) {

        List<String> givenList = Arrays.asList("a", "bb", "ccc", "dd");
        List<String> givenListWithDuplicates = Arrays.asList("a", "bb", "ccc", "dd", "bb");

        //collecting to a list
        List<String> collectorToList = givenList.stream().collect(toList());

        //collecting to a set
        Set<String> collectorToSet = givenList.stream().collect(toSet());

        //specifying the collection
        List<String> collectoToCollection = givenList.stream().collect(toCollection(LinkedList::new));

        //collecting to a map
        Map<String, Integer> collectingToMap = givenList.stream()
                .collect(toMap(Function.identity(), String::length));

        //what if there are duplicates in the stream source?
        try {
            Map<String, Integer> collectingDuplicatesToMap = givenListWithDuplicates.stream()
                    .collect(toMap(Function.identity(), String::length));

        } catch (IllegalStateException e) {
            System.out.println("We're not allowed to do that: %s".formatted(e.getMessage()));
        }

        //to get around that, we add an extra parameter, a BinaryOperator
        Map<String, Integer> collectingDuplicatesToMap = givenListWithDuplicates.stream()
                .collect(toMap(Function.identity(), String::length, (item, duplicateItem) -> item));

        //collecting and then
        int collectingAndThen = givenList.stream().collect(collectingAndThen(toList(), List::size));
        System.out.println("Size after collectingAndThen should be 4, is: %s".formatted(collectingAndThen));

        //joining
        String result = givenList.stream().collect(joining());
        System.out.println("joining(): %s".formatted(result));

        result = givenList.stream().collect(joining(" "));
        System.out.println("joining(\" \"): %s".formatted(result));

        result = givenList.stream().collect(joining(" ", "PRE-", "-POST"));
        System.out.println("joining(\" \", \"PRE-\", \"-POST\"): %s".formatted(result));

        //counting
        Long counting = givenList.stream().collect(counting());
        System.out.println("Counting the elements: %s".formatted(counting));

        //summarizingDouble/Int/Long
        DoubleSummaryStatistics stats = givenList.stream().collect(summarizingDouble(String::length));
        System.out.println("Summary stats: %s".formatted(stats));

        //averagingDouble/Int/Long
        Double averagingDouble = givenList.stream().collect(averagingDouble(String::length));
        System.out.println("averagingDouble/Int/Long: %s".formatted(averagingDouble));

        //summingInt/Double/Long
        Double summingDouble = givenList.stream().collect(summingDouble(String::length));
        System.out.println("summingDouble: %s".formatted(summingDouble));

        //maxBy / minBy
        Optional<String> maxBy = givenList.stream().collect(maxBy(Comparator.naturalOrder()));
        System.out.println("maxBy: %s".formatted(maxBy.get()));

        //groupingBy
        Map<Integer, Set<String>> groupingBy = givenList.stream().collect(groupingBy(String::length, toSet()));
        System.out.println("GroupingBy result size should be 3, is: %s".formatted(groupingBy.size()));

        //partitioningBy
        Map<Boolean, List<String>> partitioningBy = givenList.stream().collect(partitioningBy(s -> s.length() > 2));
        System.out.println("Partitioning by: %s".formatted(partitioningBy));

        //teeing
        List<Integer> numbers = Arrays.asList(42, 4, 2, 24);

        Integer difference = numbers.stream().collect(
                teeing(
                        minBy(Integer::compareTo),
                        maxBy(Integer::compareTo),
                        (min, max) -> max.get() - min.get()));

        System.out.println("Difference should be 40, is: %s".formatted(difference));

        //our own custom built Collector
        ImmutableSet<String> customCollectorResult = givenList.stream().collect(toImmutableSet());
        System.out.println("Type of customCollectorResult: %s".formatted(customCollectorResult.getClass()));

    }

    private static class ImmutableSetCollector<T> implements Collector<T, ImmutableSet.Builder<T>, ImmutableSet<T>> {

        @Override
        //generates an empty accumulator
        public Supplier<ImmutableSet.Builder<T>> supplier() {
            return ImmutableSet::builder;
        }

        @Override
        //used for adding a new element to the accumulator
        public BiConsumer<ImmutableSet.Builder<T>, T> accumulator() {
            return ImmutableSet.Builder::add;
        }

        @Override
        //provides a method for combining two accumulators
        public BinaryOperator<ImmutableSet.Builder<T>> combiner() {
            return (left, right) -> left.addAll(right.build());
        }

        @Override
        //used for converting the accumulator to the final result
        public Function<ImmutableSet.Builder<T>, ImmutableSet<T>> finisher() {
            return ImmutableSet.Builder::build;
        }

        @Override
        //provides info about required internal optimizations like ordered vs unordered or whether the collector is concurrent or not.
        public Set<Characteristics> characteristics() {
            return Sets.immutableEnumSet(Characteristics.UNORDERED);
        }

        public static <T> ImmutableSetCollector<T> toImmutableSet() {
            return new ImmutableSetCollector<T>();
        }
     }
}
