# Java 9 New Features

## Multi-Resolution Image API

- interface *java.awt.image.MultiResolutionImage* encapsulates a set of images with different resolutions into 1 object
- can retrieve a resolution-specific image variant based on a given DPI metric and set of image transformations or retrieve all the variants in the image.
- *java.awt.Graphics* class gets variant from a multi-resolution image based on the current display metric and any applied transformations
- class *java.awt.image.BaseMultiResolutionImage* provides basic implementation

```java
BufferedImage[] resolutionVariants = ...
MultiResolutionImage bmrImage = new BaseMultiResolutionImage(baseIndex, resolutionVariants);
Image testRVImage = bmrImage.getResolutionVariant(16.16);
assertSame("Images should be the same", testRVImage, resolutionVariants[3]);
```