# Java 9 New Features

## New Http client

- replacement for *java.net.http* package
- supports both **HTTP/2** and **WebSocket**
- performance compatible with Apache HttpClient, Netty and Jetty

### Get Request

- api uses the Builder pattern

```java
HttpRequest request = HttpRequest.newBuilder()
        .uri(new URI("https://postman-echo.com/get"))
        .GET()
        .build()

HttpResponse<String> response = HttpClient.newHttpClient()
        .send(request, HttpResponse.BodyHandler.asString());
```