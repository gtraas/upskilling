# Java 9 New Features
## New API's
### Immutable Set

- *java.util.Set.of* creates an immutable set of given elements
- In java 8, creating a Set of several elements would require several lines of code
- Now:
```java
Set<String> strKeySet = Set.of("key1", "key2", "key3");
```
- Set returned by this is a JVM internal class of type *java.util.ImmutableCollections.SetN*
- it is immutable, so trying ot add or remove elements results in a *UnsupportedOperationException*
- can also convert an entire array into a *Set* using the same method

### Optional to Stream

- *java.util.Optional.stream()* gives us an easy way to use the power of streams on Optional elements
```java
List<String> filteredList = listOfOptionals.stream()
        .flatMap(Optional::stream)
        .collect(Collectors.toList());
```