# Java 9 new features
## JShell

- a read-eval-print loop
- interactive tool to evaluate declarations, statements and expressions of Java, together with an API
- convenient for testing small code snippets, rather than a new class with a main method
- jshell is executable from the <JAVA_HOME>/bin folder

```
jdk-9\bin>jshell.exe
    Welcome to JShell -- Version 9
    For an introduction type: /help intro
jshell> "This is my long string. I want a part of it".substring(8,19);
$5==> "my long string"
```

- comes with a history and auto-completion. 
- also provides functionality for saving to and loading from files

```
jshell> /save c:/develop/JShell_hello_world.txt
jshell> /open c:/develop/JShell_hello_world.txt
Hello JShell!
```

- code snippets are executed on file loading