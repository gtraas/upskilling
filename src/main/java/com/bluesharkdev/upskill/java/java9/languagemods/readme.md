# Java 9 New Features

## Language modifications

### try-with-resources

- in Java 7, the try-with-resources syntax requires a new variable to be declared for each resource being managed in the statement.
- in Java 9, there is an additional refinement

    - if the resource is referenced by a final or effectively final variable, a try-with-resources statement can manage a resource without a new variable being declared

```java
    MyAutoCloseable mac = new MyAutoCloseable();  
    try (mac) {
        //do some stuff
    }
    
    try (new MyAutoCloseable() {}.finalWrapper.finalCloseable) {
        //do some stuff with finalCloseable
    } catch (Exception e) {}  
         
```

### Diamond operator extension

- can now use diamond operators in conjunction with anonymous inner classes:

```java
    FooClass<Integer> fc = new FooClass<>(1) {
        //anonymous inner class
    }

    FooClass<? extends Integer> fc0 = new FooClass<>(1) {
        //anonymous inner class
    }
    
    FooClass<?> fc1 = new FooClass<>(1) {
        //anonymous inner class
    }

```

### Interface private method
- interfaces can have private methods, which can be used to split lengthy default methods

```java
interface InterfaceWithPrivateMethods {
    private static String staticPrivate() {
        return "static private";
    }
    
    private String instancePrivate() {
        return "instance private";
    }
    
    default void check() {
        String result = staticPrivate();
        InterfaceWithPrivateMethods pvt = new InterfaceWithPrivateMethods() {
            //anonymous inner class
        }
        result = pvt.instancePrivate();
    }
}
```