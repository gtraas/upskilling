# Java 14 Features
## JVM Features

- the ZGC was introduced in Java 1 as experimental
- initially, it was only available on Linux/x64
- now available for Windows and macOS
- set to become production ready in the next release.