# Java 14 Features
## New Features
### Helpful NullPointerExceptions

- Previously, the stack trace for a NPE didn't tell much except that there was a NPE on a certain line of code
- Java has now made debugging easier by indicating exactly what was null in a given line of code
- for example:

```
int[] arr = null;
arr[0] = 1;
```

- earlier, when running this code, the exception would print:

```
Exception in thread "main" java.lang.NullPointerException
at com.demo.MyClass.main(MyClass.java:27)
```

- but now, the same scenario might give the following:

```
java.lang.NullPointerException: Cannot store to int array because "arr" is null
```

- we can now pinpoint exactly which variable was null