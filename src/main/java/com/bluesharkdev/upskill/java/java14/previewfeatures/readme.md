# Java 14 Features
## New preview features
### Pattern matching for instanceof

- introduced to reduce boilerplate code
- before, we would od this:

```
if (obj instanceof String) {
    String str = (String) obj;
    int len = str.length();
    // ...
}
```

- with the new feature, we can do this:

```
if (obj instanceof String str) {
    int len = str.length();
    // ...
}
```

### Records

- records were introduced to remove boilerplate code in data model POJOs
- simplify day to day development, improve efficiency and reduce the risk of human error
- A data model for a *User* with an id and password can be written as such:

```
public record User(int id, String password) { }
```

- this declaration will add a constructor, getters, equals, hashCode and toString methods for us