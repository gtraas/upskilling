# Java 14 New Features
## Features from previous versions
### switch expressions

- introduced in Java 12 and Java 13, they are now standardised and included as official features in Java 14
- previously we would have to do this:

```
boolean todayIsAHoliday;
switch (day) {
    case "MONDAY":
    case "TUESDAY":
    case "WEDNESDAY":
    case "THURSDAY":
    case "FRIDAY":
        isTodayAHoliday = false;
        return;
    case "SATURDAY":
    case "SUNDAY":
        isTodayAHoliday = true;
        return;
    default:
        throw new IllegalArgumentException("What's a " + day);
}
```

- now we can do this:

```
boolean isTodayAHoliday = switch (day) {
    case "MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY" -> false;
    case "SATURDAY","SUNDAY" -> true;
    default -> throw new IllegalArgumentException("What's a " + day)
};
```

### Text blocks

- these are still a preview feature but have two additional escape sequences
- \: to indicate the end of a line, so that a new line character is not introduced
- \s: to indicate a single space
- for example:

```
String multiline = "A quick brown fox jumps over the lazy dog; the lazy dog howls loudly";
```

- can now be written as:

```
String multiline = """
    A quick brown fox jumps over the lazy dog; \
    the lazy dog howls loudly.""";
```

- this improves readability for the human eye, but doesn't actually add a new line to the string.