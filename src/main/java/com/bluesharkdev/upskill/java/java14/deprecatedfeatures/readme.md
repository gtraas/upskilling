# Java 14 Features
## Deprecated and Removed Features
### Deprecated features

- Solaris and SPARC ports - because this Unix operating system and RISC processor are not in active development for the past few years
- *ParallelScavenge* + *SerialOld* garbage collection combination - since this is a rarely used combination of GC algorithms and requires significant maintenaince effort

### Removed features

- The Concurrent MarkSweep GC - deprecated since Java 9, this has been succeeded by G1 as the default GC. Also, there are more performant options available to use, such as Shenandoah and ZGC
- Pack200 Tools and API - deprecated for removal in Java 11, and have now been removed. 

