# The *synchronized* keyword
## Overview
In a multi-threaded environment, a race condition occurs when two or more threads attempt to update mutable shared
data at the same time. Java offers a mechanism to avoid race conditions by synchronizing thread access.

A piece of logic marked with *synchronized* becomes a synchronized block, allowing only 1 thread to execute it at any one 
time.

## Why synchronization?
Let's consider a typical race condition where we calculate the sum and multiple threads execute the *calculate()* method:
````
public class BaeldungSynchronizedMethods {
 
    private int sum = 0;
 
    public void calculate() {
        setSum(getSum() + 1);
    }
 
    // standard setters and getters
}
````

And a simple test:
```
@Test
public void givenMultiThread_whenNonSyncMethod() {
    ExecutorService service = Executors.newFixedThreadPool(3);
    BaeldungSynchronizedMethods summation = new BaeldungSynchronizedMethods();
 
    IntStream.range(0, 1000)
      .forEach(count -> service.submit(summation::calculate));
    service.awaitTermination(1000, TimeUnit.MILLISECONDS);
 
    assertEquals(1000, summation.getSum());
}
```

We're just using the *ExecutorService* with a 3-thread poolto execute the *calculate()* 1000 times.

If we were to execute this serially, we would get 1000 every time, but because we're using multithreading, this test
will fail almost every time. For instance:
````
java.lang.AssertionError: expected:<1000> but was:<965>
at org.junit.Assert.fail(Assert.java:88)
at org.junit.Assert.failNotEquals(Assert.java:834)
```` 
Not an unexpected result!

The simplest way to avoid this and make the operation thread-safe is to add the *synchronized* keyword.

## The synchronized keyword

The *synchronized* keyword can be used on three levels:
- Instance methods
- static methods
- code blocks

When we use a synchronized block, internally, Java uses a monitor (known as a monitor lock or intrinsic lock) to provide
synchronization. These monitors are bound to an object, thus all synchronized blocks of the same object can have only one
thread executing them at the same time.

### synchronized instance methods
We can simply add the *synchronized* keyword in the method declaration to make the method synchronized:
````
public synchronized void synchronisedCalculate() {
    setSum(getSum() + 1);
}
````

Notice that once we synchronize the method, the test case passes with actual output as 1000.

Instance methods are synchronized over the instance of the class owning the method. Thus, only one thread **per instance**
of the class can execute this method.

### synchronized static methods
Static methods are synchronized exactly like instance methods:
````
public static synchronized void syncStaticCalculate() {
    staticSum = staticSum + 1;
}
````

These methods are synchronized on the *Class* object associated with the class, and since only one class object exists
per JVM per class, only one thread can execute inside a static synchronized method per class. 

### synchronized code blocks
Sometimes we only want to synchronise a part of the method, not the entire method. To acheive this, we simply add a 
synchronised block:
````
public void performSynchrinisedTask() {
    synchronized (this) {
        setCount(getCount()+1);
    }
}
````

Notice that we passed a parameter to the synchronized block. This is the monitor object. The code inside the block gets 
synchronised on the monitor object. Simply put, only one thread per monitor object can execute inside that block of code.

If the method were static, we could simply pass the class to the synchronised block.
````
public static void performStaticSyncTask(){
    synchronized (SynchronisedBlocks.class) {
        setStaticCount(getStaticCount() + 1);
    }
}
```` 

