# Thread Pools

## The Thread Pool
In Java, threads are mapped to system-level threads which are operating system's resources. If threads are created
uncontrollably, these resources may run out quickly.

Context switching between threads is done by the operating system as well - in order to emulate parallelism. A simplistic 
view is that the more threads you create, the less time each thread will spend executing.

The Thread Pattern helps to save resources in a multithreaded environment, and also to contain the parallelism in 
certain predefined limits.

When you use a thread pool, you write your concurrent code in the form of parallel tasks and submit them for execution to 
a thread pool instance. This instance controls several re-used threads for executing these tasks.

![Thread pool](../../../../../../../../resources/images/java/concurrency/threadpools.png)

The pattern allows you to control the number of threads the application is creating, their lifecycles as well as to 
schedule tasks' execution and keep incoming tasks in a queue.

## Thread pools in Java

### Executors, Executor and ExecutorService

The *Executors* class contains several methods for creating pre-configured thread pools. These are a good place to start
because it requires no custom fine-tuning.

The *Executor* and *ExecutorService* interfaces are used to work with different thread pool implementations in Java.
Usually, you should keep your code decoupled from the implementation of the thread pool, and use these interfaces
throughout your application.

The *Executor* interface has a single *execute* method to submit *Runnable* instances for execution. 

This is how you can use the *Executors* API to acquire an *Executor* instance backed by a single thread pool and an
unbounded queue for executing tasks sequentially. Here we execute a single task that simply prints 'Hello world' to 
the stout. The task is submitted as a lambda which is inferred to be *Runnable*.
```
Executor executor = Executors.newSingleThreadExecutor();
executor.execute(() -> System.out.println("Hello World"));
```

The *ExecutorService* interface contains a large number of methods for controlling the progress of tasks and managing the 
termination of the service. Using this interface, you can submit the tasks for execution, and also control their execution 
using the returned *Future* instance.

In the following example, we create an *ExecutorService*, submit a task and then use the returned *Future*'s get method 
to wait until the submitted task is finished and the value returned:
```
ExecutorService executorService = Executors.newFixedThreadPool(10);
Future<String> future = executorService.submit(() -> "Hello World");
// some operations
String result = future.get();
```

The *submit* method is overloaded to take either a *Callable* or *Runnable*. Both of these are functional interfaces, 
and can be passed as lambdas.

*Runnable*'s single method does not throw an exception and does not return a value. The *Callable* interface, however,
may be more convenient as it throws an exception and returns a value.

Finally, to let the compiler infer the *Callable* type, simply return a value from the lambda.

### ThreadPoolExecutor
The *ThreadPoolExecutor* is an extensible thread pool implementation with lots of parameters and hooks for fine-tuning.
The main configuration parameters that we'll be looking at are:
- corePoolSize
- maximumPoolSize
- keepAliveTime

The pool consists of a fixed number of core threads that are kept inside at all times, and some excess threads that can be
spawned and then termianted as needed. The *corePoolSize* parameter is the amount of core threads which will be instantiated
and kept in the pool. If all core threads are busy, and more tasks are submitted, the pool is permitted to grow to
*maximiumPoolSize*. The *keepAliveTime* is the interval of time for which excess threads are allowed to exist in the 
idle state.

These parameters cover a wide range of use cases, but the most typical configurations are predefined in the 
*Executors* static methods. For example, the *newFixedThreadPool* method creates a *ThreadPoolExecutor* with equal
*corePoolSize* and *maximumPoolSize* values and a zero *keepAliveTime*. This means the number of threads in this pool 
are always the same.
```
ThreadPoolExecutor executor = 
  (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
executor.submit(() -> {
    Thread.sleep(1000);
    return null;
});
executor.submit(() -> {
    Thread.sleep(1000);
    return null;
});
executor.submit(() -> {
    Thread.sleep(1000);
    return null;
});
 
assertEquals(2, executor.getPoolSize());
assertEquals(1, executor.getQueue().size());
```

In this example, we create a fixed thread pool of size 2. This means that if the amount of simultaneously running tasks
is less than or equal to 2, then they will get executed straight away. Otherwise, they will be added to the queue. We 
created 3 *Callable* tasks that imitate heavy work by sleeping for 1 second. The first 2 tasks are executed at once. 
The third task will be added to the queue. We verify this by calling *executor.getPoolSize()* and 
*executor.getQueue().getSize()*.

Another pre-configured *ThreadPoolExecutor* can be created with the *Executors.newCachedThreadPool()* method. This method 
does not receive a number of threads. *corePoolSize* is set to 0, and *maximumPoolSize* is set to Integer.MAX_VALUE.
*keepAlive* is set to 60 seconds. This means that the cached pool thread can grow without bounds to accomodate any 
submitted tasks. When the threads aren't needed anymore, they will be disposed of after 60 seconds of inactivity. 
A typical use-case is when you have a large number of short-lived tasks in your application.
```
ThreadPoolExecutor executor = 
  (ThreadPoolExecutor) Executors.newCachedThreadPool();
executor.submit(() -> {
    Thread.sleep(1000);
    return null;
});
executor.submit(() -> {
    Thread.sleep(1000);
    return null;
});
executor.submit(() -> {
    Thread.sleep(1000);
    return null;
});
 
assertEquals(3, executor.getPoolSize());
assertEquals(0, executor.getQueue().size());
```

The queue size in the above example will always be 0, because internally, a *SynchronousQueue* instance is used. 
In a *SynchronousQueue*, pairs of *insert* and *remove* operations always occur simultaneously, so the queue never 
actually contains anything.

The *Executors.newSingleThreadExecutor()* API creates another typical form of ThreadPoolExecutor which contains a 
single thread. This is ideal for setting up an event loop. The *corePoolSize* and *maximumPoolSize* are 1 and the
*keepAliveTime* is 0.

Tasks in the following example will be executed sequentially, so the flag value will be 2 after completion.
```
AtomicInteger counter = new AtomicInteger();
 
ExecutorService executor = Executors.newSingleThreadExecutor();
executor.submit(() -> {
    counter.set(1);
});
executor.submit(() -> {
    counter.compareAndSet(1, 2);
});
```  
This *ThreadPoolExecutor* is decorated with an immutable wrapper, so it cannot be reconfigured after creation. This is 
why we can't cast it to *ThreadPoolExecutor*.

### ScheduledThreadPoolExecutor

The *ScheduledThreadPoolExecutor* extends the *ThreadPoolExecutor* class and also implements the *ScheduledExecutorService*
interface with several additional methods.
- *schedule* method allows us to execute a task once after a specified delay.
- *scheduleAtFixedRate* method allows us to execute a task after a specified inital delay and then execute it repeatedly
within a certain period: the period argument is the time **measured between the starting times of the tasks**, so the 
execution rate is fixed.
- *scheduleWithFixedDelay* method is similar to *scheduleAtFixedRate* in that it repeatedly executes a given task, but the
specified delay is measured between the end of the previous task and the start of the next. As such, the execution rate 
may vary depending on the time it takes to execute any given task.

The *Executors.newScheduledThreadPool()* method is typically used to create a *ScheduledThreadPoolExecutor* with a 
given *corePoolSize*, and unbounded *maximumPoolSize* and a zero *keepAliveTime*. Here's how to schedule a task for 
execution in 500 milliseconds:
```
ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
executor.schedule(() -> {
    System.out.println("Hello World");
}, 500, TimeUnit.MILLISECONDS);
```

The following code shows how to execute a task after 500 milliseconds delay, and then repeat it every 100 milliseconds.
After scheduling the task, we wait until it fires 3 times using the *CountDownLatch* lock, and then cancel it using 
the *Future.cancel()* method:
```
CountDownLatch lock = new CountDownLatch(3);
 
ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
ScheduledFuture<?> future = executor.scheduleAtFixedRate(() -> {
    System.out.println("Hello World");
    lock.countDown();
}, 500, 100, TimeUnit.MILLISECONDS);
 
lock.await(1000, TimeUnit.MILLISECONDS);
future.cancel(true);
```

### ForkJoinPool
*ForkJoinPool* is the central part of the fork/join framework introduced in Java 7. It solves s common problem of 
spawning multiple tasks in recursive algorithms. Using a simple *ThreadPoolExecutor*, you will run out of threads
very quickly, as every task or subtask requires its own thread to run.

In a fork/join framework, any task can spawn (fork) a number of subtasks and wait for their completion using the join
method. The benefit of the fork/join framework is that it does not create a new thread for each task or subtask. 
It implements the WorkStealing algorithm instead.

Let's see a simple example of using *ForkJoinPool* to traverse a tree of nodes and calculate the sum of all leaf values.

This is the tree implementation
```
static class TreeNode {
 
    int value;
 
    Set<TreeNode> children;
 
    TreeNode(int value, TreeNode... children) {
        this.value = value;
        this.children = Sets.newHashSet(children);
    }
}
```

Now, if we want to sum all values in the tree in parallel, we need to implement a *RecursiveTask<Integer>* interface.
Each task receives its own node and adds its value to the sum of the values of its children. To calculate the sum of the 
children values, we do the following:
- stream the children set
- maps over this stream, creating a new *CountingTask* for each element
- executes each subtask by forking it
- collects the results by calling the join method on each forked task
- sums the results using the *Collectors.summingInt* collector

```
public static class CountingTask extends RecursiveTask<Integer> {
 
    private final TreeNode node;
 
    public CountingTask(TreeNode node) {
        this.node = node;
    }
 
    @Override
    protected Integer compute() {
        return node.value + node.children.stream()
          .map(childNode -> new CountingTask(childNode).fork())
          .collect(Collectors.summingInt(ForkJoinTask::join));
    }
}
```

The code to run the calculation on the actual tree is as follows:
```
TreeNode tree = new TreeNode(5,
  new TreeNode(3), new TreeNode(2,
    new TreeNode(2), new TreeNode(8)));
 
ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
int sum = forkJoinPool.invoke(new CountingTask(tree));
```