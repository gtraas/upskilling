package com.bluesharkdev.upskill.java.concurrency.basic.overview.semaphore;

import java.util.concurrent.Semaphore;

public class SemaphoreExample {

    static Semaphore semaphore = new Semaphore(10);

    public void execute() throws InterruptedException {

        System.out.println("Available permit : " + semaphore.availablePermits());
        System.out.println("Number of threads waiting to acquire: " + semaphore.getQueueLength());

        if (semaphore.tryAcquire()) {
            try {
                // perform some critical operations
            } finally {
                semaphore.release();
            }
        }

    }

    public static void main(String... args) {
        try {
            //instead of calling it here, we'd call from within the run() method of a thread.
            new SemaphoreExample().execute();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
