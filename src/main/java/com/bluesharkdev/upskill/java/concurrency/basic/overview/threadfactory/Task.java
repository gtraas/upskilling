package com.bluesharkdev.upskill.java.concurrency.basic.overview.threadfactory;

public class Task implements Runnable {
    public int threadNum;

    public Task(int threadNumber) {
        this.threadNum = threadNumber;
    }

    @Override
    public void run() {
        System.out.println("Task " + threadNum + " running");
    }
}
