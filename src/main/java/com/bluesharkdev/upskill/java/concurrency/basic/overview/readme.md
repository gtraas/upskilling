# Overview of java.util.concurrent

## Main components

- Executor
- ExecutorService
- ScheduledExecutorService
- Future
- CountDownLatch
- CyclicBarrier
- Semaphore
- ThreadFactory
- BlockingQueue
- DelayQueue
- Locks
- Phaser

### Executor
Is an interface that represents an object that executes provided tasks. 

It depends on the particular implementation (i.e. from where the invocation is initiated) if the task should be run on a new
or the current thread. Hence, using this interface, we can decouple the task execution flow from the actual task
execution mechanism.

One point to note here is that Executor does not strictly require the task execution to be asynchronous. In the simplest case, an executor can invoke the submitted task instantly in the invoking thread.

We need to create an invoker to create the executor instance:

```
public class Invoker implements Executor {
    @Override
    public void execute(Runnable r) {
        r.run();
    }
}
```
Now, we can use this invoker to execute the task.

```
public void execute() {
    Executor executor = new Invoker();
    executor.execute( () -> {
        // task to be performed
    });
}
```
Point to note here is that if the executor can't accept the task for execution, it will throw *RejectedExecutionException*.

### ExecutorService
ExecutorService is a complete solution for asynchronous processing. It manages an in-memory queue and schedules submitted 
tasks based on thread availability.

To use ExecutorService, we need to create one *Runnable* class.

```
public class Task implements Runnable {
    @Override
    public void run() {
        // task details
    }
}
```

Now we can create the ExecutorService instance and assign this task to it. At the time of creation of the ExecutorService,
we'll need to provide the thread-pool size.

```
ExecutorService executor = Executors.newFixedThreadPool(10);
```

If we only want a single-threaded ExecutorService, we can use *newSingleThreadExecutor(ThreadFactory threadFactory)* to create
the instance

Once the executor is created, we can use it to submit the task:
```
public void execute() { 
    executor.submit(new Task()); 
}
```

We could also create the *Runnable* instance while submitting the task:
```
executor.submit(() -> {
    new Task();
});
```

ExecutorService also comes with 2 out of the box execution termination methods. The first is *shutdown()*. This waits 
until all the submitted tasks finish executing. The other is *shutdownNow()*, which immediately terminates all the 
pending / executing tasks.

There is also another method, *awaitTermination(long timeout, TimeUnit timeunit)*, which forcefully blocks until:
- all tasks have completed execution after a shutdown event was triggered or 
- execution-timeout occurred or
- the execution thread itself is interrupted

```
try {
    executor.awaitTermination( 20l, TimeUnit.NANOSECONDS );
} catch (InterruptedException e) {
    e.printStackTrace();
}
```

### ScheduledExecutorService
It is similar to the ExecutorService, but it can schedule commands to run after a given delay, or to execute periodically.

Executor and ExecutorService tasks are not scheduled, but rather run instantly. To provide the same behaviour with
the ScheduledExecutorService, we'd provide a negative or zero value.

```
public void execute() {
    ScheduledExecutorService executorService
      = Executors.newSingleThreadScheduledExecutor();
 
    Future<String> future = executorService.schedule(() -> {
        // ...
        return "Hello world";
    }, 1, TimeUnit.SECONDS);
 
    ScheduledFuture<?> scheduledFuture = executorService.schedule(() -> {
        // ...
    }, 1, TimeUnit.SECONDS);
 
    executorService.shutdown();
}
```

It can also schedule a task after a given delay:
```
executorService.scheduleAtFixedRate(() -> {
    // ...
}, 1, 10, TimeUnit.SECONDS);
 
executorService.scheduleWithFixedDelay(() -> {
    // ...
}, 1, 10, TimeUnit.SECONDS);
```

In the above, the *scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit)* method creates and
executes a periodic action that is invoked firstly after the provided *initialDelay* and subsequently with the given *period* 
**until the service instance is shut down**.

The *scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit)* method creates and executes 
a periodic action that is invoked firstly after the provided *initialDelay* and repeatedly with the given *delay* between 
the termination of the executing one and the invocation of the next one.

An easier way to understand the difference is this:
- scheduleAtFixedRate: Creates and executes a periodic action that becomes enabled first after the given initial delay, 
and subsequently with the given period; that is executions will commence after *initialDelay* then *initialDelay+period*, 
then *initialDelay + (2 * period)*, and so on.
- scheduleWithFixedDelay: Creates and executes a periodic action that becomes enabled first after the given initial delay, 
and subsequently with the given delay between the termination of one execution and the commencement of the next.

### Future
This is used to represent the result of an asynchronous operation. It comes with methods for checking if the asynchronous
operation is completed or not, getting the completed result, etc.

The *cancel(boolean mayInterruptIfRunning)* cancels the operation and releases the executing thread. If the *mayInterruptIfRunning*
is true, the thread executing the task will be terminated instantly. Otherwise, the thread will be allowed to complete 
its execution.

We can create a future instance like this:
```
public void invoke() {
    ExecutorService executorService = Executors.newFixedThreadPool(10);
 
    Future<String> future = executorService.submit(() -> {
        // ...
        Thread.sleep(10000l);
        return "Hello world";
    });
}
```
 
 We can check if the future result is done, and fetch the data if the computation is complete:
````


 if (future.isDone() && !future.isCancelled()) {
     try {
         str = future.get();
     } catch (InterruptedException | ExecutionException e) {
         e.printStackTrace();
     }
 }
````
 
 We can also specify a timeout for an operation. If the task takes more time than provided, a TimeoutException is thrown:
````
try {
     future.get(10, TimeUnit.SECONDS);
 } catch (InterruptedException | ExecutionException | TimeoutException e) {
     e.printStackTrace();
 }
````
 
### CountDownLatch
This was introduced in JDK 5, and it blocks a set of threads until some operation completes. A *CountDownLatch* is initialised
with a *counter(Integer type)*. This counter decrements as the dependent threads complete their execution. Once the 
counter reaches zero, the other threads are released. 
 
### CyclicBarrier
*CyclicBarrier* works almost the same as *CountDownLatch*, except that we can reuse it. Unlike *CountDownLatch*, it allows 
multiple threads to wait for each other using the *await()* method (known as a barrier condition) before invoking the 
final task.
 
We need to create a *Runnable* task instance to initiate the barrier condition:
```
public class Task implements Runnable {
  
    private CyclicBarrier barrier;
  
    public Task(CyclicBarrier barrier) {
        this.barrier = barrier;
    }
  
    @Override
    public void run() {
        try {
            LOG.info(Thread.currentThread().getName() + 
              " is waiting");
            barrier.await();
            LOG.info(Thread.currentThread().getName() + 
              " is released");
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
 
}
```

Now we can invoke some threads to race for the barrier condition:
```
public void start() {
 
    CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> {
        // ...
        LOG.info("All previous tasks are completed");
    });
 
    Thread t1 = new Thread(new Task(cyclicBarrier), "T1"); 
    Thread t2 = new Thread(new Task(cyclicBarrier), "T2"); 
    Thread t3 = new Thread(new Task(cyclicBarrier), "T3"); 
 
    if (!cyclicBarrier.isBroken()) { 
        t1.start(); 
        t2.start(); 
        t3.start(); 
    }
}
``` 
 
Here, the *isBroken()* method checks if any of the threads got interrupted during the execution time. We should always 
perform this check before performing the actual process.
 
### Semaphore
The *Semaphore* is used for blocking thread level access to some part of the physical or logical reasource. A semaphore
contains a set of permits; whenever a thread tries to enter the critical section, it needs to check the semaphore if a 
permit is available or not.
 
If a permit is not available (via *try(Acquire()*), the thread is not allowed to jump into the critical section. However,
if the permit is available, the access is granted and the permit counter decreases.
 
Once the executing thread releases the critical section, the permit counter increases (Done by the *release()*).
 
We can specify a timeout for acquiring access by using the *tryAcquire(long timeout, TimeUnit unit)* method.
 
We can also check the number of available permits or the number of threads waiting to acquire the semaphore.
 
### ThreadFactory
The *ThreadFactory* acts a thread (non-existing) pool which creates a new thread on demand. It eliminates the need 
for a lot of the boilerplate coding for implementing efficient thread mechanisms.
 
Thread factories can be defined as follows:
```
public class BaeldungThreadFactory implements ThreadFactory {
    private int threadId;
    private String name;
  
    public BaeldungThreadFactory(String name) {
        threadId = 1;
        this.name = name;
    }
 
    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, name + "-Thread_" + threadId);
        LOG.info("created new thread with id : " + threadId +
            " and name : " + t.getName());
        threadId++;
        return t;
    }
}
``` 
 
We can then use this *newThread(Runnable)* to create a new thread at runtime:
```
BaeldungThreadFactory factory = new BaeldungThreadFactory( 
    "BaeldungThreadFactory");
for (int i = 0; i < 10; i++) { 
    Thread t = factory.newThread(new Task());
    t.start(); 
}
```
 
### BlockingQueue
In asynchronous programming, one of the most common integration patterns is the *producer-consumer* pattern. The *java.util.concurrent*
package comes with a data-structure known as a *BlockingQueue* - which can be very useful in these situations.

### DelayQueue
*DelayQueue* is an infinite-size blocking queue of elements, where an element can only be pulled if its expiration time 
(known as user-defined delay) is  completed. Hence, the topmost element will have the most amount of delay and it will 
be polled last. 
 
### Locks
Not surprisingly, *Lock* is a utility for blocking other threads from accessing a certain segment of code, apart from the 
thread that is currently executing it.
 
The main difference between a lock and a synchronized block is that a synchronised block is fully contained in a method. However,
we can have Lock API's *lock()* and *unlock()* operations on seperate methods.
 
### Phaser
*Phaser* is a more flexible solution than *CyclicBarrier* and *CountDownLatch* - used to act as a reuseable barrier on 
which a dynamic number of threads need to wait before continuing execution. We can coordinate multiple phases of execution,
reusing the *Phaser* instance for each program phase.