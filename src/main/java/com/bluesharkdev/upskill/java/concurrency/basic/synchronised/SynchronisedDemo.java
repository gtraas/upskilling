package com.bluesharkdev.upskill.java.concurrency.basic.synchronised;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class SynchronisedDemo {

    //unsynchronized code
    public void unsynchronisedMethod() throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(3);
        UnsynchronisedCode summation = new UnsynchronisedCode();

        IntStream.range(0, 1000)
                .forEach(count -> service.submit(summation::calculate));
        service.awaitTermination(1000, TimeUnit.MILLISECONDS);

        System.out.println("Should be 1000, is: " + summation.getSum());

        service.shutdownNow();
    }

    //synchronized code block
    public void synchronisedBlockTest() throws InterruptedException {
        ExecutorService service = Executors.newCachedThreadPool();

        IntStream.range(0, 1000)
                .forEach(count -> service.submit(SynchronisedBlocks::performStaticSyncTask));
        service.awaitTermination(500, TimeUnit.MILLISECONDS);

        System.out.println("Should be 1000, is: " + SynchronisedBlocks.getStaticCount());

        service.shutdownNow();
    }

    //synchronised instance method
    public void synchronisedInstanceMethodTest() throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(3);
        SynchronisedMethods method = new SynchronisedMethods();

        IntStream.range(0, 1000)
                .forEach(count -> service.submit(method::synchronisedCalculate));
        service.awaitTermination(100, TimeUnit.MILLISECONDS);

        System.out.println("Should be 1000, is: " + method.getSyncSum());

        service.shutdownNow();
    }

    //synchronized static method
    public void synchronizedStaticMethodTest() throws InterruptedException {
        ExecutorService service = Executors.newCachedThreadPool();

        IntStream.range(0, 1000)
                .forEach(count -> service.submit(SynchronisedMethods::syncStaticCalculate));
        service.awaitTermination(100, TimeUnit.MILLISECONDS);

        System.out.println("Should be 1000, is: " + SynchronisedMethods.staticSum);

        service.shutdownNow();
    }

    public static void main(String... args) {
        try {
            SynchronisedDemo sd = new SynchronisedDemo();
            sd.unsynchronisedMethod();
            sd.synchronisedBlockTest();
            sd.synchronisedInstanceMethodTest();
            sd.synchronizedStaticMethodTest();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
