package com.bluesharkdev.upskill.java.concurrency.basic.futures;

import java.util.concurrent.*;

public class FutureDemo {

    public String invoke() {

        String str = null;

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Future<String> future = executorService.submit(() -> {
            // Task
            System.out.println("I be sleeping for 10 seconds");
            Thread.sleep(10000l);
            return "Hello world";
        });

//        future.cancel(false);

        try {
            System.out.println("Future.get with a 20 second timeout");
            future.get(20, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e1) {
            e1.printStackTrace();
        }

        if (future.isDone() && !future.isCancelled()) {
            try {
                str = future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        executorService.shutdown();

        return str;

    }

    public static void main(String... args) {
        System.out.println("Demonstrating futures...");
        String s = new FutureDemo().invoke();
        System.out.println("Result is: " + s);

        System.out.println();
        System.out.println("The factorial square calculator...");
        Integer result = new FactorialSquareCalculator(4).compute();
        System.out.println("Result should be 30, is: " + result);
    }

}
