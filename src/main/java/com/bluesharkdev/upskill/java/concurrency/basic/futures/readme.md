# Futures
## Creating Futures
The *Future* class represents a future result of an asynchronous computation - a result that will eventually appear 
in the Future, after the processing is complete.

Long running methods are good candidates for asynchronous processing and the *Future* interface. This enables us to 
execute some other process while we are waiting for the task encapsulated in *Future* to complete.

Some examples of operations that would leverage the async nature of the *Future*:
- computational intensive processes (mathematical, statistical and scientific calculations)
- manipulating large data structures (big data)
- remote method calls (downloading files, HTML scraping, web services)

### Implementing Futures with FutureTask
For our example, we're going to create a very simple class that calculates the square of an Integer. This definitely doesn't
fit the "long-running" method category, but we'll put a Thread.sleep call on it to make it last a second.
```
public class SquareCalculator {    
     
    private ExecutorService executor 
      = Executors.newSingleThreadExecutor();
     
    public Future<Integer> calculate(Integer input) {        
        return executor.submit(() -> {
            Thread.sleep(1000);
            return input * input;
        });
    }
}
```
The bit of code that actually performs the calculation is contained within the *call()* method, supplied here as a lambda
expression. As you can see, aside from the Thread.sleep, there's nothing special about it.

It does get more interesting when we direct our attention to the usage of *Callable* and *ExecutorService*.

*Callable* is an interface representing a task that returns a result and has a single *call()* method. Here, we've created
it with a lambda expression.

Creating an instance of *Callable* doesn't take us anywhere, we still have to pass this instance to an executor that will 
take care of starting that task in a new thread and give us back the valuable *Future* object. That's where 
*ExecutorService* comes in.

There are a few ways we can get hold of the *ExecutorService* instance, mostly via the Executor's static factory methods.
In this example, we used the *newSingleThreadExecutor()*, which gives us an Executor capable of handling one thread only.

Once we have the ExecutorService object, we just need to call *submit()*, passing our Callable as an argument. *submit()*
will take care of starting the task and return a *FutureTask* object, which is an implementation of the Future interface.

## Consuming Futures
### Using isDone() and get() to obtain results

Now we need to call *calculate()* and use the returned Future to get the resulting Integer. Two methods from the Future API
will help us with this task.

*Future.isDone()* tells us if the executor has finished processing the task. If the task is complete, it will return 
true, otherwise false.

The method that returns the actual result from the calculation is *Future.get()*. Notice that this method blocks the execution
until the task is complete. In our example, this won't be an issue, since we call isDone() first.

By using these methods, we can run some other code while we wait for the main task to finish:
```
Future<Integer> future = new SquareCalculator().calculate(10);
 
while(!future.isDone()) {
    System.out.println("Calculating...");
    Thread.sleep(300);
}
 
Integer result = future.get();
```
In this example, we write a simple message to the output, to let the user know that we're calculating things. 

The method *get()* will block the execution until the task is complete. Not a problem for us, since we're checking if 
the task is finished first. It's worth mentioning an overloaded version of *get()* that takes a timeout and a TimeUnit
as arguments.

```
Integer result = future.get(500, TimeUnit.MILLISECONDS);
```

This method will throw a *TimeoutException* if the task doesn't return before the specified timeout period.

### Cancelling a Future with *cancel()*

Suppose we've triggered a task, but for some reason, we don't really care about the result anymore. We can use 
*Future.cancel(boolean)* to tell the executor to stop the operation and interrupt its underlying thread.

```
Future<Integer> future = new SquareCalculator().calculate(4);
 
boolean canceled = future.cancel(true);
```

Our instance of *Future* from the code above would never complete its operation. In fact, if we try to call *get()* from
that instance, after the call to *cancel()*, we would get a *CancellationException*. *Future.isCancelled()* will tell us
if a Future has been cancelled. This can be quite useful for avoiding a *CancellationException*.
 
It is possible that a call to *cancel()* fails. In that case, its returned value will be false. Also notice that 
*cancel()* takes a boolean value as an argument. This controls whether the thread executing this task should be 
interrupted or not.

## More multithreading with *Thread* pools

At this point, we have an *ExecutorService* that is single-threaded. It uses *Executors.newSingleThreadExecutor*.
Let's look at the code and the output to demonstrate this:
```
SquareCalculator squareCalculator = new SquareCalculator();
 
Future<Integer> future1 = squareCalculator.calculate(10);
Future<Integer> future2 = squareCalculator.calculate(100);
 
while (!(future1.isDone() && future2.isDone())) {
    System.out.println(
      String.format(
        "future1 is %s and future2 is %s", 
        future1.isDone() ? "done" : "not done", 
        future2.isDone() ? "done" : "not done"
      )
    );
    Thread.sleep(300);
}
 
Integer result1 = future1.get();
Integer result2 = future2.get();
 
System.out.println(result1 + " and " + result2);
 
squareCalculator.shutdown();
```

The output:
```
calculating square for: 10
future1 is not done and future2 is not done
future1 is not done and future2 is not done
future1 is not done and future2 is not done
future1 is not done and future2 is not done
calculating square for: 100
future1 is done and future2 is not done
future1 is done and future2 is not done
future1 is done and future2 is not done
100 and 10000
```

We may as well have just done this without any kind of threading. To harness the power of threading, let's use something
other than *newSingleThreadExecutor*. Let's use *newFixedThreadPool()* instead.

```
public class SquareCalculator {
  
    private ExecutorService executor = Executors.newFixedThreadPool(2);
     
    //...
}
```

Now we can use 2 simultaneous threads. Let's see the difference in our output:
```
calculating square for: 10
calculating square for: 100
future1 is not done and future2 is not done
future1 is not done and future2 is not done
future1 is not done and future2 is not done
future1 is not done and future2 is not done
100 and 10000
```

We can now see the 2 threads starting simultaneously. Execution is also quicker, since we're running in parallel.

There are a few other factory methods that we can use too: 
- *newCachedThreadPool()* supplies a pool of Threads that can be re-used when they are available.
- *newScheduledThreadPool()* schedules commands to run after a given delay

## Overview of *ForkJoinTask*

*ForkJoinTask* is an abstract class that implements Future, and is capable of running a large number of tasks with
a relatively small number of actual threads.

The main characteristic of *ForkJoinTask* is that it will spawn a large amount of subtasks to complete its main task.
It does this by generating new tasks using the *fork()* method, and then gathering the results using *join()*. Hence
the name of the class...

Two abstract classes that are of interest that implement ForkJoinTask:
- *RecursiveTask*: return a value on completion
- *RecursiveAction*: does not return a value on completion

These classes are used for recursive actions like navigating file systems, or complex mathematical computations.

To show what this does, let's relook at our example. We now want to calculate the sum of squares for all its factorial
elements. So, if we pass the number 4 to this function, we should get the result from the sum of 4<sup>2</sup> + 
3<sup>2</sup> + 2<sup>2</sup> + 1<sup>2</sup>, which is 30.

So let's see the code:
```
public class FactorialSquareCalculator extends RecursiveTask<Integer> {
  
    private Integer n;
 
    public FactorialSquareCalculator(Integer n) {
        this.n = n;
    }
 
    @Override
    protected Integer compute() {
        if (n <= 1) {
            return n;
        }
 
        FactorialSquareCalculator calculator 
          = new FactorialSquareCalculator(n - 1);
 
        calculator.fork();
 
        return n * n + calculator.join();
    }
}
```

Notice how we can achieve recursiveness by creating a new instance of *FactorialSquareCalculator* within *compute*.
By calling *fork()*, which is a non-blocking method, we ask *ForkJoinPool* to initiate the execution of this task. The 
*join()* method will return the result from that calculation, to which we add the square of the number we're currently 
visiting.

Now we just need a *ForkJoinPool* to handle the execution and thread management:
```
ForkJoinPool forkJoinPool = new ForkJoinPool();
 
FactorialSquareCalculator calculator = new FactorialSquareCalculator(10);
 
forkJoinPool.execute(calculator);
```