package com.bluesharkdev.upskill.java.concurrency.basic.overview.executorservice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceDemo {

    public static void main(String [] args) {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        executor.submit((Runnable) Task::new);

        executor.shutdown();
        executor.shutdownNow();
        try {
            executor.awaitTermination(20l, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
