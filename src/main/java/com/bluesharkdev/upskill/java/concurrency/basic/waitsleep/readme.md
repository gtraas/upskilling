# *wait()* vs *sleep()*

## General differences between *wait()* and *sleep()*

Simply put, *wait()* is an instance method that's used for thread synchronization.

It can be called by any object, since it is defined right on *java.lang.Object*, but it can only be called from a 
synchronized block. It releases the lock on the object so that another thread can jump in and acquire a lock.

On the other hand, *Thread.sleep()* is a static method that can be called from any context. *Thread.sleep()* pauses 
the current thread and does not release any locks.

An example:
```
private static Object LOCK = new Object();
 
private static void sleepWaitExamples() 
  throws InterruptedException {
  
    Thread.sleep(1000);
    System.out.println(
      "Thread '" + Thread.currentThread().getName() +
      "' is woken after sleeping for 1 second");
  
    synchronized (LOCK) {
        LOCK.wait(1000);
        System.out.println("Object '" + LOCK + "' is woken after" +
          " waiting for 1 second");
    }
}
```

Running this example will produce the following output:
```
Thread ‘main' is woken after sleeping for 1 second
Object ‘java.lang.Object@31befd9f' is woken after waiting for 1 second
```

## Waking up *wait()* and *sleep()*

When we use the *sleep()* method, a thread gets started after a specified time interval, unless it is interrupted.

For *wait()*, the waking up process is a bit more complicated. We can wake the thread by calling either *notify()* or 
*notifyAll()* methods on the monitor that is being waited on.

Use *notifyAll()* instead of *notify()* when you want to wake all threads that are in the waiting state. Similarly to 
the *wait()* method itself, *notify()* and *notifyAll()* have to be called from the synchronized context.

An example of how to wait:
```
synchronized (b) {
    while (b.sum == 0) {
        System.out.println("Waiting for ThreadB to complete...");
        b.wait();
    }
 
    System.out.println("ThreadB has completed. " + 
      "Sum from that thread is: " + b.sum);
}
```   
And then to notify
```
int sum;
  
@Override
public void run() {
    synchronized (this) {
        int i = 0;
        while (i < 100000) {
            sum += i;
            i++; 
        }
        notify(); 
    } 
}
```

Output from the above would be:
```
Waiting for ThreadB to complete…
ThreadB has completed. Sum from that thread is: 704982704
```
