package com.bluesharkdev.upskill.java.concurrency.basic.threadlocal;

public class ThreadLocalDemo {

    public void usingThreadLocal() {
        ThreadLocalWithUserContext firstUser = new ThreadLocalWithUserContext(1);
        ThreadLocalWithUserContext secondUser = new ThreadLocalWithUserContext(2);
        new Thread(firstUser).start();
        new Thread(secondUser).start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void usingMap() {
        SharedMapWithUserContext firstUser = new SharedMapWithUserContext(1);
        SharedMapWithUserContext secondUser = new SharedMapWithUserContext(2);
        new Thread(firstUser).start();
        new Thread(secondUser).start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Size of map should be 2, is: " + SharedMapWithUserContext.userContextPerUserId.size());
    }

    public static void main(String... args) {
        ThreadLocalDemo demo = new ThreadLocalDemo();
        demo.usingMap();
        demo.usingThreadLocal();

    }
}
