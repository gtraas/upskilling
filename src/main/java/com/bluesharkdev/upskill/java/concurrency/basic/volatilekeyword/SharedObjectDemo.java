package com.bluesharkdev.upskill.java.concurrency.basic.volatilekeyword;

public class SharedObjectDemo {

    public void test1() throws InterruptedException {
        SharedObject sharedObject = new SharedObject();

        Thread writer = new Thread(() -> sharedObject.incrementCount());
        writer.start();
        Thread.sleep(100);

        Thread readerOne = new Thread(() -> {
            int valueReadByThread2 = sharedObject.getCount();
            System.out.println("valueReadByThread2 should be 1, is: " + valueReadByThread2);
        });
        readerOne.start();

        Thread readerTwo = new Thread(() -> {
            int valueReadByThread3 = sharedObject.getCount();
            System.out.println("valueReadByThread3 should be 1, is: " + valueReadByThread3);
        });
        readerTwo.start();
    }

    public void test2() throws InterruptedException {
        SharedObject sharedObject = new SharedObject();
        Thread writerOne = new Thread(() -> sharedObject.incrementCount());
        writerOne.start();
        Thread.sleep(100);

        Thread writerTwo = new Thread(() -> sharedObject.incrementCount());
        writerTwo.start();
        Thread.sleep(100);

        Thread readerOne = new Thread(() -> {
            int valueReadByThread2 = sharedObject.getCount();
            System.out.println("valueReadByThread2 should be 2, is: " + valueReadByThread2);
        });
        readerOne.start();

        Thread readerTwo = new Thread(() -> {
            int valueReadByThread3 = sharedObject.getCount();
            System.out.println("valueReadByThread3 should be 2, is: " + valueReadByThread3);
        });
        readerTwo.start();
    }

    public static void main(String... main) {
        System.out.println("Starting test1");
        try {
            new SharedObjectDemo().test1();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Starting test2");
        try {
            new SharedObjectDemo().test2();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
