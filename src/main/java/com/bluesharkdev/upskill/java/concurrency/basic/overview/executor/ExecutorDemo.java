package com.bluesharkdev.upskill.java.concurrency.basic.overview.executor;

import java.util.concurrent.Executor;

public class ExecutorDemo {
    public static void main(String [] args) {
        Executor executor = new Invoker();
        executor.execute(()-> System.out.println("Running"));
    }
}
