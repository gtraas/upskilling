# The *volatile* keyword

## Overview
In Java, each thread has a seperate memory space known as working memory. This holds the values of different variables
used for performing operations. After performing an operation, the thread copies the updated value of the variable to
main memory, and from there, other threads can read the latest value.

Simply put, the *volatile* keyword marks a variable to go to main memory, for both reads and writes.

## When to use *volatile*
In situations where the next value of the variables is dependent on the previous value, there is a chance that multiple 
threads reading and writing the variable may go out of sync, due to a time gap between reading and writing back to main 
memory. 

Consider this example:
```
public class SharedObject {
    private volatile int count = 0;
    
    public void increamentCount() {      
        count++;
    }
    public int getCount() {
        return count;
    }
}
```

With no synchronization here, a typical race condition can occur. Basically, with the execution gap between incrementing 
and writing to main memory, other threads may see a value of 0, and try to write it to main memory. The race condition can 
also be avoided by using the Java-provided atomic data types, like *AtomicInt* and *AtomicLong*.

## *volatile* and thread synchronization

For all multi-threaded applications, we need to ensure a couple of rules for a consistent behaviour:
- Mutual exclusion: only one thread executes the critical section at a time
- Visibility: changes made by one thread to shared data, are visible to other threads, to maintain data consistency

*Synchronized* methods and blocks provide both of the above properties, at the cost of the performance of the application.

*Volatile* is a useful primitive, because it can help ensure the visibility aspect of the data change without providing
the mutual exclusion. It's useful in the places where we are ok with multiple threads executing a block of code in parallel
but we need to maintain the visibility aspect.

## *Happens-Before* Guarantee
Starting with Java 5, the *volatile* keyword also provides additional capabilities which ensure that values of all variables
, including non-volatile variables are written to main memory along with the *volatile* write operation. 

This is called *Happens-Before*, as it gives a visibility of all variables to another reading thread. Also, the JVM doesn't 
re-order the reading and writing instructions of *volatile* variables.

An example:
```
Thread 1
    object.aNonValitileVariable = 1;
    object.aVolatileVariable = 100; // volatile write
 
Thread 2:
    int aNonValitileVariable = object.aNonValitileVariable;
    int aVolatileVariable =  object.aVolatileVariable;
```   

In this case, when *Thread 1* has written the value of *aVolatileVariable*, the value of *aNonVolatileVariable* is also
written to the main memory. And even though it is not a volatile variable, it displays volatile behaviour.

By making use of these semantics, we can define only a few of the variables in our class as *volatile* and optimise 
the visibility guarantee.

 