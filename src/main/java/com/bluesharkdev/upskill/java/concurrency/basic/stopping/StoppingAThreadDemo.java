package com.bluesharkdev.upskill.java.concurrency.basic.stopping;

public class StoppingAThreadDemo {

    public void stopThreadAtomic() throws InterruptedException {
        int interval = 5;

        ControlSubThread controlSubThread = new ControlSubThread(interval);
        controlSubThread.start();

        // Give things a chance to get set up
        Thread.sleep(interval);
        System.out.println("Is the thread running?: " + controlSubThread.isRunning());
        System.out.println("Is the thread stopped?: " + controlSubThread.isStopped());

        // Stop it and make sure the flags have been reversed
        controlSubThread.stop();
        Thread.sleep(interval);
        System.out.println("Thread should be stopped: " + controlSubThread.isStopped());
    }

    public void interruptThread() throws InterruptedException {
        int interval = 50;

        ControlSubThread controlSubThread = new ControlSubThread(interval);
        controlSubThread.start();

        // Give things a chance to get set up
        Thread.sleep(interval);
        System.out.println("Thread is running?: " + controlSubThread.isRunning());
        System.out.println("Thread is stopped?: " + controlSubThread.isStopped());

        // Stop it and make sure the flags have been reversed
        controlSubThread.interrupt();

        Thread.sleep(interval);

        System.out.println("Thread should be stopped: " + controlSubThread.isStopped());
    }

    public static void main(String... args) {
        try {
            System.out.println("Stopping a thread");
            new StoppingAThreadDemo().stopThreadAtomic();
            System.out.println("Interrupting a thread");
            new StoppingAThreadDemo().interruptThread();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
