# A Guide to ThreadLocal

## The ThreadLocal API
The *ThreadLocal* API allows us to store data that will be accessible **only** by a specific thread.

If we want to have an Integer associated with a specific thread we would do this:
```
ThreadLocal<Integer> threadLocalValue = new ThreadLocal<>();
```

If we want to use this value from a thread, we simply call either *get()* or *set()*. We can think of it that ThreadLocal
stores data in a map - with the thread as the key.

Due to this, when we call *get()* on the *threadLocalValue*, we will get an Integer value for the requesting thread.

```
threadLocalValue.set(1);
Integer result = threadLocalValue.get();
```

To create a ThreadLocal, we can use the *withInitial()* static method and pass a supplier to it:
```
ThreadLocal<Integer> threadLocal = ThreadLocal.withInitial(() -> 1);
```

And to remove the value, we can use *remove()*.
```
threadLocal.remove();
```

## Implementation without ThreadLocal
Let's say we want to store *User* data in a *Context* object:
```
public class Context {
    private String userName;
 
    public Context(String userName) {
        this.userName = userName;
    }
}
```

We want to have 1 thread per user. Let's create a *SharedMapWithUserContext* class that implements a *Runnable* 
interface. The implementation in the *run()* method calls on the database through the *UserRepository* class that returns
a *Context* object for a given *userId*.

We're going to store this in a *ConcurrentHashMap* keyed by *userId*.
```
public class SharedMapWithUserContext implements Runnable {
  
    public static Map<Integer, Context> userContextPerUserId
      = new ConcurrentHashMap<>();
    private Integer userId;
    private UserRepository userRepository = new UserRepository();
 
    @Override
    public void run() {
        String userName = userRepository.getUserNameForUserId(userId);
        userContextPerUserId.put(userId, new Context(userName));
    }
 
    // standard constructor
}
```

We can test this by creating and starting 2 threads for two different userIds and then asserting whether we have 2 entries
in the *userContextPerUserId* map:
```
SharedMapWithUserContext firstUser = new SharedMapWithUserContext(1);
SharedMapWithUserContext secondUser = new SharedMapWithUserContext(2);
new Thread(firstUser).start();
new Thread(secondUser).start();
 
assertEquals(SharedMapWithUserContext.userContextPerUserId.size(), 2);
```

## Implementation with ThreadLocal
We can rewrite the above to store our *Context* in a *ThreadLocal*. Each thread will have its own *ThreadLocal* instance.

When using *ThreadLocal*, we need to be very careful because every *ThreadLocal* instance is associated with a particular 
thread. In our example, we have a dedicated thread for each particular *userId*, and this thread is created by us, so 
we have full control over it.

The *run()* method will fetch the user context and store it into the *ThreadLocal* variable using the *set()* method:
```
public class ThreadLocalWithUserContext implements Runnable {
  
    private static ThreadLocal<Context> userContext 
      = new ThreadLocal<>();
    private Integer userId;
    private UserRepository userRepository = new UserRepository();
 
    @Override
    public void run() {
        String userName = userRepository.getUserNameForUserId(userId);
        userContext.set(new Context(userName));
        System.out.println("thread context for given userId: "
          + userId + " is: " + userContext.get());
    }
     
    // standard constructor
}
```

We can test this by starting two threads that will execute the action for a given userId:
```
ThreadLocalWithUserContext firstUser = new ThreadLocalWithUserContext(1);
ThreadLocalWithUserContext secondUser = new ThreadLocalWithUserContext(2);
new Thread(firstUser).start();
new Thread(secondUser).start();
```

After running this code, we'll see on the stout that *ThreadLocal* was set per given thread.
```
thread context for given userId: 1 is: Context{userNameSecret='18a78f8e-24d2-4abf-91d6-79eaa198123f'}
thread context for given userId: 2 is: Context{userNameSecret='e19f6a0a-253e-423e-8b2b-bca1f471ae5c'}
```

## Do not use *ThreadLocal* with *ExecutorService*
If we want to use an *ExecutorService* and submit a *Runnable* to it, using *ThreadLocal* will yield non-deterministic
results - because we do not have a guarantee that every *Runnable* action for a given *userId* will be handled by the 
same thread every time it is executed.

ThreadLocal should **only** be used when we have full control over which thread will pick which runnable action to execute.

## A good practical example of what this is actually for
Say you have a Servlet that gets the logged in user and then executes some code:
```
doGet(HttpServletRequest req, HttpServletResponse resp) {
  User user = getLoggedInUser(req);
  doSomething()
  doSomethingElse()
  renderResponse(resp)
}
```

What happens if the doSomething() method needs access to the user object? We can't make *user* an instance or static 
variable because each thread will then use the same user object. We could pass the user around, but this gets messy:
```
doGet(HttpServletRequest req, HttpServletResponse resp) {
  User user = getLoggedInUser(req);
  doSomething(user)
  doSomethingElse(user)
  renderResponse(resp,user)
}
```

A more elegant solution is to put the user object into a ThreadLocal:
```
doGet(HttpServletRequest req, HttpServletResponse resp) {
  User user = getLoggedInUser(req);
  StaticClass.getThreadLocal().set(user)
  try {
    doSomething()
    doSomethingElse()
    renderResponse(resp)
  }
  finally {
    StaticClass.getThreadLocal().remove()
  }
}
```

Any code that needs the user object now just needs to get it from the ThreadLocal:
```
User user = StaticClass.getThreadLocal().get()
```

The code for the StaticClass:
```
class StaticClass {
  static private ThreadLocal threadLocal = new ThreadLocal<User>();
  static getThreadLocal() {
    return threadLocal;
  }
}
```