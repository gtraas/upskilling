package com.bluesharkdev.upskill.java.concurrency.basic.overview.future;

import java.util.concurrent.*;

public class FutureDemo {

    public static void main(String [] args) {
        String str = null;

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Future<String> future = executorService.submit(() -> {
            // Task
            Thread.sleep(10000l);
            return "Hellow world";
        });

        try {
//            future.get(5, TimeUnit.SECONDS); //Uncomment to cause TimeoutException
            future.get(20, TimeUnit.SECONDS); //Uncomment to avoid TimeoutException
        } catch (InterruptedException | ExecutionException | TimeoutException e1) {
            e1.printStackTrace();
        }

        if (future.isDone() && !future.isCancelled()) {
            try {
                str = future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        System.out.println(str);

        executorService.shutdown();

    }
}
