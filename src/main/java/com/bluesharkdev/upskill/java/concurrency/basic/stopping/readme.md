# How to kill a Java thread

## Introduction
Stopping a thread is no longer easy. The Thread.stop() method is now deprecated. Using is can lead to the corruption
of monitored objects

## Using a flag
Let's begin with a class that creates and starts a thread. This task won't end on its own, so we need to find some way 
to stop it:

We can use an atomic flag, such as:
```
public class ControlSubThread implements Runnable {
 
    private Thread worker;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private int interval;
 
    public ControlSubThread(int sleepInterval) {
        interval = sleepInterval;
    }
  
    public void start() {
        worker = new Thread(this);
        worker.start();
    }
  
    public void stop() {
        running.set(false);
    }
 
    public void run() { 
        running.set(true);
        while (running.get()) {
            try { 
                Thread.sleep(interval); 
            } catch (InterruptedException e){ 
                Thread.currentThread().interrupt();
                System.out.println(
                  "Thread was interrupted, Failed to complete operation");
            }
            // do something here 
         } 
    } 
}
```

Rather than having a *while* loop evaluating a constant *true*, we're using an *AtomicBoolean* and now we can start
or stop execution by setting it to true or false.

## Interrupting a Thread
What happens when *sleep()* is set to a long interval or if we're waiting for a *lock* that might never be released?

We face the risk of blocking for a long period or never terminating cleanly.

We can create the *interrupt()* for these situations, so let's add a few methods and a new flag to the class:
```
public class ControlSubThread implements Runnable {
 
    private Thread worker;
    private AtomicBoolean running = new AtomicBoolean(false);
    private int interval;
 
    // ...
 
    public void interrupt() {
        running.set(false);
        worker.interrupt();
    }
 
    boolean isRunning() {
        return running.get();
    }
 
    boolean isStopped() {
        return stopped.get();
    }
 
    public void run() {
        running.set(true);
        while (running.get()) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e){
                Thread.currentThread().interrupt();
                System.out.println(
                  "Thread was interrupted, Failed to complete operation");
            }
            // do something
        }
    }
}
```

We've added an interrupt method that sets our running flag to false and calls the worker thread's interrupt method.

If the thread is sleeping when this is called, sleep() will exit with an InterruptedException, as would any other 
blocking call.

This returns the thread to the loop, and it will exit since *running* is false. 