package com.bluesharkdev.upskill.java.concurrency.basic.overview.threadfactory;

public class Demo {

    public static void main(String... args) {
        ExampleThreadFactory factory = new ExampleThreadFactory("ExampleThreadFactory");
        for (int i = 0; i < 10; i++) {
            Thread t = factory.newThread(new Task(i));
            t.start();
        }
    }

}
