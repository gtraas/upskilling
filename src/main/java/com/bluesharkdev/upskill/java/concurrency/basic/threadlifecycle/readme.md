# The Thread lifecycle

## Multithreading in Java
In Java, multithreading is driven by the core concept of the thread. During their lifecycle, threads go through various
states:

![Thread lifecycle](../../../../../../../../resources/images/java/concurrency/threadlifecycle.jpg)

## Life cycle of a thread
The java.lang.Thread class contains a static State enum which defines its potential states:
- NEW: newly created thread that has not yet started execution
- RUNNABLE: either running or ready for execution but its waiting for resource allocation
- BLOCKED: waiting to acquire a monitor lock to enter or re-enter a synchronized block / method
- WAITING: waiting for some thread to perform a particular action with no time limit
- TIMED_WAITING: waiting for some other thread to perform a particular action for a specfied period
- TERMINATED: has completed its execution

### NEW
A NEW thread (or born thread) is a thread that's been created, but not yet started. It remains in this state until
we start it using the *start()* method.

### RUNNABLE
When we've created the thread and called the *start()* method, it moves from the NEW state to the RUNNABLE state. 
Threads in this state are either running or ready to run, but waiting for resource allocation from the system.

In a multi-threaded environment, the Thread-Scheduler (part of the JVM) allocates a fixed amount of time to each thread.
It will run for a certain amount of time and then will relinquish control to other RUNNABLE threads. 

### BLOCKED
A thread is in the blocked state when it is ineligible to run. It enters this state when it is waiting for a monitor lock
and is trying to access a section of code that is locked by some other thread.

An example:
```
public class BlockedState {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new DemoThreadB());
        Thread t2 = new Thread(new DemoThreadB());
         
        t1.start();
        t2.start();
         
        Thread.sleep(1000);
         
        Log.info(t2.getState());
        System.exit(0);
    }
}
 
class DemoThreadB implements Runnable {
    @Override
    public void run() {
        commonResource();
    }
     
    public static synchronized void commonResource() {
        while(true) {
            // Infinite loop to mimic heavy processing
            // 't1' won't leave this method
            // when 't2' try to enters this
        }
    }
}
```

In this code:
- we have created 2 seperate threads
- t1 starts and enters the synchronized *commonResource()* method. Only on thread can access this method at any given time.
- When t1 enters, we simulate heavy processing with an infinite loop. 
- When we start t2, it tries to enter *commonResource()*, which is being accessed by t1, so t2 will enter the BLOCKED
state

### WAITING
A thread in waiting is waiting on some other thread to perform a particular action. Any thread can enter this state by
calling one of these three methods:
- object.wait()
- thread.join()
- LockSupport.park()

Let's try to reproduce this:
```
public class WaitingState implements Runnable {
    public static Thread t1;
 
    public static void main(String[] args) {
        t1 = new Thread(new WaitingState());
        t1.start();
    }
 
    public void run() {
        Thread t2 = new Thread(new DemoThreadWS());
        t2.start();
 
        try {
            t2.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            Log.error("Thread interrupted", e);
        }
    }
}
 
class DemoThreadWS implements Runnable {
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            Log.error("Thread interrupted", e);
        }
         
        Log.info(WaitingState.t1.getState());
    }
}
```

So what are we doing here?
- We create and start t1
- t1 creates and starts t2
- While the processing of t2 continues, we call t2.join(). This puts t1 in the WAITING state until t2 completes execution.
- Since t1 is waiting on t2, we call t1.getState() from t2

### Timed Waiting
A thread is in TIMED_WAITING when it's waiting for another thread to perform a particular action within the given time frame.
There are 5 ways of putting the thread in TIMED_WAITING:
- thread.sleep(long millis)
- wait(int timeout) or wait(int timeout, int nanos)
- thread.join(long millis)
- LockSupport.parkNanos
- LockSupport.parkUntil

An example:
```
public class TimedWaitingState {
    public static void main(String[] args) throws InterruptedException {
        DemoThread obj1 = new DemoThread();
        Thread t1 = new Thread(obj1);
        t1.start();
         
        // The following sleep will give enough time for ThreadScheduler
        // to start processing of thread t1
        Thread.sleep(1000);
        Log.info(t1.getState());
    }
}
 
class DemoThread implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            Log.error("Thread interrupted", e);
        }
    }
}
```

### TERMINATED
This is the state of a dead thread. A thread enters this state when it finishes execution or is terminated abnormally.
