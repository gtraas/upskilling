# Create a file in Java
## With Java 6
- old school
```
    File newFile = new File("src/test/resources/newFile_jdk6.txt");
    boolean success = newFile.createNewFile();
```
- the file must not exist for this to work
- if the file does exist, *success* will be false

## With Java 7 and NIO2
```
    Path newFilePath = Paths.get("src/test/resources/newFile_jdk7.txt");
    Files.createFile(newFilePath);
``` 
- we're now using Path rather than File
- if the file exists, we'll get a *FileAlreadyExistsException*

## With Guava
- a one-liner
```
Files.touch(new File("src/test/resources/newFile_guava.txt"));
```

## With Commons IO
- creates a file, or a file with a full path
```
FileUtils.touch(new File("src/test/resources/newFile_commonsio.txt"));
```
- this does behave differently to the other ones, in that if the file exists, it simply doesn't do anything