# Java 13 Features
## ZGC

- introduced in Java 11 as a low latency garbage collector
- it was not equipped to return unused heap memory to the operating system
- Java 13 has added this capability
- we now get a reduced memory footprint, as well as a performance improvement
- ZGC now returns uncommitted memory to the OS by default up to the specified minimum heap space is reached
- it also now has a maximum supported heap space of 16TB, which is an increase from 4TB previously