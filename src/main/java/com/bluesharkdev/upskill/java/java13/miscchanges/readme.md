# Java 13 New Features
## Miscellaneous Changes

- **java.nio**: method *FileSystems.newFileSystem(Path, Map<String, ?>)* has been added
- **java.time**: new official Japanese era name has been added
- **javax.crypto**: support for MS Cryptography Next Generation (CNG) has been added
- **javax.security**: property *jdk.sasl.disabledMechanisms* has been added to disable SASL mechanisms
- **javax.xml.crypto**: new *String* constants were introduced to represent Canonical XML 1.1 URLs
- **javax.xml.parsers**: new methods were added to instantiate DOM and SAX factories with namespaces support
- Unicode support was upgraded to version 12.1
- Support was added for Kerberos principle name canonicalization and cross-realm referrals
- Additionally, a few APIs are proposed for removal. Three String methods, and the *javax.security.cert* API.
- Removals include the *rmic* tool and old features from the JavaDoc tool. 
- Pre-JDK 1.4 *SocketImpl* implementations are also no longer supported.
