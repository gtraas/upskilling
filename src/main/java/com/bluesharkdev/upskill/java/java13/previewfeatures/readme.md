# Java 13 New Features
## Preview Features

### switch Expressions

- a new *yield* expression has been added

```java
var me = 4;
var operation = "squareMe";
var result = switch (operation) {
    case "doubleMe" -> {
        yield me * 2;
    }
    case "squareME" -> {
        yield me * me;
    }
}
```

- it's now easy to implement the strategy pattern using the new switch *yield* expression.

### Text Blocks

- Java 13 introduced a preview feature for multi-line Strings such as embedded JSON, XML, etc
- Earlier, we would need to do the following to embed a JSON string as a String literal:

```
String JSON_STRING = "{\r\n" + "\"name\" : \"Baeldung\",\r\n" + "\"website\" : \"https://www.%s.com/\"\r\n" + "}";
```

- If we use the new Text Block, it becomes much easier to read:

```
String TEXT_BLOCK_JSON = """
{
    "name" : "Graham",
    "website" : "https://www.%s.com/"
}
""";
```

- there's no need to add escape characters or add carriage returns
- all String functions remain available for use on the text block
- *java.lang.String* also has 3 new methods to manipulate text blocks:

  - *stripIndent()* mimics the compiler to remove incidental white space
  - *translateEscapes()* translates escape sequences such as '\\t' and '\t'
  - *formatted()* works the same as *format()* but for text blocks
  - so for *formatted()*, the below two statements are the same 
```
TEXT_BLOCK_JSON.formatted("graham").contains("www.graham.com");
String.format(TEXT_BLOCK_JSON, "graham").contains("www.graham.com");
```

- because the above three String methods are preview features, they have been marked as deprecated, since they could be removed in a future release
