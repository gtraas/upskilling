# Java 15 Features
## Records

- a new type of class that makes it easy to create immutable data types
- they are still a preview feature at this point

### Without records
- prior to records, we'd create an immutable data type as follows:

```
public class Person {
    private final String name;
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
```

- this is a lot of code that essentially just holds a state
- all fields are explicitly labelled as final 
- there is a single, all-args constructor
- we have an accessor method for every field
- might even declare the class itself as final to prevent subclassing
- might also override the *toString()* method to provide meaningful logging
- also override the *hashCode()* and *equals()* methods to enable effective comparisons

### With records
- the same object as above can be declared as below using records:

```
public record Person(String name, int age) { }
```

- class definition has a new syntax that is specific to records
- in the header, we provide the details about the fields in the record
- compiler will produce an all-args constructor, accessor methods, a meaningful *toString()* method, and sensible implementations of the *hashCode()* and *equals()* methods
- they do allow for overriding some of the default behaviours:

``` 
public record Person(String name, int age) {
    public Person {
        if (age < 0) {
            throw new IllegalArgumentException("Age cannot be negative");
        }
    }
}
```

- records do have some restrictions:

  - they are always final
  - they cannot be declared abstract
  - they can't use *native* methods