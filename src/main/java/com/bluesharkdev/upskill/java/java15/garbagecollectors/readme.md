# Java 15 Features
## Garbage collectors

- both ZGC and Shenandoah are no longer experimental
- G1 collector is still the default, but users can opt to use either of the other two
- Shenandoah is not available on all vendor JDKs

  - notably, Oracle JDK does not include it