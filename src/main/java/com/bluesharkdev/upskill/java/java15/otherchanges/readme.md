# Java 15 Features
## Other changes

- text blocks are now fully supported in Java 15
- Helpful NPE are now fully supported by default
- legacy DatagramSocket API has been rewritten
- support for the Edwards-Curve Digital Signature algorithm is included
- deprecations to be removed in future releases

    - Biased locking
    - Solaris/SPARC ports
    - RMI Activation

- The Nashorn Javascript engine has been removed