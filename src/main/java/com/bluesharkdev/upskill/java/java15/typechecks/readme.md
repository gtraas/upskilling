# Java 15 Features
## Pattern matching type checks

- it was a preview feature in Java 14, and will continue as a preview in Java 15 with no enhancements
- the goal is to remove boilerplate code for the *instanceof* type check

```
if (person instanceof Employee) {
    Employee employee = (Employee) person;
    Date hireDate = employee.getHireDate();
}
```

- the pattern matching allows us introduce a new binding variable:

```
if (person instanceof Employee employee) {
    Date hireDate = employee.getHireDate();
}
```

- can also combine the pattern matching with conditional statements:

```
if (person instanceof Employee employee && employee.getYearsOfService() > 5) {
    //.....
}
```