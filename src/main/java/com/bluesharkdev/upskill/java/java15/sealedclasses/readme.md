# Java 15 Features
## Sealed Classes

- previously, Java provided no fine-grained control over inheritance
- *private*, *protected*, *public* and the default package-private provide very coarse control
- the goal of **sealed** classes is to allow individual classes to declare which type may be used as sub-types
- also applies to interfaces and determining which types can implement them
- sealed classes involve two new keywords: *sealed* and *permits*:

```
public abstract sealed class Person 
    permits Employee, Manager {
    
    //.....
    
}
```

- here the sealed class Person can only be extended by the Employee and Manager classes.
- any class that extends a sealed class must itself be declared *final*, *sealed* or *non-sealed*

```
public final class Employee extends Person {
}

public non-sealed class Manager extends Person {
}
```

- an example in action:
```
if (person instanceof Employee employee) {
    return employee.getEmployeeId();
} else if (person instanceof Manager manager) {
    return manager.getEmployeeId();
}
```

- without the sealed class, the compiler couldn't reasonably know whether all sub-classes are covered in this if statement
- without an else, the compiler would previously have complained about us not covering every case.