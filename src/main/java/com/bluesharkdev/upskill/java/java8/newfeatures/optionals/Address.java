package com.bluesharkdev.upskill.java.java8.newfeatures.optionals;

public class Address {

    private String street;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

}
