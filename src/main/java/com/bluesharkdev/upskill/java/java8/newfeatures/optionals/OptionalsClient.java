package com.bluesharkdev.upskill.java.java8.newfeatures.optionals;

import com.bluesharkdev.upskill.java.java8.newfeatures.methodreference.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OptionalsClient {

    public static void main(String [] args) {
        List<String> list = new ArrayList<>();
        list.add("One");
        list.add("OneAndOnly");
        list.add("Derek");
        list.add("Change");
        list.add("factory");
        list.add("justBefore");
        list.add("Italy");
        list.add("Italy");
        list.add("Thursday");
        list.add("");
        list.add("");

        System.out.println("Testing whether we get an empty Optional");
        Optional<String> optionalEmpty = Optional.empty();
        System.out.println("Is the optional empty?: " + (optionalEmpty.isPresent() ? "No" : "Yes"));

        String str = "value";
        System.out.println("Getting an optional of '" + str + "'");
        Optional<String> optional = Optional.of(str);
        System.out.println("Optional: " + optional.get());

        System.out.println("Getting Optional.ofNullable('" + str + "')");
        Optional<String> optionalNullable = Optional.ofNullable(str);
        System.out.println("Optional.ofNullable: " + optionalNullable.get());
        System.out.println("Getting Optional.ofNullable(null)");
        Optional<String> optionalNull = Optional.ofNullable(null);
        System.out.println("Optional.ofNullable(null): " + optionalNull.isPresent());

        System.out.println("Making sure we get the same list when we use 'Optional.of(list).orElse(new ArrayList<>())' ");
        List<String> listOpt = Optional.of(list).orElse(new ArrayList<>());
        System.out.println((listOpt == list) ? "Yes, we do" : "No, we don't");
        List<String> listNull = null;
        System.out.println("Making sure we get a null list when we use 'Optional.ofNullable(listNull).orElse(new ArrayList<>())'");
        List<String> listOptNull = Optional.ofNullable(listNull).orElse(new ArrayList<>());
        System.out.println((listOptNull.isEmpty()) ? "Yes" : "No");

        Optional<User> user = Optional.ofNullable(getUser());
        System.out.println("Making sure we get the address from a user");
        String result = user.map(User::getAddress).map(Address::getStreet).orElse("not specified");
        System.out.println("Address: " + result);

        Optional<OptionalUser> optionalUser = Optional.ofNullable(getOptionalUser());
        System.out.println("Making sure we get the address from a user with the stream.flatMap method");
        String resultOpt = optionalUser.flatMap(OptionalUser::getAddress).flatMap(OptionalAddress::getStreet).orElse("not specified");
        System.out.println("Address: " + resultOpt);

        Optional<User> userNull = Optional.ofNullable(getUserNull());
        System.out.println("Making sure we don't get a NPE when accessing a null user. Providing an option with the optional");
        String resultNull = userNull.map(User::getAddress).map(Address::getStreet).orElse("not specified");
        System.out.println("Address: " + resultNull);

        Optional<OptionalUser> optionalUserNull = Optional.ofNullable(getOptionalUserNull());
        System.out.println("Making sure we don't get a NPE when diving 2 levels deep");
        String resultOptNull = optionalUserNull.flatMap(OptionalUser::getAddress).flatMap(OptionalAddress::getStreet).orElse("not specified");
        System.out.println("Address: " + resultOptNull);
    }

    private static User getUser() {
        User user = new User();
        Address address = new Address();
        address.setStreet("1st Avenue");
        user.setAddress(address);
        return user;
    }

    private static OptionalUser getOptionalUser() {
        OptionalUser user = new OptionalUser();
        OptionalAddress address = new OptionalAddress();
        address.setStreet("1st Avenue");
        user.setAddress(address);
        return user;
    }

    private static OptionalUser getOptionalUserNull() {
        OptionalUser user = new OptionalUser();
        OptionalAddress address = new OptionalAddress();
        address.setStreet(null);
        user.setAddress(address);
        return user;
    }

    private static User getUserNull() {
        User user = new User();
        Address address = new Address();
        address.setStreet(null);
        user.setAddress(address);
        return user;
    }

}
