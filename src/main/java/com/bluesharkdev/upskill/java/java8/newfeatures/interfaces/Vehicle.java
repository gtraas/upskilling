package com.bluesharkdev.upskill.java.java8.newfeatures.interfaces;

public interface Vehicle {
    void moveTo(long altitude, long longitude);

    static String producer() {
        return "N&F Vehicles";
    }

    default long[] startPosition() {
        return new long[] { 23, 15 };
    }

    default String getOverview() {
        return "Default method invoked";
    }

    default String getEngineSize() {
        return "2.0L";
    }
}
