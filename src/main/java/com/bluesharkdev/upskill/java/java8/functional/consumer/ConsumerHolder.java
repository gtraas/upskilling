package com.bluesharkdev.upskill.java.java8.functional.consumer;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerHolder {

    static Consumer<String> printString = print -> System.out.println("Printing: " + print);

    static BiConsumer<String, String> concat = (string1, string2) -> System.out.println(string1.concat(string2));
}
