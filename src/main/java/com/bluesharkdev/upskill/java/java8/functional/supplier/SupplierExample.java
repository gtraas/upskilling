package com.bluesharkdev.upskill.java.java8.functional.supplier;

import java.util.function.Supplier;

public class SupplierExample {

    public static void main(String[] args) {

        System.out.println(getGreeting.get());

    }

    static Supplier<String> getGreeting = () -> "Hello";

}
