package com.bluesharkdev.upskill.java.java8.functional.combinator;

import java.time.LocalDate;

import static com.bluesharkdev.upskill.java.java8.functional.combinator.CustomerRegistrationValidator.*;

public class Main {

    public static void main(String[] args) {
        Customer customer = new Customer(
                "Alice",
                "alicegmail.com",
                "+02765344322749",
                LocalDate.of(2000, 1, 16)
        );

        //old way
        System.out.println(new CustomerValidatorService().isValid(customer));

        //combinator pattern
        ValidationResult result = isEmailValid().and(isPhoneNumberValid()).and(isAnAdult()).apply(customer);
        System.out.println(result);

        //if we do this
        CustomerRegistrationValidator crv = isEmailValid().and(isPhoneNumberValid().and(isAnAdult()));
        //it won't actually run anything until we call the apply function
        System.out.println("did we print to the log: NO");
        System.out.println(crv.apply(customer));
        System.out.println("We did now");
    }

}
