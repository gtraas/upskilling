package com.bluesharkdev.upskill.java.java8.newfeatures.interfaces;

public class VehicleImpl implements Vehicle {
    @Override
    public void moveTo(long altitude, long longitude) {
        //do nothing
    }

    @Override
    public String getEngineSize() {
        return "1.4L";
    }
}
