package com.bluesharkdev.upskill.java.java8.functional.function;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionExample {

    public static void main(String... args) {

        System.out.println("String length of 'hello' (should be 5) is: " + findLength.apply("hello"));

        System.out.println("String length should be 2, is: " + substring.andThen(findLength).apply("hello"));

        System.out.println("Does 'hello' contain 'll': " + doesStringContain.apply("hello", "ll"));

        Map<String, Function> functionMap = new HashMap<>();

        functionMap.put("findLength", findLength);

        functionMap.put("substring", substring);

        System.out.println("Using the map: " + functionMap.get("findLength").apply("Hello"));


    }

    static Function<String, Integer> findLength = string -> string.length();

    static Function<String, String> substring = string -> string.length() < 2 ? "" : string.substring(0,2);

    static BiFunction<String, String, Boolean> doesStringContain = (string, content) -> string.contains(content);

}
