package com.bluesharkdev.upskill.java.java8.functional.streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.bluesharkdev.upskill.java.java8.functional.streams.Streams.Gender.*;

public class Streams {

    public static void main(String[] args) {
        List<Person> people = Stream.of(
                new Person("John", 33, MALE),
                new Person("Jack", 20, MALE),
                new Person("Josie", 19, FEMALE),
                new Person("Mary", 41, FEMALE)
        ).collect(Collectors.toList());

        //print out people's ages
        people.stream()
                .mapToInt(Person::getAge)
                .forEach(System.out::println);

        //print out men's names
        people.stream()
                .filter(person -> MALE.equals(person.gender))
                .map(Person::getName)
                .forEach(System.out::println);
    }

    static class Person {
        private final String name;
        private final int age;
        private final Gender gender;

        Person(String name, int age, Gender gender) {
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        public String toString() {
            return name + ", " + gender;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }

    enum Gender {
        MALE, FEMALE
    }
}
