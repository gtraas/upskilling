package com.bluesharkdev.upskill.java.java8.functional.predicate;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateExample {

    public static void main(String[] args) {
        System.out.println("Is length greater than 2 for 'hello'? " + lengthIsGreaterThan2.test("hello"));
        System.out.println("is 'hello' equal to 'Hello'? " + isEqual.test("hello", "Hello"));
    }

    static Predicate<String> lengthIsGreaterThan2 = string -> string.length() > 2;

    static BiPredicate<String, String> isEqual = (string1, string2) -> string1.equals(string2);
}
