package com.bluesharkdev.upskill.java.java8.newfeatures.interfaces;

public class NewFeaturesInterfacesClient {
    public static void main(String [] args) {
        System.out.println("Accessing a static implemented method on an interface: ");
        System.out.println("Vehicle.producer(): " + Vehicle.producer());

        System.out.println();
        Vehicle v = new VehicleImpl();
        System.out.println("Testing the default method");
        System.out.println("Invoking an overridden default method... should print '14.L'");
        System.out.println(v.getEngineSize());
        System.out.println("Invoking a default method via an implementing class (not overridden).... should print 'Default method invoked'");
        System.out.println(v.getOverview());
    }
}
