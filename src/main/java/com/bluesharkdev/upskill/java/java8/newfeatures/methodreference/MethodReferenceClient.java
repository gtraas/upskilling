package com.bluesharkdev.upskill.java.java8.newfeatures.methodreference;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodReferenceClient {

    public static void main(String [] args) {
        List<String> list = new ArrayList<>();
        list.add("One");
        list.add("OneAndOnly");
        list.add("Derek");
        list.add("Change");
        list.add("factory");
        list.add("justBefore");
        list.add("Italy");
        list.add("Italy");
        list.add("Thursday");
        list.add("");
        list.add("");

        System.out.println("Testing the reference to a static method");
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        boolean isReal = users.stream().anyMatch(u -> User.isRealUser(u));
        boolean isRealRef = users.stream().anyMatch(User::isRealUser);
        System.out.println("Using a lambda: " + isReal);
        System.out.println("Using a method reference to a static method: " + isRealRef);

        System.out.println();
        System.out.println("Testing the reference to an instance method");
        User user = new User();
        boolean isLegalName = list.stream().anyMatch(user::isLegalName);
        System.out.println("Should be true: is .... " + isLegalName);

        System.out.println();
        System.out.println("Testing the reference to an instance method of an object of a particular type");
        long count = list.stream().filter(String::isEmpty).count();
        System.out.println("Count of empty strings in the list (should be 2): " + count);

        System.out.println();
        System.out.println("Testing the 'new' method as a method reference to create a new Instance");
        Stream<User> stream = list.stream().map(User::new);
        List<User> userList = stream.collect(Collectors.toList());
        System.out.println("User list should be the same size as list. List: " + list.size() + ". UserList: " + userList.size());
        System.out.println("Are the objects in the UserList really users? " + (userList.get(0) instanceof User ? "Yes" : "No"));

    }

}
