## Interface Default and Static methods

Prior to Java 8, interfaces could only have public abstract methods. Adding new functionality to the interface meant altering code in all the implementing classes. It also wasn't possible to define an interface with an implementing method. 
With Java 8, interfaces can now have **static** and **default** methods that, despite being declared in an interface, have a defined behaviour.

### Static method

Let's consider the static method *producer()* in the Vehicle interface. This method is only available through and inside the interface. Overrriding by an implementing class is not permitted.
We can use this method like any other static method on any other class, i.e. *Vehicle.producer()*


### Default methods
These are declared using the *default* keyword. These are accessible through implementing classes and can be overridden. If an implementing class does not override the default method, the default method itself is invoked.

## Method references
### Reference to a static method
The reference to a static method holds the following syntax: *ContainingClass::methodName*.

Let's try to count all empty strings in the List<String> with help of Stream API.

*boolean isReal = list.stream().anyMatch(u -> User.isRealUser(u));*
Take a closer look at lambda expression in the anyMatch() method, it just makes a call to a static method isRealUser(User user) of the User class. So it can be substituted with a reference to a static method:

*boolean isReal = list.stream().anyMatch(User::isRealUser);*
This type of code looks much more informative.

### Reference to an Instance Method
The reference to an instance method holds the following syntax: *containingInstance::methodName*. The following code calls the method isLegalName(String string) on type User which validates an input parameter:

*User user = new User();*\
*boolean isLegalName = list.stream().anyMatch(user::isLegalName);*

### Reference to an Instance Method of an Object of a Particular Type

This reference method takes the following syntax: *ContainingType::methodName*.

*long count = list.stream().filter(String::isEmpty).count();*

### Reference to a Constructor
    
A reference to a constructor takes the following syntax: *ClassName::new*. As a constructor in Java is a special method, the method reference could be applied to it too with the help of new as a method name.
    
*Stream< User > stream = list.stream().map(User::new);*