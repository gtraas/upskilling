package com.bluesharkdev.upskill.java.java8.functional.consumer;

import static com.bluesharkdev.upskill.java.java8.functional.consumer.ConsumerHolder.*;

public class ConsumerUser {

    public static void main(String[] args) {
        printString.accept("Print");

        concat.accept("Hello, ", "world");
    }
}
