package com.bluesharkdev.upskill.java.java8.functional.callbacks;

import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
        hello("John", null, nolastname);
        hello2("John", null, () -> System.out.println("No last name provided"));
    }

    static void hello(String firstName, String lastName, Consumer<String> callback) {
        System.out.println(firstName);
        if (lastName != null) {
            System.out.println(lastName);
        } else {
            callback.accept(firstName);
        }
    }

    static void hello2(String firstName, String lastName, Runnable runnable) {
        System.out.println(firstName);
        if (lastName != null) {
            System.out.println(lastName);
        } else {
            runnable.run();
        }
    }

    public static Consumer<String> nolastname = value -> System.out.println("No last name provided for " + value);
}
