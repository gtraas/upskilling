# ArrayList
## Introduction
*List* represents an ordered sequence of values where some value may occur more than one time.

*ArrayList* is one of the *List* implementations built atop an array, which is able to dynamically grow and shrink as 
you add and remove elements. Elements can be easily accessed by their index.
*ArrayList* has the following properties:
- random access is O(1)
- Adding an element is O(1)
- Inserting and deleting takes O(n)
- Searching takes O(n) for an unsorted list, and O(log n) for a sorted list

## Creating the ArrayList
*ArrayList* has several constructors. It is a generic, so we can parameterise it with any type we want, and the compiler 
will ensure that only that type goes into the list. We also won't need to cast elements when retrieving them.

It is good practice to use the interface *List* as a variable type, because it decouples it from a particular implementation

### Default no-arg constructor
```
List<String> list = new ArrayList<>();
```
This creates an empty *ArrayList* instance.

### Constructor accepting initial capacity
```
List<String> list = new ArrayList<>(20);
```
This specifies the initial length of the underlying array. This may help avoid unnecessary resizing while adding new 
items.

### Constructor accepting a Collection
```
Collection<Integer> numbers = IntStream.range(0, 10).boxed().collect(toSet());
 
List<Integer> list = new ArrayList<>(numbers);
```

## Adding elements to the ArrayList
You can insert elements either at the end or at a specific position.

```
List<Long> list = new ArrayList<>();
 
list.add(1L);
list.add(2L);
list.add(1, 3L);
 
assertThat(Arrays.asList(1L, 3L, 2L), equalTo(list));
```

You can also insert a collection or several elements at once:
```
List<Long> list = new ArrayList<>(Arrays.asList(1L, 2L, 3L));
LongStream.range(4, 10).boxed()
  .collect(collectingAndThen(toCollection(ArrayList::new), ys -> list.addAll(0, ys)));
assertThat(Arrays.asList(4L, 5L, 6L, 7L, 8L, 9L, 1L, 2L, 3L), equalTo(list));
```

## Iterating over the ArrayList
There are two types of iterators available: 
- *Iterator*
- *ListIterator*

The difference is that *Iterator* traverses the list in only one direction. *ListIterator* can traverse the list both 
ways.

The *ListIterator*:
```
List<Integer> list = new ArrayList<>(IntStream.range(0, 10).boxed().collect(toCollection(ArrayList::new)));
ListIterator<Integer> it = list.listIterator(list.size());
List<Integer> result = new ArrayList<>(list.size());
while (it.hasPrevious()) {
    result.add(it.previous());
}
 
Collections.reverse(list);
assertThat(result, equalTo(list));
```

## Searching the ArrayList
Let's set up an example list:
```
List<String> list = LongStream.range(0, 16)
  .boxed()
  .map(Long::toHexString)
  .collect(toCollection(ArrayList::new));
List<String> stringsToSearch = new ArrayList<>(list);
stringsToSearch.addAll(list);
```

### Searching an unsorted list
In order to find an element, you can use *indexOf()* or *lastIndexOf()*. They both accept an object and return an *int*
value:
```
assertEquals(10, stringsToSearch.indexOf("a"));
assertEquals(26, stringsToSearch.lastIndexOf("a"));
``` 

If you want to find all elements satisfying a predicate, we can filter the list using the Streams API using *Predicate*:
```
Set<String> matchingStrings = new HashSet<>(Arrays.asList("a", "c", "9"));
 
List<String> result = stringsToSearch
  .stream()
  .filter(matchingStrings::contains)
  .collect(toCollection(ArrayList::new));
 
assertEquals(6, result.size());
```

We could also use a for loop, or an Iterator
```
Iterator<String> it = stringsToSearch.iterator();
Set<String> matchingStrings = new HashSet<>(Arrays.asList("a", "c", "9"));
 
List<String> result = new ArrayList<>();
while (it.hasNext()) {
    String s = it.next();
    if (matchingStrings.contains(s)) {
        result.add(s);
    }
}
```

### Searching a sorted list
If we have a sorted array, we can use a binary search algorithm which works faster than a linear search:
```
List<String> copy = new ArrayList<>(stringsToSearch);
Collections.sort(copy);
int index = Collections.binarySearch(copy, "f");
assertThat(index, not(equalTo(-1)));
```
If an element if not found, -1 is returned.

## Removing elements from an ArrayList
In order to remove an element, we should first find its index, and then remove it using the *remove()* method.
An overloaded version of this method accepts an object, searches for it and removes the first occurence of an equal
element:
```
List<Integer> list = new ArrayList<>(IntStream.range(0, 10).boxed().collect(toCollection(ArrayList::new)));
Collections.reverse(list);
 
list.remove(0);
assertThat(list.get(0), equalTo(8));
 
list.remove(Integer.valueOf(0));
assertFalse(list.contains(0));
```
The above example is a good example of why we need to be careful using boxed types (such as Integers) in Lists. In
order to remove a particular element, you should first box the *int* value, otherwise an element will be removed by 
its index.

Removing several elements is easiest done using the Streams API, but it can also be done using an Iterator:
```
Set<String> matchingStrings
 = HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "f"));
 
Iterator<String> it = stringsToSearch.iterator();
while (it.hasNext()) {
    if (matchingStrings.contains(it.next())) {
        it.remove();
    }
}
```