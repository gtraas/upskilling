# TreeSet
## Introduction
The *TreeSet* is a sorted collection that extends the *AbstractSet* class and implements the *NavigableSet* interface.
A quick summary of the most important aspects of the implementation:
- It stores unique elements
- It doesn't preserve the insertion order of the elements
- It sorts the elements in ascending order
- It is **not** thread-safe

In this implementation, objects are sorted and stored in ascending order according to their natural order. The *TreeSet* 
uses a self-balancing binary search tree, more specifically a *Red-Black tree*.

Being a self-balancing binary search tree, each node of the binary tree comprises an extra bit, which is used to identify
the color of the node which is either red or black. During subsequent insertions and deletions, these "color" bits helps
in ensuring that the tree remains more or less balanaced.

Creating an instance:
```
Set<String> treeSet = new TreeSet<>();
```

### Constructing a treeset with a constructor Comparator param
We can construct a TreeSet with a constructor that lets us define the order in which the elements get sorted by using a 
Comparable or Comparator:
```
Set<String> treeSet = new TreeSet<>(Comparator.comparing(String::length));
```

Although TreeSet isn't thread-safe, it can be synchronized externally using the Collections.synchronizedSet() wrapper:
```
Set<String> syncTreeSet = Collections.synchronizedSet(treeSet);
```

## TreeSet add()
As expected, this method adds an element to the treeset. If an element is added, it returns true, otherwise false.

The contract of the method states that an element will be added only if the same element isn't already present in the 
Set.

To add an element to the TreeSet:
```
boolean successful = treeSet.add("String added");
```

The add method is extremely important as the implementation details of the method illustrate how the TreeSet works
internally, how it leverages the TreeMap's put method to store the elements:
```
public boolean add(E e) {
    return m.put(e, PRESENT) == null;
}
```
The variable *m* refers to an internal backing *TreeMap*:
```
private transient NavigableMap<E, Object> m;
```

Therefore, the *TreeSet* internally depends on a backing *NavigableMap* which gets initialised with an instance of 
*TreeMap* when an instance of *TreeSet* is created:
```
public TreeSet() {
    this(new TreeMap<E,Object>());
}
```

## TreeSet contains()
This is used to check whether a given element is present in the TreeSet. If the element is found, it returns true, otherwise
false:
```
assertTrue(treeSetContains.contains("String Added"));
```

### TreeSet remove()
Used to remove a specific element if it is present in the TreeSet. Returns true if the element was removed.
```
removeFromTreeSet.remove("String Added");
```

### TreeSet clear()
Used to remove **all** elements from a treeset:
```
clearTreeSet.clear();
```

### TreeSet size()
Used to identify the number of elements in the TreeSet.
```
int size = treeSetSize.size();
```

### TreeSet isEmpty()
Used to determine whether a treeset is empty or not.
```
emptyTreeSet.isEmpty()
```

### Iterating a TreeSet
The *iterator()* method returns an iterator that iterates in ascending order. It is fail-fast.
```
Set<String> treeSet = new TreeSet<>();
    treeSet.add("First");
    treeSet.add("Second");
    treeSet.add("Third");
    Iterator<String> itr = treeSet.iterator();
    while (itr.hasNext()) {
        System.out.println(itr.next());
    }
```

We can also iterate in descending order.
```
TreeSet<String> treeSet = new TreeSet<>();
    treeSet.add("First");
    treeSet.add("Second");
    treeSet.add("Third");
    Iterator<String> itr = treeSet.descendingIterator();
    while (itr.hasNext()) {
        System.out.println(itr.next());
    }
```

The *iterator* throws a *ConcurrentModificationException* if the set is modified at any time after the iterator is 
created in any way except through the iterator's *remove()* method.

There is no guarantee on fail-fast behaviour of an iterator as it is impossible to make any hard guarantees in the 
presence of unsynchronized concurrent modification.

### TreeSet first()
This method return the first element in a TreeSet, or throws a *NoSuchElementException*.

### TreeSet last()
Returns the last element, or throws a *NoSuchElementException*.

### TreeSet subSet()
This will return the elements ranging from *fromElement* to *toElement*. *fromElement* is inclusive, while *toElement* is
exclusive:
```
SortedSet<Integer> treeSet = new TreeSet<>();
treeSet.add(1);
treeSet.add(2);
treeSet.add(3);
treeSet.add(4);
treeSet.add(5);
treeSet.add(6);
     
Set<Integer> expectedSet = new TreeSet<>();
expectedSet.add(2);
expectedSet.add(3);
expectedSet.add(4);
expectedSet.add(5);
 
Set<Integer> subSet = treeSet.subSet(2, 6);
  
assertEquals(expectedSet, subSet);
```

### TreeSet headSet()
This method returns elements that are smaller than the specified element:
```
SortedSet<Integer> treeSet = new TreeSet<>();
treeSet.add(1);
treeSet.add(2);
treeSet.add(3);
treeSet.add(4);
treeSet.add(5);
treeSet.add(6);
 
Set<Integer> subSet = treeSet.headSet(6);
  
assertEquals(subSet, treeSet.subSet(1, 6));
```

### TreeSet tailSet()
This method returns the elements of the TreeSet that are greater than or equal to the given element:
```
NavigableSet<Integer> treeSet = new TreeSet<>();
treeSet.add(1);
treeSet.add(2);
treeSet.add(3);
treeSet.add(4);
treeSet.add(5);
treeSet.add(6);
 
Set<Integer> subSet = treeSet.tailSet(3);
  
assertEquals(subSet, treeSet.subSet(3, true, 6, true));
```

### Storing *null* elements
TreeSet no longer supports the storing of *null* values. In Java 7, this was possible.

Adding *null* will result in a *NullPointerException* because of the need to compare it to other elements.

### Performance of the TreeSet
When compared to a HashSet, the performance of a TreeSet is on the lower side. Operations like *add*, *remove* and *search*
take O(log n) while operations like *printing n* elements in sorted order requires O(n) time.

A TreeSet should be our primary choice if we want to keep our entries sorted as a TreeSet may be accessed and traversed
in either ascending or descending order. 

If we're short on memory and we want to access elements that are relatively close to each other in natural order, we should
use the TreeSet. In cases where data needs to be read from the hard drive (which has greater latency than a cache or 
memory), we should use TreeSet.  