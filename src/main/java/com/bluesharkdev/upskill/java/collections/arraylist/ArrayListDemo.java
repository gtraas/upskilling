package com.bluesharkdev.upskill.java.collections.arraylist;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static java.util.stream.Collectors.toCollection;

public class ArrayListDemo {
    public static void main(String... args) {
        List<String> list = LongStream.range(0, 16)
                .boxed()
                .map(Long::toHexString)
                .collect(toCollection(ArrayList::new));
        List<String> stringsToSearch = new ArrayList<>(list);
        stringsToSearch.addAll(list);

        System.out.println("Searching an unsorted list");

        System.out.println("Should be 10, is: " + stringsToSearch.indexOf("a"));
        System.out.println("Should be 26, is: " + stringsToSearch.lastIndexOf("a"));

        Set<String> matchingStrings = new HashSet<>(Arrays.asList("a", "c", "9"));

        List<String> result = stringsToSearch
                .stream()
                .filter(matchingStrings::contains)
                .collect(toCollection(ArrayList::new));

        System.out.println("Should be 6, is: " + result.size());

        Iterator<String> it = stringsToSearch.iterator();

        result = new ArrayList<>();
        while (it.hasNext()) {
            String s = it.next();
            if (matchingStrings.contains(s)) {
                result.add(s);
            }
        }

        System.out.println("Should be 6, is: " + result.size());

        System.out.println("Searching a sorted list");
        List<String> copy = new ArrayList<>(stringsToSearch);
        Collections.sort(copy);
        int index = Collections.binarySearch(copy, "f");
        System.out.println("Index should not be -1: " + (index == -1 ? "Is -1" : "Is not -1"));

        System.out.println("Removing elements from the list");

        List<Integer> removingList = new ArrayList<>(IntStream.range(0, 10).boxed().collect(toCollection(ArrayList::new)));
        Collections.reverse(removingList);

        removingList.remove(0);
        System.out.println("Element 0 should be 8, is: " + removingList.get(0));

        removingList.remove(Integer.valueOf(0));
        System.out.println("Element should be removed: " + (removingList.contains(0) ? "Present" : "Not present"));


    }
}
