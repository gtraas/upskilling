# LinkedList

## Introduction
LinkedList is a doubly-linked list implementaion of the *List* and *Deque* interfaces. It implements all optional
list operations and permits all elements, including null.

## Features
- Operations that index into the list will traverse the list from the beginning or the end, whichever is closer to the 
specified index
- It is **not** synchronized
- Its *Iterator* and *ListIterator* iterators are **fail-fast** (which means that after creation of the iterator, if the 
list is modified, a *ConcurrentModificationException* is thrown).
- Every element is a node, which keeps a reference to the next and previous nodes.
- It maintains insertion order.

Although the *LinkedList* is not synchronized, we can retrieve a synchronized version of it using 
*Collections.synchronizedList(yourLinkedList)*.

## Comparison to ArrayList
### Structure
An *ArrayList* is an index-based data structure backed by an *Array*. It provides random access to its elements with a
performance equal to O(1).

A *LinkedList* stores its data as a list of elements and every element is linked to its previous and next element. In 
this case, the search algorithm for an item has an execution time equal to O(n).

### Operations
Insertion, addition and removal of items are considerably faster in a *LinkedList* because there is no need to resize
an array or update the index when an element is added to some arbitrary position inside the collection, only the 
references to the surrounding elements.

### Memory Usage
*LinkedList* consumes more memory than an *ArrayList* because every node in a *LinkedList* stores 2 references, one for 
the previous element and one for the next element. An *ArrayList* only holds the data and its index.

## Usage
### Creation
```
LinkedList<Object> linkedList = new LinkedList<>();
``` 

### Adding elements
*LinkedList* implements the *List* and *Deque* interfaces, so besides the standard *add()* and *addAll()* methods, you
can find *addFirst()* and *addLast()*, which adds an element to the beginning or end of the list, respectively.

### Removing an element
Similarly to element addition, this list implementation offers *removeFirst()* and *removeLast()*.

There is also a convenience method *removeFirstOccurence()* and *removeLastOccurence()*, which returns boolean (true if
the collection contained the specified element).

### Queue operations
The *Deque* interface provides queue-like behaviour:
```
linkedList.poll();
linkedList.pop();
```
These methods will retrieve the first element, and remove it from the list.

The difference between *poll()* and *pop()* is the *pop()* will throw *NoSuchElementException* on an empty list, while 
*poll()* will return null. The APIs *pollFirst()* and *pollLast()* are also available.

There is also a *push()* method:
```
linkedList.push(Object o);
```
This inserts an element at the head of the collection.
