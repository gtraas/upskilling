# HashMap
## Basic Usage

- key-value mapping
- every key is mapped to exactly 1 value
- use the key to retrieve the value
- performance is better than a list

    - with a list, finding a specific element can be O(n) for an unsorted list, O(log n) for a sorted list

- time complexity for a HashMap to insert and retrieve an element is O(1)

### Setup

- this is our class we'll use throughout the demo

```java
public class Product {
    private String name;
    private String description;
    private List<String> tags;
    
    //getters / setters / constructors
    
    public Product addTagsOfTheProducts(Product product) {
        this.tags.addAll(product.getTags());
        return this;
    }
}
```

### Put

- let's create the HashMap

```
Map<String, Product> productsByName = new HashMap<>();
```

- and add something:

```
Product eBike = new Product("E-Bike", "A bike with a battery");
Product roadBike = new Product("Road Bike", "A bike for competition");
productsByName.put(eBike.getName(), eBike);
productsByName.put(roadBike.getName(), roadBike);
```

### Get

- can retrieve the value by using the key:

```
Product nextPurchase = productsByName.get("E-Bike");
```

- if we try to retrieve a value for which there is no key, we receive a *null*

```
Product nextPurchase = productsByName.get("Car");
assertNull(nextPurchase);
```

- if we insert a second value with the same key, we will get the latest value

```
Product newEBike = new Product("E-Bike", "This one has a better battery");
productsByName.put(newEBike.getName(). newEBike);
assertEquals("This one has a better battery", productsByName.get("E-Bike").getDescription());
```

### *null* as the key

- HashMap allows us to have a *null* as the key

```
Product defaultProduct = new Product("Chocolate", "At least buy a chocolate");
productsByName.put(null, defaultProduct);

Product nextPurchase = productsByName.get(null);
assertEquals("At least buy a chocolate", productsByName.get(null).getDescription());
```

### Remove a value

- we can remove a key-value mapping by:

```
productsByName.remove("E-Bike");
assertNull(productsByName.get("E-Bike"));
```

### Check if a Key or Value exists in a Map

- to check for a key, we use *containsKey*:

```
productsByName.containsKey("E-Bike");
```

- to check for a value, we use *containsValue*:

```
productsByName.containsValue(eBike);
```

- both methods will return true in this case
- complexity to check for a key is O(1)
- complexity to check for a value is O(n)
   
  - we'll need to iterate over all elements in the map to check for the value

### Iterating over a HashMap

- three ways to iterate over a HashMap
- can iterate over the keySet:

```
for(String key: productsByName.keySet()) {
    Product product = productsByName.get(key);
}
```

- can iterate over the set of all entries:

```
for (Map.Entry<String, Product> entry: productsByName.entrySet()) {
    Product product = entry.getValue();
    String key = entry.getKey();
}
```

- can iterate over the values:

```
List<Product> products = new ArrayList<>(productsByName.values());
```

## The key

- can use any class as the key for the HashMap
- the implementation of *hashcode* and *equals* needs to be properly implemented for this to work
- implementing the hashcode and equals method:

```
@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Product p = (Product) o;
        return Objects.equals(name, p.getName()) && Objects.equals(description, p.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }
```

- only the key object needs to have the hashCode and equals method overridden 

## Additional methods as of Java 8
### forEach

- provides a functional way to iterate over a HashMap

```
productsByName.forEach( (key, product) -> {
    System.out.println("Key: " + key + ", Product: " + product.getDescription());
});
```

### getOrDefault

- returns a default element in case there is no mapping for the given key

```
Product chocolate = new Product("chocolate", "something sweet");
Product defaultProduct = productsByName.getOrDefault("horse carriage", chocolate);
Product eBike = productsByName.getOrDefault("E-Bike", chocolate);
```

### putIfAbsent

- with this method, we can add a new mapping, but only if the key doesn't yet exist
- will not override the value in the hashmap if the key is already there

```
productsByName.putIfAbsent("E-Bike", chocolate);
```

### merge

- we can modify the value for a given key if a mapping exists, or a new value otherwise:

```
Product eBike2 = new Product("E-Bike", "A bike with a battery");
eBike2.getTags().add("Sport");
productsByName.merge("E-Bike", eBike2, Product::addTagsOfOtherProducts();
```

### compute

- can compute the value for a given key

```
productsByName.compute("E-Bike", (k, v) -> {
    if (v != null) {
        return v.addTagsOfOtherProducts(eBike2);
    } else {
        return eBike2;
    }
});
```

- old school equivalent:

```
if (productsByName.containsKey("E-Bike") {
    productsByName.get("E-Bike").addTagsOfOtherProduct(eBike2);
} else {
    productsByName.put("E-Bike", eBike2);
}
```

- *compute* and *merge* are very similar
- the *compute* method accepts two arguments: the key and a BiFunction for the remapping
- the *merge* method accepts three arguments: the key, a default value to add if the key doesn't exist and a BiFunction for the remapping

## HashMap internals
### Hashcode and Equals

- HashMap attempts to calculate the position of a value based on the key
- HashMap stores elements in "buckets" based on the hashcode of the key
- to retrieve a value, it calculates which bucket the value is in by looking at the key's hashcode
- it then iterates through the objects found in that bucket and uses the equals method to find the exact match
- if keys have the same hashcode, their corresponding values are stored in a linkedlist
- so if the hashcode method is not implemented correctly, the get and put methods go from O(1) to O(n)

### Key immutability

- in most cases, we should use immutable keys
- if our key changes after we used it to store a value in a HashMap, the key's bucket will change
- this is because the hashcode will change, and then HashMap is looking in the wrong hash bucket
- most likely, the hashmap will return null, since that key's value won't be in the new bucket

### Collisions

- equal keys MUST have the same hashcode
- different keys CAN have the same hashcode
- if two different keys have the same hash, the two values belonging to them will be stored in the same bucket
- in this same bucket, values are stored in a list and retrieved by looping over all elements
- in Java 8, the data structure in which values in the same bucket are stored was changed from a list to a balanced tree if there are 8 or more elements in the same bucket
- it is changed back to a list when there are 6 or less elements in the bucket
- this improves the performance from O(n) to O(log n)

### Capacity and Load Factor

- to avoid having too many buckets with multiple values, the capacity is doubled if 75% of the buckets become non-empty
- default value for load factor is 75%
- default value for initial capacity is 16
- both can be changed in the constructor