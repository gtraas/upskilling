# The SortedSet interface
## Overview
- maintains its elements in ascending order, according to the natural order, or a Comparator passed to it at creation time
- also provides methods for the following:
    - range view: allows range operations on the sorted set
    - endpoints: returns the first or last element in the sorted set
    - comparator access: returns the Comparator, if any.

## Set Operations
- operations that inherit from Set behave identically on sorted sets, except for two:
    - the Iterator returned by a SortedSet traverses the set in order
    - the array returned by the toArray method contains the sorted set's elements in order
- although the interface doesn't guarantee it, the toString method returns a string containing all elements of the 
sorted set

## Standard Constructors
- if null is passed as the Comparator, the elements natural ordering is used to sort them
- TreeSet, when taking a Collection in its constructor, sorts the elements according to their natural order
    - it would have been better to check dynamically whether the specified Collection was a SortedSet. If so, it
    should have used the SortedSet's Comparator.
    - It also provides a constructor that specifically takes a SortedSet
    - This was probably a mistake (Oracle's words, not mine)....
    
## Range-view operations
- analogous to those provided by the List interface, with a big exception:
    - range views of a sorted set remain valid even if the backing sorted set is modified directly.
    - this is feasible because the endpoints of a range view are absolute points in the element space rather than specific
    elements. 
    - a range view of a SortedSet is just a window onto whatever portion of the set lies in the designated element space
    - Changes to the range-view write back to the underlying set and vice versa.
    - thus it is ok to use range-views for extended periods, unlike range-views on lists.
- provides 3 range-view operations:
- *subSet(lower, upper)*
    - rather than indices, the *lower* and *upper* are objects that must be comparable to the 