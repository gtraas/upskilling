package com.bluesharkdev.upskill.java.collections.linkedlist;

import java.util.LinkedList;

public class LinkedListDemo {
    public static void main(String... args) {
        LinkedList<String> linkedList = new LinkedList<>();

        linkedList.addFirst("first");
        linkedList.add("second");
        linkedList.addLast("last");

        System.out.println("Poll first: " + linkedList.pollFirst());

        System.out.println("Poll last: " + linkedList.pollLast());

        System.out.println("LinkedList size: " + linkedList.size());
    }
}
