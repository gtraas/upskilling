package com.bluesharkdev.upskill.java.collections.hashmap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.*;

public class HashMapDemo {

    private static Map<String, Product> productsByName;

    public static void main(String[] args) {
        productsByName = new HashMap<>();

        //putting and getting
        Product eBike = new Product("E-Bike", "A bike with a battery");
        Product roadBike = new Product("Road Bike", "A bike for competition");
        productsByName.put(eBike.getName(), eBike);
        productsByName.put(roadBike.getName(), roadBike);
        Product nextPurchase = productsByName.get("E-Bike");
        assertEquals("A bike with a battery", nextPurchase.getDescription());

        //getting a key that doesn't exist produces a null
        nextPurchase = productsByName.get("Car");
        assertNull(nextPurchase);

        //replacing a value by using the same key
        Product newEBike = new Product("E-Bike", "This one has a better battery");
        productsByName.put(newEBike.getName(), newEBike);
        assertEquals("This one has a better battery", productsByName.get("E-Bike").getDescription());

        //null as a key
        Product defaultProduct = new Product("Chocolate", "At least buy a chocolate");
        productsByName.put(null, defaultProduct);

        nextPurchase = productsByName.get(null);
        assertEquals("At least buy a chocolate", productsByName.get(null).getDescription());

        //remove a value
        productsByName.remove("E-Bike");
        assertNull(productsByName.get("E-Bike"));

        //re-add the e-bike for the containsKey and containsValue example
        productsByName.put(eBike.getName(), eBike);

        assertTrue(productsByName.containsKey(eBike.getName()));
        assertTrue(productsByName.containsValue(eBike));

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Product {
        private String name;
        private String description;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Product p = (Product) o;
            return Objects.equals(name, p.getName()) && Objects.equals(description, p.getDescription());
        }

        @Override
        public int hashCode() {
            System.out.println(Objects.hash(name, description));
            return Objects.hash(name, description);
        }
    }
}
