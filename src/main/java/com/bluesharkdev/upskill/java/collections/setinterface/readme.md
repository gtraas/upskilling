# The Set interface
## Overview
- A set is a Collection that contains no duplicates
- contains methods only inherited from Collection
- adds the restriction that duplicates are not permitted.
- adds a stronger contract on equals and hashcode
    - this allows Set instances to be compared even if they have different implementations.
    - they are equal if they contain the same elements.
- there are three different Set implementations:
- HashSet
    - stores elements in a hashtable. 
    - best performing of the three
    - makes no guarantees about the order of iteration
- TreeSet
    - stores elements in a red-black tree
    - orders elements based on their value
    - substantially slower than HashSet
- LinkedHashSet
    - implemented as a HashTable with a LinkedList running through it
    - orders elements based on the order in which they were inserted
    - slightly worse performance than HashSet
- Potential interview question:
    - how do you eliminate duplicates from a Collection?
```
//HashSet version
Collection<Type> noDuplicates = new HashSet<>(collectionWithDuplicates);

//Sorted collection with no duplicates
Collection<Type> noDuplicates = new TreeSet<>(collectionWithDuplicates);

//Collection that maintains insertion order
Collection<Type> noDuplicates = new LinkedHashSet<>(collectionWithDuplicates);
```

## Basic Operations
- *size* returns the number of elements
- *isEmpty* does exactly as expected
- *add* adds an element to the Set if it's not already present and returns a boolean indicating whether the element was
added or not
- *remove* removes the specified element from the Set if it is present and returns a boolean indicating whether the 
element was present
- *iterator* returns an Iterator over the Set.


## Set Bulk Operations
- *s1.containsAll(s2)* returns true if s2 is a subset of s1 (s2 is a subset of s1 if set s1 contains all elements in s2)
- *s1.addAll(s2)* transforms s1 into the union of s1 and s2 (set containing all elements contained in either set)
- *s1.retainAll(s2)* transforms s1 into the intersection of s1 and s2 (A set containing only the elements common to both 
sets)  
- *s1.removeAll(s2)* transforms s1 into the asymmetric set difference of s1 and s2 (the set containing all the elements 
found in s1 but not in s2)