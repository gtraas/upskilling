# HashSet
## Introduction
Important aspects:
- It stores unique elements and permits nulls
- It is backed by a HashMap
- It does not maintain insertion order
- It is not thread-safe

## The API
### add()
An element will only be added if it does not already exist in the set. If an element is added, the method returns true, 
otherwise false.
```
Set<String> hashset = new HashSet<>();
  
assertTrue(hashset.add("String Added"));
```

### contains()
This checks whether a given element is present in the HashSet. It returns true if the element is present, otherwise false.
```
@Test
public void whenCheckingForElement_shouldSearchForElement() {
    Set<String> hashsetContains = new HashSet<>();
    hashsetContains.add("String Added");
  
    assertTrue(hashsetContains.contains("String Added"));
}
```
Whenever an object is passed to this method, the hash value gets calculated. Then, the corresponding bucket location 
gets resolved and traversed.

### remove()
This removes an element from the HashSet if it's present. It returns true if the set contained the element, false if not.
```
@Test
public void whenRemovingElement_shouldRemoveElement() {
    Set<String> removeFromHashSet = new HashSet<>();
    removeFromHashSet.add("String Added");
  
    assertTrue(removeFromHashSet.remove("String Added"));
}
```

### clear()
Used when we want to remove all elements from the set.
```
@Test
public void whenClearingHashSet_shouldClearHashSet() {
    Set<String> clearHashSet = new HashSet<>();
    clearHashSet.add("String Added");
    clearHashSet.clear();
     
    assertTrue(clearHashSet.isEmpty());
}
```

### size()
Returns the number of elements in the set.
```
@Test
public void whenCheckingTheSizeOfHashSet_shouldReturnThesize() {
    Set<String> hashSetSize = new HashSet<>();
    hashSetSize.add("String Added");
     
    assertEquals(1, hashSetSize.size());
}
```

### isEmpty()
Returns true if there are no elements in the hashset.
```
@Test
public void whenCheckingForEmptyHashSet_shouldCheckForEmpty() {
    Set<String> emptyHashSet = new HashSet<>();
     
    assertTrue(emptyHashSet.isEmpty());
}
```

### iterator()
Returns an iterator over the elements in the set. The elements are visited in no particular order and iterators are 
fail-fast.

This displays the random iteration here:
```
@Test
public void whenIteratingHashSet_shouldIterateHashSet() {
    Set<String> hashset = new HashSet<>();
    hashset.add("First");
    hashset.add("Second");
    hashset.add("Third");
    Iterator<String> itr = hashset.iterator();
    while(itr.hasNext()){
        System.out.println(itr.next());
    }
}
```
If a set is modified at any time and in any way after the iterator is created, it will throw a 
*ConcurrentModificationException*. To get around this, we can use the iterator's remove method:
```
@Test
public void whenRemovingElementUsingIterator_shouldRemoveElement() {
  
    Set<String> hashset = new HashSet<>();
    hashset.add("First");
    hashset.add("Second");
    hashset.add("Third");
    Iterator<String> itr = hashset.iterator();
    while (itr.hasNext()) {
        String element = itr.next();
        if (element.equals("Second"))
            itr.remove();
    }
  
    assertEquals(2, hashset.size());
}
```
The fail-fast behaviour of an iterator cannot be guaranteed as it's impossible to make any hard guarantees in the 
presence of unsynchronized concurrent modification. Fail-fast iterators throw *ConcurrentModificationException* 
on a best-effort basis. It would be wrong to write a program that depended on this exception for its correctness.

## How HashSet maintains uniqueness
When we put an object into a HashSet, it uses the object's hashcode value to determine if an element is not in the set 
already. Each hashcode value corresponds to a certain bucket location which can contain various elements, for which 
the calculated hash value is the same. But two objects within the same hashcode might not be equal.

Objects within the same bucket are compared using the equals method.

## Performance of a HashSet
 This is affected by two main factors:
 - initial capacity
 - load factor
 
 The expected time complexity of adding an element to a set is O(1), which can drop to O(n) (in java 8, it only drops 
 to O(log n)) in the worst case scenario.  It is therefore essential to maintain the correct HashSet capacity.
 
 The load factor refers to the maximum fill level, after which a set will need to be resized.
 We can create a HashSet with custom values for initial capacity and load factor:
 ```
 Set<String> hashset = new HashSet<>();
 Set<String> hashset = new HashSet<>(20);
 Set<String> hashset = new HashSet<>(20, 0.5f);
 ```
 In the first, we use the defaults, which is an initial capacity of 16 and a load factor of 0.75. In the second, we 
 override default capacity, and in the third, we override both.
 
 A low initial capacity reduces space complexity, but increases the frequency of rehashing which is an expensive process.
 
 On the other hand, though, a high initial capacity increases the cost of iteration and initial memory consumption.
 
 As a rule of thumb:
 - a high initial capacity is good for a large number of entries coupled with little to no iteration
 - a low initial capacity is good for few entries with a lot of iterations.
 
 Therefore, it is important to strike a balance between the two. The default implementation is usually optimised and works
 fine.
 
 