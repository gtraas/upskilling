package com.bluesharkdev.upskill.java.collections.setinterface;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetBulkOperationsDemo {

    public static void main(String... args) {
        Set<Integer> set1 = new TreeSet<>();
        Set<Integer> set2 = new TreeSet<>();

        for (int i = 1; i < 11; i++) {
            set1.add(i);
            if (i % 2 == 0) {
                set2.add(i);
            }
        }



        Set<Integer> union = new TreeSet<>(set1);
        union.addAll(set2);

        Set<Integer> intersection = new TreeSet<>(set1);
        intersection.retainAll(set2);

        Set<Integer> difference = new TreeSet<>(set1);
        difference.removeAll(set2);

        System.out.println("Ünion");
        Iterator<Integer> itr = union.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
        System.out.println("Intersection");
        itr = intersection.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

        System.out.println("Difference");
        itr = difference.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

    }
}
