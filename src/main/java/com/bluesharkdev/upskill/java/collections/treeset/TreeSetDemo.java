package com.bluesharkdev.upskill.java.collections.treeset;

import java.util.*;

public class TreeSetDemo {
    public static void main(String... args) {
        System.out.println("TreeSet Demo");
        Set<String> treeSet = new TreeSet<>();
        Set<String> treeSetComparator = new TreeSet<>(Comparator.comparing(String::length));
        Set<String> syncTreeSet = Collections.synchronizedSet(treeSet);

        System.out.println("Adding element to treeset.");
        boolean successful = treeSet.add("String added");
        System.out.println("Added to treeset?: " + successful);

        System.out.println("Checking the contains method:");
        boolean contains = treeSet.contains("String added");
        System.out.println("Contains?: " + contains);

        System.out.println("Removing the element from the treeset");
        boolean removed = treeSet.remove("String added");
        System.out.println("Removed: " + removed);
        System.out.println("Size should be 0, is: " + treeSet.size());

        System.out.println("Clearing a treeset");
        treeSet.add("1");
        treeSet.add("2");
        treeSet.add("3");
        System.out.println("TreeSet size: " + treeSet.size());
        System.out.println("Clearing treeset");
        treeSet.clear();
        System.out.println("Cleared treeset size: " + treeSet.size());
        System.out.println("Since size is 0, isEmpty should return true, returns: " + treeSet.isEmpty());

        System.out.println("Iteratng over a TreeSet");
        treeSet = new TreeSet<>();
        treeSet.add("First");
        treeSet.add("Second");
        treeSet.add("Third");
        Iterator<String> itr = treeSet.iterator();
        System.out.println("Ascending order");
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

        System.out.println("Descending order");
        TreeSet<String> descTreeSet = new TreeSet<>(treeSet);
        itr = descTreeSet.descendingIterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

        System.out.println("Getting the first element");
        System.out.println(descTreeSet.first());

        System.out.println("Getting the last element");
        System.out.println(descTreeSet.last());

        System.out.println("Subsetting");
        System.out.println("Using the subset() method");
        SortedSet<Integer> treeSet1 = new TreeSet<>();
        treeSet1.add(1);
        treeSet1.add(2);
        treeSet1.add(3);
        treeSet1.add(4);
        treeSet1.add(5);
        treeSet1.add(6);

        System.out.println("Subset between 2 and 6");
        Set<Integer> subSet = treeSet1.subSet(2, 6);
        Iterator<Integer> itr1 = subSet.iterator();
        while (itr1.hasNext()) {
            System.out.println(itr1.next());
        }

        System.out.println("Headset 6");
        subSet = treeSet1.headSet(6);
        itr1 = subSet.iterator();
        while (itr1.hasNext()) {
            System.out.println(itr1.next());
        }

        System.out.println("Tailset 3");
        subSet = treeSet1.tailSet(3);
        itr1 = subSet.iterator();
        while (itr1.hasNext()) {
            System.out.println(itr1.next());
        }

    }
}
