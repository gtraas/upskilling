package com.bluesharkdev.upskill.java.collections.hashset;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {

    public static void main(String... args) {
        Set<String> hashset = new HashSet<>();

        System.out.println("adding an element");
        System.out.println("Element added?: " + hashset.add("String Added"));

        System.out.println("contains()");
        System.out.println("Does it contain our element?: " + hashset.contains("String Added"));

        System.out.println("remove()");
        System.out.println("Did we remove it?: " + hashset.remove("String Added"));
        System.out.println("Further proof: hashset size is " + hashset.size());
        System.out.println("Using isEmpty: " + hashset.isEmpty());

        System.out.println("Iterator");
        hashset.add("First");
        hashset.add("Second");
        hashset.add("Third");
        Iterator<String> itr = hashset.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }

        System.out.println("Removing an element using the iterator");
        itr = hashset.iterator();
        while (itr.hasNext()) {
            String element = itr.next();
            if (element.equals("Second"))
                itr.remove();
        }

        System.out.println("Size should now be 2, is: " + hashset.size());



    }

}
