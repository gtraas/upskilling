# The List interface
## Overview
- It is an ordered *Collection*
- Can contain duplicates
- includes operations for the following:
    - positional access: manipulates elements based on their position in the list. This includes methods such as 
    *get*, *set*, *add* and *addAll*
    - Search: searches for a specified object in the list and returns its numerical position. Search methods include
    *indexOf* and *lastIndexOf*
    - Iteration: extends the *Iterator* semantics to take advantage of the list's sequential nature. the *listIterator*
    method provides this behaviour.
    - Range-view: The *subList* method performs range operations on the list.
- Other pointers:
    - removing items from the back of most List implementations (including ArrayList) is more efficient, performance-wise
    than removing them from the front of the list.
    
## Collection operations
- All methods do what you'd expect them to do
- the *remove* method always removes the **first** occurrence of the specified element in the list.
- the *add* and *addAll* add elements to the end of the list.

The following concatenates one list to another:
```
list1.addAll(list2);
```

A nondestructive form of this produces a third list consisting of both of the other lists:
```
List<E> list3 = new ArrayList<>(list1);
list3.addAll(list2);
```

- *List* strengthens the requirements on hashcode and equal so that two List objects can be compared for logical
equality without regard for their implementation classes. 
- Two *List*s are equal if they contain the same elements in the same order.

## Positional access and search operations
- basic positional access operations are *get*, *set*, *add* and *remove*.
    - the *set* and *remove* operations return the old value that is being overwritten or removed
- *indexOf* and *lastIndexOf* return the first or last index of the specified element in the list.
- The *addAll* operation inserts all the elements of the specified *Collection* starting at the specified position.
    - the elements are inserted in the order in which that *Collection*'s iterator returns them.
    
### Examples:
A method to swap two indexed values in a List:
```
public static <E> void swap(List<E> list, int i, int j) { 
    E tmp = list.get(i);
    list.set(i, list.get(j));
    list.set(j, tmp);
}
```
This is a polymorphic algorithm. It swaps two elements in any List, regardless of the implementation type. Let's use this
method in another polymorphic algorithm:
```
public static void shuffle(List<?> list, Random rnd) {
    for (int i = list.size(); i > 1; i--) {
        swap(list, i-1, rnd.nextInt(i));
    }
}
```
 
The above algorithm is included in the *Collections* class. It is fair (all permutations occur with equal likelihood,
assuming an unbiased source of randomness) and fast (exactly *list.size() - 1* swaps).

## Iterator
- the *iterator* method returns an iterator that'll traverse the list in the correct order.
- the *listIterator* returns a *ListIterator* which is richer than the Iterator.
    - It allows reverse traversal of the list.
    - It has both *next()* and *previous()* methods.
    - To traverse the list backwards, we give the listIterator the list size
```
ListIterator<Type> iterator = list.listIterator(list.size());    
```

## Range-view operations
- *subList(from, to)* returns a *List* whose indices range from *from* inclusive to *to* exclusive.
- The returned List is backed up by the List on which *subList* was called, which means that changes on the sublist are
mirrored in the main list.
```
int i = list.subList(fromIndex, toIndex).indexOf(o);
int j = list.subList(fromIndex, toIndex).lastIndexOf(o);
```
- indexOf and lastIndexOf return the indices of the sublist, not the list in the above example
- If elements are added or removed from the backing list while manipulating the subList, and these actions are not taken 
via the subList, the semantics of the List may become undefined.
- the longer you use a sublist, the greater the likelihood you'll compromise it by modifying the backing list directly or
through another sublist object.
