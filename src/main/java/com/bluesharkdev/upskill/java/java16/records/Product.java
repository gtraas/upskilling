package com.bluesharkdev.upskill.java.java16.records;

public record Product(String name, int price, boolean inStock) {

    //all args constructor that allows validation of input
    public Product {
        if (price < 0) {
            throw new IllegalArgumentException("Price is less than 0");
        }
        //we don't see any assignments as you would expect in a constructor
        //this is because it's implicitly done by the compiler

        //can also add default values
        if (price == 0) {
            price = 100;
        }
    }

}
