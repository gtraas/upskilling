package com.bluesharkdev.upskill.java.java16.records;

import java.util.Objects;

public class OldWay {

    private String vendor;
    private String name;
    private int price;
    private boolean inStock;

    public OldWay(String vendor, String name, int price, boolean inStock) {
        this.vendor = vendor;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OldWay oldWay = (OldWay) o;
        return price == oldWay.price && inStock == oldWay.inStock && vendor.equals(oldWay.vendor) && name.equals(oldWay.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vendor, name, price, inStock);
    }

    @Override
    public String toString() {
        return "OldWay{" +
                "vendor='" + vendor + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", inStock=" + inStock +
                '}';
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }
}
