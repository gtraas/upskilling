# Java 16 Features
## Records

- records are now officially in as a feature
- see the java files for an in-depth look at how they work
- they are not replacements for JavaBeans or POJOs
- records are immutable and final. There are no setter methods
- records can implement an interface, but cannot be extended (hence the final)
- potential record uses

    - DTOs
    - compound map keys
    - when we need multiple return values