# Java 16 Features
## Day Period Support

- new addition to the *DateTimeFormatter* is the symbol *B* which represents an alternative to the am/pm format

```
LocalTime date = LocalTime.parse("15:25:08.690791");
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h B");
```

- instead of *3pm*, we would get something like *3 in the afternoon*.
- other alternatives are *B*, *BBBB* and *BBBBB*
- these are styles for short, full and narrow styles respectively
- all the above give the same output
- see the code example for more