package com.bluesharkdev.upskill.java.java16.dayperiods;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DayPeriodExample {

    public static void main(String[] args) {
        LocalTime localTime = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h B");
        System.out.println(String.format("h B leads to: %s", formatter.format(localTime)));
        formatter = DateTimeFormatter.ofPattern("h BBBB");
        System.out.println(String.format("h BBBB leads to: %s", formatter.format(localTime)));
        formatter = DateTimeFormatter.ofPattern("h BBBBB");
        System.out.println(String.format("h BBBBB leads to: %s", formatter.format(localTime)));

    }

}
