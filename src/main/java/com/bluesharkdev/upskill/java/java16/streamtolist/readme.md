# Java 16 Features
## Stream.toList method

- aim is to reduce boilerplate code with commonly used Stream collectors, such as Collectors.toList

```
List<String> intsAsString = Arrays.asList("1","2","3");
List<Integer> intList = intsAsString.stream().map(Integer::parseInt).collect(Collectors.toList()); // OLD WAY
List<Integer> intList2 = intsAsString.stream().map(Integer::parseInt).toList();
```

- intList2 will be an unmodifiable list.