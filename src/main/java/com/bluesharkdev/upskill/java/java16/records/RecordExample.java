package com.bluesharkdev.upskill.java.java16.records;

public class RecordExample {

    public static void main(String[] args) {

        Product p1 = new Product("Peanut butter", 200, true);
        String name = p1.name(); //example of the "get" method
        System.out.println(p1); //example of the "toString" printout
        Product p2 = new Product("Peanut butter", 200, true);

        System.out.println(p1 == p2);
        System.out.println(p1.equals(p2));

        //testing the default value
        Product p3 = new Product("Priceless", 0, true);
        System.out.println(p3);

        //can also create inline records
        record DiscountedProduct(Product product, boolean discounted) {}

        System.out.println(new DiscountedProduct(p1, true));

        //testing the validation
        Product p4 = new Product("Shark fins", -3, false);

    }

}
