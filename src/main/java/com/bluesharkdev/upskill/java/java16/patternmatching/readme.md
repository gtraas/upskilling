# Java 16 Features
## Pattern matching for instanceof

- this feature has been added to Java 16. It is no longer a preview feature
- refresher:

```
if (person instanceof Employee employee) {
    employee.submitLeaveRequest();
}
```