package com.bluesharkdev.upskill.java.java12.languagechanges;

import org.apache.tomcat.jni.Local;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LanguageChanges {

    public static void main(String[] args) {

        System.out.println("File::mismatch");
        try {
            Path filePath1 = Files.createTempFile("file1", ".txt");
            Path filePath2 = Files.createTempFile("file2", ".txt");
            Files.writeString(filePath1, "Java 12 Article");
            Files.writeString(filePath2, "Java 12 Article");

            long mismatch = Files.mismatch(filePath1, filePath2);
            System.out.println("Mismatch should be -1, is: " + mismatch);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Path filePath3 = Files.createTempFile("file3", ".txt");
            Path filePath4 = Files.createTempFile("file4", ".txt");
            Files.writeString(filePath3, "Java 12 Article");
            Files.writeString(filePath4, "Java 12 Tutorial");

            long mismatch = Files.mismatch(filePath3, filePath4);
            System.out.println("Mismatch should be 8, is: " + mismatch);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\nTeeing example");
        double mean = Stream.of(1, 2, 3, 4, 5)
                .collect(Collectors.teeing(Collectors.summingDouble(i -> i),
                        Collectors.counting(),
                        (sum, count) -> sum / count));

        System.out.println("Average should be 3.0, is: " + mean);

        System.out.println("\nCompact number formatter");
        NumberFormat shortFormat = NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.SHORT);
        shortFormat.setMaximumFractionDigits(2);
        System.out.println("Number should be 2.59K, is: " + shortFormat.format(2592L));

        NumberFormat longFormat = NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.LONG);
        longFormat.setMaximumFractionDigits(2);
        System.out.println("Number should be 2.59 thousand, is: " + longFormat.format(2592));
    }
}
