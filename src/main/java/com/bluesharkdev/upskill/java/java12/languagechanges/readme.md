# Java 12 New Features
## Language Changes and Features
### The String class

- two new methods for the String class
- first is the *indent* 
  - adjusts the indentation of each line based on an integer parameter
  - If the parameter is greater than 0, new spaces will be inserted at the beginning of each line
  - if the parameter is less than 0, it removes spaces from the beginning of each line
    - if a given line does not contain enough white space, all leading white space characters will be removed
  - a basic example
```java
String text = "Hello world!\nWelcome to Java 12";
text = text.indent(4);
System.out.println(text);

text = text.indent(-10);
System.out.println(text);
```
- output looks like this:
```
    Hello world!
    Welcome to Java 12
Hello world!
Welcome to Java 12
```
- second method is *transform*
  - accepts a single argument function as a parameter that will be applied to the string
  - example
```java
String text = "Hello";
String transformed = text.transform(value -> 
        new StringBuilder(value).reverse().toString()
        );
assertEquals("olleH", transformed);
```

### File::mismatch method

```java
public static long mismatch(Path path, Path path2) throws IOException;
```
- method is used to compare two files and find the position of the first mismatched byte in the contents
- return value is inclusive of 0L up to the byte size of the smaller file or -1L if the files are identical
- Let's look at 2 examples

```java
@Test
public void givenIdenticalFiles_shouldNotFindMismatch(){
        Path filePath1=Files.createTempFile("file1",".txt");
        Path filePath2=Files.createTempFile("file2",".txt");
        Files.writeString(filePath1,"Java 12");
        Files.writeString(filePath2,"Java 12");

        long mismatch = Files.mismatch(filePath1,filePath2);
        assertEquals(-1,mismatch);
}

//Second example
@Test
public void givenDifferentFiles_thenShouldFindMismatch() {
    Path filePath3 = Files.createTempFile("file3", ".txt");
    Path filePath4 = Files.createTempFile("file4", ".txt");
    Files.writeString(filePath3, "Java 12 Article");
    Files.writeString(filePath4, "Java 12 Tutorial");
    
    long mismatch = Files.mismatch(filePath3, filePath4);
    assertEquals(8, mismatch);
}
```

### A new *teeing* Collector

- a new *teeing* collector was introduced to Java 12 as an addition to the Collectors class
- it is a composite of 2 downstream collectors
- every element is processed by both downstream collectors
- their results are passed to the merge function and transformed to the final result
- example useage of teeing collector is counting an average from a set of numbers
- the first collector parameter will sum up the value
- the second collector parameter will give us a count
- merge function will take these results and count the average
```java
@Test
public void givenSetOfNumbers_thenCalculateAverage() {
    double mean = Stream.of(1, 2, 3, 4, 5)
        .collect(Collectors.teeing(Collectors.summingDouble(i -> i),
        Collectors.counting(), (sum, count) -> sum / count));
    assertEquals(3.0, mean);
}
```

### Compact Number Formatting

- designed to represent a number in a shorter form, based on patterns in a given locale
- can get its instance from the *CompactNumberInstance* method in the *NumberFormat* class
```java
public static NumberFormat getCompactNumberInstance(Local locale, NumberFormat.Style formatStyle);
```
- local parameter is responsible for providing proper format patterns
- format style can be SHORT or LONG
  - let's consider these with an example... 1000 in the USA
  - the SHORT style would be written as 1k
  - the LONG style would be written as 1 thousand
```java
@Test
public void givenNumber_thenCompactValues() {
    NumberFormat likesShort = NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.SHORT);
    likesShort.setMaximumFractionDigits(2);
    assertEquals("2.59K", likesShort.format(2592));
    
    NumberFormat likesLong = NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.LONG);
    likesLong.setMaximumFractionDigits(2);
    assertEquals("2.59 thousand", likesLong.format(2592));
}
```