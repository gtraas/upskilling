# Java 12 New Features
## JVM Changes
### Shenandoah GC

- experimental GC algorithm
- not included in default builds
- reduces the GC pause times by doing evacuation work simultaneously with running Java threads
- pause times are not dependent on the heap's size and should be consistent
- Garbage collecting a 200 GB heap or a 2 GB heap should have similar low pause behaviour
- Shenandoah is part of the Java 15 release
