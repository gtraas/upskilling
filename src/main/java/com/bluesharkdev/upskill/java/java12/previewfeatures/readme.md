# Java 12 New Features
## Preview features
### *switch* expressions

- let's compare the old and new *switch* statement
- old syntax:
```
DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
String typeOfDay = "";
switch (dayOfWeek) {
    case MONDAY:
    case TUESDAY:
    case WEDNESDAY:
    case THURSDAY:
    case FRIDAY:
        typeOfDay = "Work day";
        break;
    case SATURDAY:
    case SUNDAY:
        typeOfDay = "Weekend";
}
```
- same statement with new switch
```
typeOfDay = switch(dayOfWeek) {
    case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> "Work day";
    case SATURDAY, SUNDAY -> "Weekend";
};
```
- more compact and more readable
- remove the need for break statements
- can assign the switch statement directly to a variable
- also possible to execute code in switch statements
```
switch(dayOfWeek) {
    case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> logger.info("Work day");
    case SATURDAY, SUNDAY -> logger.info("Weekend");
};
```
- more complex logic can be wrapped in curly braces
```
case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY ->{
    logger.info("Work day");
    //more complicated stuff
};
```

### Pattern matching for *instanceof*
- let's do old vs new
- old
```
Object obj = "Hello world!";
if (obj instanceof String) {
    String s = (String) obj;
    int length = s.length();
}
```
- new
```
if (obj instanceof String s) {
    int length = s.length();
}
```