# Setting the Java version with Maven
- we can check the default JDK version using the command:
```
mvn -v
```
- this prints out:
```
Apache Maven 3.6.1 (d66c9c0b3152b2e69ee9bac180bb8fcc8e6af555; 2019-04-04T21:00:29+02:00)
Maven home: C:\apache-maven-3.6.1
Java version: 1.8.0_201, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk1.8.0_201\jre
Default locale: en_US, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

## Using the Compiler plugin
### Compiler plugin
- the first option is setting the version in the compiler properties
```
<properties>
    <maven.compiler.target>1.8</maven.compiler.target>
    <maven.compiler.source>1.8</maven.compiler.source>
</properties>
```
- the Maven compiler accepts these properties with *-target* and *-source* versions
- if we want to use Java 8, we need to set the *-source* to *1.8*
- for the compiled classes to be compatible with 1.8, *-target* needs to be set to *1.8*
- the default value is 1.6
- we can configure the compiler plugin directly, though:
```
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.1</version>
            <configuration>
                <source>1.7</source>
                <target>1.7</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```
- this has additional configuration properties that allow us to have more control over the compilation process beyond the 
*-source* and *-target* versions

### Java 9 and beyond
- starting from Java 9, we have a new *-release* command-line option. 
- this automatically configures the compiler to produce class files that will link to the given version
- by default, *-source* and *-target* options don't guarantee a cross-compilation
- means we can't run our app on older versions of the platform
- to compile and run the programs for older versions of Java, we would need to add the *-bootclasspath* option
- therefore, the new *-release* option replaces three flags: *-source*, *-target* and *-bootclasspath*
- in the properties section:
```
<properties>
    <maven.compiler.release>7</maven.compiler.release>
</properties>
``` 
- and for the *maven-compiler-plugin*:
```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.8.0</version>
    <configuration>
        <release>7</release>
    </configuration>
</plugin>
```
- furthermore, we don't need a JDK 7 installed on the compiling machine, because Java 9 already contains the relevant 
information

## Spring Boot specification
- specifies the java version in the *properties* tag in the pom file
- first, we need to add the *spring-boot-starter-parent* as a parent to our project
```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.0.5.RELEASE</version>
</parent>
```
- this allows us to configure default plugins and multiple properties, including the Java version
- by default, the Java version is 1.8
- we can override the default version of the property by specifying the following:
```
<properties>
    <java.version>1.9</java.version>
</properties>
```
- Java 8 is the minimum for Spring Boot
- we will NOT be able to configure or use Spring Boot for older version of Java
