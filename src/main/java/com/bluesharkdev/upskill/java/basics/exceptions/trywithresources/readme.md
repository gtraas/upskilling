# Try with resources
## Overview

- since Java 7, we have been able to declare resources to be used in a try block with the assurance that the resource will be closed after execution of the block
- resource needs to implement the *AutoCloseable* interface

## Using try-with-resources

- to be auto-closed, a resource must be both declared and initialised inside the try:

```java
try (PrintWriter writer = new PrintWriter(new File("test.txt"))) {
    writer.println("Hello world");
}
```

## Replacing try-catch-finally with try-with-resources

- simple and obvious way to use the new try-with-resources is to replace the try-catch-finally blocks
- let's compare verbosity. First the try-catch-finally:

```java
Scanner scanner = null;
try {
    scanner = new Scanner(new File("test.txt"));
    while (scanner.hasNext()) {
        System.out.println(scanner.nextLine());
    }
} catch (FileNotFoundException e) {
    e.printStackTrace();
} finally {
    if (scanner != null) {
        scanner.close();
    }
}
```

- now, with try-with-resources

```java
try (Scanner scanner = new Scanner(new File("test.txt"))) {
    while (scanner.hasNext()) {
        System.out.println(scanner.nextLine());
    }
} catch (FileNotFoundException e){
    e.printStackTrace();
}
```

## try-with-resources with multiple resources

- can declare multiple resource with no problem by sepearting them with a semi-colon

```java
try (Scanner scanner = new Scanner(new File("test.txt"));
        PrintWriter writer = new PrintWriter(new File("testWrite.txt"))) {
    while(scanner.hasNext()) {
        writer.print(scanner.nextLine());
    }
}
```

## A custom resource with AutoCloseable

- if we have a custom resource we want to use with a try-with-resources, that resource will need to implement the *Closeable* or *AutoCloseable* interfaces and override the *close()* method

```java
public class MyResource implements AutoCloseable {
    @Override
    public void close() throws Exception {
        System.out.println("Closed MyResource");
    }
}
```

## Resource closing order

- resources that were defined first will be closed last
- example:
 
#### Resource 1

```java
public class AutoCloseableResourcesFirst implements AutoCloseable {
    public AutoCloseableResourcesFirst() {
        System.out.println("Constructor -> AutoCloseableResourcesFirst");
    }    
    
    public void doSomething() {
        System.out.println("doSomething -> AutoCloseableResourcesFirst");
    }
    
    @Override
    public void close() throws Exception {
        System.out.println("Closed AutoCloseableResourcesFirst");
    }
}
```

#### Resource 2

```java
public class AutoCloseableResourcesSecond implements AutoCloseable {
    public AutoCloseableResourcesSecond() {
        System.out.println("Constructor -> AutoCloseableResourcesSecond");
    }    
    
    public void doSomething() {
        System.out.println("doSomething -> AutoCloseableResourcesSecond");
    }
    
    @Override
    public void close() throws Exception {
        System.out.println("Closed AutoCloseableResourcesSecond");
    }
}
```

#### Code

```java
private void orderOfClosingResources() throws Exception {
    try (AutoCloseableResourcesFirst af = new AutoCloseableResourcesFirst();
         AutoCloseableResourcesSecond as = new AutoCloseableResourcesSecond()) {
        
        af.doSomething();
        as.doSomething();
        
    }
}
```

#### Output

```
Constructor -> AutoCloseableResources_First
Constructor -> AutoCloseableResources_Second
Something -> AutoCloseableResources_First
Something -> AutoCloseableResources_Second
Closed AutoCloseableResources_Second
Closed AutoCloseableResources_First
```

## *catch* and *finally*

- the try block can still have *catch* and *finally* blocks, which will work in the same way as the traditional try

## Java 9 - effectively final variables

- before Java 9, we could only use fresh variables in the try-with-resources block
- we can use final or effectively final variables inside a try-with-resources as from Java 9

```java
final Scanner scanner = new Scanner(new File("testRead.txt"));
PrintWriter writer = new PrintWriter(new File("testWrite.txt"));
try (scanner;writer) {
    //do stuff
}
```