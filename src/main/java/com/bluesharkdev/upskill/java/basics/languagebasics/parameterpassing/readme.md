# Pass by value and Pass by reference
## Pass-by-value vs Pass-by-reference
### Pass-by-value
- in this method, the caller and callee method operate on two different variables that are copies of one another
- changes to one do not modify the other
- while calling a method, parameters passed to the callee method will be clones of the original parameters

### Pass-by-reference
- the caller and callee operate on the same object
- the unique identifier of the object is sent to the method
- any changes to the parameter's instance members will result in that change being made to the original value

## Parameter passing in Java
- primitive variables store the actual values whereas non-primitives store the reference variables which point to the addresses of teh objects they're referring to
- both values and references are stored in the stack memory
- arguments are always pass-by-value
- during method invocation, a copy of the argument, whether its a value or a reference, is created in stack memory and passed to the method
- with primitives, the value is copied inside stack memory which is then passed to the callee
- with non-primitives, a reference in stack memory points to the actual data which lives in the heap

### Passing primitive types
- example
```
public class PrimitivesUnitTest {
 
    @Test
    public void whenModifyingPrimitives_thenOriginalValuesNotModified() {
        
        int x = 1;
        int y = 2;
       
        // Before Modification
        assertEquals(x, 1);
        assertEquals(y, 2);
        
        modify(x, y);
        
        // After Modification
        assertEquals(x, 1);
        assertEquals(y, 2);
    }
    
    public static void modify(int x1, int y1) {
        x1 = 5;
        y1 = 10;
    }
}
```
- the variables x and y in the main method are primitives and their values are stored in the stack
- when we call modify, an exact copy is made for each of these variables and stored at a different location in stack memory
- any modification to these copies affects only the copies and leaves the originals unaltered

### Passing non-primitives
- a Java object is stored in 2 stages
  - the reference variable is stored in stack memory
  - the object they are referring to is stored in heap memory
- whenever an object is passed as an argument, an exact copy of the reference variable is created which points to the same location of the object in heap memory
- thus, when we make changes to the object in the method, that change will be reflected in the original object
- an example:
```
public class NonPrimitivesUnitTest {
 
    @Test
    public void whenModifyingObjects_thenOriginalObjectChanged() {
        Foo a = new Foo(1);
        Foo b = new Foo(1);

        // Before Modification
        assertEquals(a.num, 1);
        assertEquals(b.num, 1);
        
        modify(a, b);
        
        // After Modification
        assertEquals(a.num, 2);
        assertEquals(b.num, 1);
    }
 
    public static void modify(Foo a1, Foo b1) {
        a1.num++;
       
        b1 = new Foo(1);
        b1.num++;
    }
}
 
class Foo {
    public int num;
   
    public Foo(int num) {
        this.num = num;
    }
}
```