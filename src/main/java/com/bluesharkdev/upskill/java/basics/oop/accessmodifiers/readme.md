# Access modifiers
## Overview
- 4 access modifiers
  - public
  - private
  - protected
  - default (no keyword)

## Default
- when we don't use an access modifier specifically, Java sets a default access to the given class, method or property
- default access modifier is also called package-private which means all members are visible within the same package
- not accessible outside the package

## Public
- when we add *public* to a class, method or field, we're making it available to the entire world
- this is the least restrictive access modifier

## Private
- any method, property or constructor with the *private* keyword is accessible only from the same class
- this is the most restrictive access modifier
- it is core to the concept of encapsulation

## Protected
- between *public* and *private*, we have *protected*
- fields, methods and constructors with *protected* access are accessible from classes in the same package, and also from their subclasses

## Comparison

| Modifier | Class  | Package | Subclass | World  |
|:--------:|:------:|:-------:|:--------:|:------:|
| public | Y | Y | Y| Y |
| protected | Y | Y | Y | N |
| default | Y | Y | N | N |
| private | Y | N | N | N |
