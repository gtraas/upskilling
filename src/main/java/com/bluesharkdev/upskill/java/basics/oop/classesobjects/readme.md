# Classes & Objects
## Overview
- classes are blueprints or templates for objects
- objects are created from classes and contain varying states within their fields and present certain behaviours with their methods

## Classes
- a class represents a definition of an object
- contains fields, constructors and methods
- example of a class representing a car:
```
class Car {

    // fields
    String type;
    String model;
    String color;
    int speed;

    // constructor
    Car(String type, String model, String color) {
        this.type = type;
        this.model = model;
        this.color = color;
    }
    
    // methods
    int increaseSpeed(int increment) {
        this.speed = this.speed + increment;
        return this.speed;
    }
    
    // ...
}
```
- this represents a car in general
- can create any type of car from this class
- use fields to hold state and use a constructor to create objects
- every java class has an empty constructor by default
- used when we don't provide a specific implementation
- this is how it would look:
```
Car() { }
```
- this constructor initialises all fields of the object to their default values
- Strings initialised to null, integer to 0
- our class has a specific constructor because we want our objects to have their fields defined when we create them.
```
Car(String type, String model) {
    // ...
}
```

## Objects
- objects are created from classes at runtime
- objects of a class are called instances
- we create and initialise as such:
```
Car focus = new Car("Ford", "Focus", "red");
Car auris = new Car("Toyota", "Auris", "blue");
Car golf = new Car("Volkswagen", "Golf", "green");
```
- all cars are now created and their speeds are 0
- can change this by invoking the *increaseSpeed* method
```
focus.increaseSpeed(10);
auris.increaseSpeed(20);
golf.increaseSpeed(30);
```

## Access Modifiers
- without an access modifier, we use default package-private, which means ony classes in the same package can access the class
- usually we add a *public* modifier for constructors to allow access from all other objects
```
public Car(String type, String model, String color) {
    // ...
}
```
- classes usually have public modifiers, but the fields are usually private
- we add getters and setters to allow access to the fields
- the same Car class  with fully-specified access control:
```
public class Car {
    private String type;
    // ...

    public Car(String type, String model, String color) {
       // ...
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSpeed() {
        return speed;
    }

    // ...
}
```
- class is marked public which means we can use it in any package
- constructor is marked public which means we can create this from any other object
- fields are all private which means they're not accessible from our object directly, but we provide access to them via getters and setters
- *type* and *model* fields don't have getters and setters because they hold internal data and can only be defined through the constructor at initialisation
- *color* can be accessed and changed, whereas speed can only be accessed
- we enforce speed adjustments through specialised public methods *increaseSpeed()* and *decreaseSpeed()*
- we use access control to encapsulate the state of the object.