# Syntax in Java
## Overview
- Java is statically-typed, object-oriented language that is platform independent.

## Data types
- basic data types that store simple data
- form the foundation of data manipulation
- there are primitive types for 
  - integer values (int, long, byte, short)
  - floating point values (float and double)
  - character values (char)
  - logical values (boolean)
- there are also reference types
  - these are objects that contain references to values or other objects, or even null.
  - the String class is a good example of a reference type.
    - an instance represents a sequence of characters

## Declaring variables in Java
- to declare a variable, specify its name and type
```
int a;
int b;
double c;
```
- these variables are not initialised, so Java automatically initiates them.
- int is initiated to 0
- double is initiated to 0.0
- we can use assignment during declaration to initialise them
```
int a = 10;
```
- an identifier is a name of any length consisting of digits, letters, an underscore and dollar signs that conforms to the following rules:
  - starts with a letter, underscore or dollar
  - cannot be a reserved keyword
  - cannot be *true*, *false* or *null*
- declaration and initialization of other types of variables follow the same syntax
```
String name = "Name;
char toggler = 'y';
boolean isVerified = false;
```
- main difference between the literal values of String and char is that String has double quotes and char has single quotes.

## Arrays
- an array is a reference type that can store a collection of values of a specific type
- general syntax for declaring an array is:
```
type[] identifier = new type[length];
```
- the type can be any primitive or reference type
- to declare an array that will hold a maximum of 100 integers:
```
int[] numbers = new int[100];
```
- to refer to a specific element or to assign a value to an element, we use the identifier and its index:
```
numbers[0] = 1;
numbers[1] = 2;
numbers[2] = 3;
int thirdElement = numbers[2];
```
- arrays are zero-based indexes, so the first element is always index 0
- we can get the length of an array using *arrayName.length*
  - notice there is no method call here

## Java keywords
- keywords are reserved words that have special meaning in Java
- examples are *public*, *static*, *class*, *main*, *new*, *instanceof*

## Operators
### Arithmetic operators
- there are several arithmetic operators available in java
  - *+* (addition, also string concatenation)
  - *-* (subtraction)
  - x (multiplication)
  - */* (division)
  - *%* (remainder)

### Logical operators
- the following logical operators are used for evaluating boolean expressions
  - && (AND)
  - || (OR)
  - ! (NOT)

### Comparison Operators
- to compare one variable to another, use:
  - < (less than)
  - <= (less than or equal to)
  - *>* (greater than)
  - *>=* (greater than or equal to)
  - == (equal)
  - != (not equal)

## Java Program Structure
- basic unit is a class
- a class can have one or more fields (or properties), methods or even other class members called inner classes
- for a class to be executable, it must have a *main* method
