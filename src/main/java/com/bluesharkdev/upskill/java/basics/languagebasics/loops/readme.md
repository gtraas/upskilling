# Java Loops
## For loops
### Simple for loop
- control structure that allows us to repeat certain operations by incrementing and evaluating a loop counter
- syntax for a standard for loop
```
for (initialization; condition; step) 
    statement;
    
//in an actual example
for (int i = 0; i < 5; i++) {
    print("Simple loop: i = " + i);
}
```
- the initialisation, condition and step are all optional
```
for ( ; ; ) {
    //an infinite loop
}
```

### Labelled for loops
- we can label our for loops
- useful for nested loops, so we can break / continue from a specific for loop
- example:
```
aa: for (int i = 1; i <= 3; i++) {
    if (i == 1)
        continue;
    bb: for (int j = 1; j <= 3; j++) {
        if (i == 2 && j == 2) {
            break aa;
        }
        print(i + " " + j);
    }
}
```

### Enhanced for loop
- makes it easier to iterate over all elements in an array or collection
- syntax:
```
for (Type item: items) 
    statement;
```
- need to declare only the identifier for an element we're iterating over currently and the source of the array or collection
- in English: For each element in *items*, assign the element to the *item* variable and run the body of the loop
- simple example:
```
int[] arr = { 0, 1, 2, 3, 4 };
for (int num: intArr) {
    print("enhanced for loop: i = " + num);
}
```
- we can use it for various data structures
```
for (String item: listOfStrings) {
    print(item);
}
```
- we can iterate over a Set<String> as follows
```
for (String item : set) {
    System.out.println(item);
}
```
- and given a Map<String, Integer> we can iterate over that too
```
for (Entry<String, Integer> entry : map.entrySet()) {
    System.out.println(
      "Key: " + entry.getKey() + 
      " - " + 
      "Value: " + entry.getValue());
}
```

### Iterable.forEach()
- with Java 8+ we can use the forEach method in the Iterable interface
- it accepts a lambda expression representing what action we want to perform
- internally it delegates to the standard loop
```
default void forEach(Consumer<? super T> action) {
    Objects.requireNonNull(action);
    for (T t : this) {
        action.accept(t);
    }
}
```
- example:
```
List<String> names = new ArrayList<>();
names.add("Larry");
names.add("Steve");
names.add("James");
names.add("Conan");
names.add("Ellen");

names.forEach(name -> System.out.println(name));
```

## While loops
- repeats a statement or block of statements while its controlling condition is true
- syntax:
```
while (condition) 
    statement;
```
- the condition is evaluated before the first iteration of the loop
- if the condition starts out false, the loop will not run
- example
```
int i = 0;
while (i < 5) {
    System.out.println("While loop: i = " + i++);
}
```

## Do-While loops
- works exactly like a while loop except that the first check of the condition happens after the first iteration of the loop
- syntax:
```
do {
    statement;
} while (Boolean-expression);
```
- example:
```
int i = 0;
do {
    System.out.println("Do-While loop: i = " + i++);
} while (i < 5);
```