# Java Primitives
## Primitive Data Types
- there are eight primitives
  - int
  - byte
  - short
  - long
  - float
  - double
  - boolean
  - char
- they are stored directly on the stack
- they are not objects and represent raw values

### int
- holds non-fractional number values
- uses 32 bits of memory
- it can represent values from -2,147,483,648 to 2,147,483,647
- an unsigned int can hold a value up to 4,294,967,295
- the default value of an int is 0
- decimal values are chopped off an int during arithmetic

### byte
- similar to an int, but only takes up 8 bits of memory
- can hold values from -128 to 127
- default value is 0

### short
- halfway between a byte and an int
- it takes up 16 bits of memory
- its range is -32,768 to 32,767
- default value is 0

### long
- double the size of an int
- takes up 64 bits of memory
- ranges between -9,223,372,036,854,775,808 and 9,223,372,036,854,775,807
- default value is 0

### float
- a single-precision decimal number representation
- once we get past 6 decimal places, the number becomes less precise
  - for more precise work, we should use BigDecimal
- stored in 32 bits of memory
- because of the decimal point, its range is different
- smallest decimal is 1.40239846 x 10<sup>-45</sup>
- largest is 3.40282347 x 10<sup>38</sup>
- default value is 0.0
- we add a *f* to the end of a literal number to force a float, otherwise Java will automatically try to use a double
```
float f = 3.145f;
```

### double
- this is a double-precision decimal number
- stored in 64bits of memory
- suffers from the same precision limitation as floats do
- range is 4.9406564584124654 x 10<sup>-324</sup> to 1.7976931348623157 x 10<sup>308</sup>
- default value is 0.0
- we add a D to designate a literal as a double
```
double d = 3.14D;
```

### boolean
- simplest primitive
- contains two values: *true* or *false*
- uses a single bit
  - for convenience, Java pads the value and stores it in a single byte
- defaults to false

### char
- 16-bit integer that represents a Unicode-encoded character
- ranges from 0 to 65,535
  - \u0000 to \uffff
- when assigning literals, we can use any character literal or the associated number:
```
char c = 'a';
//is the same as
char c = 65;
```
- default value is \u0000

### Overflow
- what happens when we try to store a value that's larger than the maximum allowed?
- we get overflow
```
int i = Integer.MAX_VALUE;
int j = i + 1;
// j will roll over to -2,147,483,648
```
- underflow is the same issue except it involves storing a value smaller than the minimum.
- when numbers underflow, they return to 0 or 0.0

### Autoboxing
- each primitive also has an object implementation
- eg, an Integer can wrap an int
- Java can perform a conversion from primitive to object using autoboxing
```
Integer i = 1;
```