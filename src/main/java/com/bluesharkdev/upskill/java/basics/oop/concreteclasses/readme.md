# Concrete classes
# What is a concrete class
- it is a class that we can create an instance of using the *new* keyword
- it is a full implementation of its blueprint
- example:
```
public class Car {
    public String honk() {
        return "beep!";
    }

    public String drive() {
        return "vroom";
    }
}
```

## Java Abstraction vs Concrete classes
- not all Java types implement all their methods
- this flexibility, or *abstraction*, allows us to think in more general terms about the domain we're trying to model
- can achieve abstraction using interfaces and abstract classes

### Interfaces
- an interface is a blueprint for a class
```
interface Driveable {
    void honk();
    void drive();
}
```
- we cannot instantiate this with *new* because it has unimplemented methods
- concrete classes can implement these methods

### Abstract classes
- it is a class with unimplemented methods
```
public abstract class Vehicle {
    public abstract String honk();

    public String drive() {
        return "zoom";
    }
}
```
- we mark abstract classes with the keyword *abstract*
- again, we can't use the *new* keyword

### Concrete classes
- don't have any unimplemented methods
- based on the above hierarchy
```
public class FancyCar extends Vehicle implements Driveable {
    public String honk() { 
        return "beep";
    }
}
```
- we can instantiate the FancyCar class with the *new* keyword because it has no unimplemented methods
