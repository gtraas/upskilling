# The Java main method
## Common signature
- most common signature
```
public static void main(String[] args) { }
```
- there are other variants
```
//with the [] near the args on either side
public static void main(String []args) {}
public static void main(String args[]) {}

//the arguments can be represented as varargs
public static void main(String...args) {}

//we can add strictfp for when we are working with floating point values
public strictfp static void main(String[] args) {}

//synchronized and final are also valid keywords for use on the main method, but they won't have any effect
//we can add final to the args to ensure it cannot be changed
public static void main(final String[] args) {}

//therefore we could have a main method that looks like this
final static synchronized strictfp void main(final String[] args) {}
```

## Having more than one main() method
- to specify which main method the JVM needs to use as the entry point to our application, we specify it in the MANIFEST.MF file
```
Main-Class: mypackage.ClassWithMainMethod
```
- this is done when creating an executable *.jar* file