# Constructors
## Overview
- constructors are the gatekeepers of object-oriented design

## Example: Setting up a bank account
- imagine we need to create a class that represents a bank account
- it will contain Name, Date of Creation and Balance
- we will also override the *toString()* method for printing details to the console
```
class BankAccount {
    String name;
    LocalDateTime opened;
    double balance;
    
    @Override
    public String toString() {
        return String.format("%s, %s, %f", 
          this.name, this.opened.toString(), this.balance);
    }
}
```
- it now contains all the information required, but it does not have a constructor
- if we create a new account, the field value will not be initialised

## No-Argument constructor
- let's fix that with a constructor
```
class BankAccount {
    public BankAccount() {
        this.name = "";
        this.opened = LocalDateTime.now();
        this.balance = 0.0d;
    }
}
```
- the compiler automatically adds this method, but it does not initialise the variables, leading to an NPE
- this method fixes the NPE problem.

## A Parameterized Constructor
- the real benefit of constructors is that they help us to maintain encapsulation when injecting state into the object
- to be actually useful, we need to be able to inject some initial values into the object
```
class BankAccount {
    public BankAccount() { ... }
    public BankAccount(String name, LocalDateTime opened, double balance) {
        this.name = name;
        this.opened = opened;
        this.balance = balance;
    }
}
```
- to use this:
```
LocalDateTime opened = LocalDateTime.of(2018, Month.JUNE, 29, 06, 30, 00);
BankAccount account = new BankAccount("Tom", opened, 1000.0f); 
account.toString();
```

## Copy constructors
- constructors need not be limited to initialization alone
- if we need to create a new account from an existing one, we need a copy constructor
- in our example, the new account should have the same name as the previous account, today's creation date and zero funds
```
public BankAccount(BankAccount other) {
    this.name = other.name;
    this.opened = LocalDateTime.now();
    this.balance = 0.0f;
}
```
- to use this new constructor:
```
LocalDateTime opened = LocalDateTime.of(2018, Month.JUNE, 29, 06, 30, 00);
BankAccount account = new BankAccount("Tim", opened, 1000.0f);
BankAccount newAccount = new BankAccount(account);
```

## A Chained Constructor
- we may also want to infer some of the parameters or give them some default values
- eg, we want to create a new bank account using only a name
```
public BankAccount(String name, LocalDateTime opened, double balance) {
    this.name = name;
    this.opened = opened;
    this.balance = balance;
}
public BankAccount(String name) {
    this(name, LocalDateTime.now(), 0.0f);
}
```
- with the keyword *this*, we are calling the other constructor
- if we want to chain a superclass constructor, we use the *super* keyword instead of *this*
- *this* or *super* has to be the first statement in a chained constructor

## Value Types
- a value object is an object that does not change its internal state after initialization
- this means the object is immutable
- an example of an immutable class
```
class Transaction {
    final BankAccount bankAccount;
    final LocalDateTime date;
    final double amount;

    public Transaction(BankAccount account, LocalDateTime date, double amount) {
        this.bankAccount = account;
        this.date = date;
        this.amount = amount;
    }
}
```
- the keyword *final* is what makes the fields immutable
- each of these members can only be initialized by the constructor within the class
- we cannot reassign them later on
- if we create multiple constructors for the *Transaction* class, each constructor will need to initialise every final variable
