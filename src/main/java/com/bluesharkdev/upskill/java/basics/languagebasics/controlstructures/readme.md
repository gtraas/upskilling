# Control structures in Java
## Overview
- three types of control structures
  - conditional branches for choosing between two or more paths
    - if/else/else if, ternary operator and switch
  - iteration
    - for, while and do while
  - branching statements
    - break and continue

## If/Else/Else If
- most basic control structure
```
if (condition) {
    ....statements
} else if (other condition) {
    ....more statements
} else {
    ....last resort statements
} 
```

### Ternary operator
- shorthand version of an if/else statement
```
if (count > 2) {
    doSomething();
} else {
    doSomethingElse();
}
//can be refactored into this
count > 2 ? doSomething() : doSomethingElse();
```
- it can be good for readability, but it isn't always a good substitute for the if/else

### Switch
- if we have multiple cases to choose from, we can use a switch statement
```
int count = 3;
switch(count) {
case 0:
    print("Count is 0");
    break;
case 1:
    print("Count is 1");
    break;
default:
    print("Count is higher than 1 or negative");
    break;
}
```
- 3 or more if/else statements can be hard to read, so sometimes we can use a switch statement
- has scope and input limitations

### Loops
- used when we need ot repeat code multiple times
```
//for loop
for (int i = 1; i<= 50; i++) {
    doSomething();
}

//while loop
int counter = 1;
while (counter <= 50) {
    doSomething();
    counter++;
}
```

### Break
- used when we need to exit early from a loop
```
List<String> names = getNameList();
String name = "John Doe";
int index = 0;
for ( ; index < names.size(); index++) {
    if (names.get(index).equals(name)) {
        break;
    }
}
```

### Continue
- used to skip the rest of the loop we are in
```
List<String> names = getNameList();
String name = "John Doe";
String list = "";
for (int i = 0; i < names.length; i++) { 
    if (names[i].equals(name)) {
        continue; //we'll jump to the top of the for and skip the list addition
    }
    list += names[i];
}
```