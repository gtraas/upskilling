# Packages
## Overview
- use packages to group related classes, interfaces and sub-packages
- main benefits:
  - making related types easier to find
  - avoiding naming conflicts
    - com.abc.Application and com.def.Application are unique
  - controlling access: can control visibility and access by combining packages and access modifiers

## Creating a package
- the package declaration is always the first line in a class
- if we don't define the package, the class goes into the *default* or unnamed package
- then we lose the benefits of the package structure and can't have sub pakcages
- can't import the types in the default package into other packages
- protected and package-private access scopes would be meaningless

### Naming conventions
- names must be all lower case
- package names are period delimited
- names usually determined by the company or org that creates them
  - usually a reverse of the company website

### Directory structure
- each package has its own directory

## Using package members
### Imports
- can import a single type from a package or use an asterisk to import all types in a package

### Fully qualified names
- may be using two classes with the same name from different packages
- most common example would be java.sql.Date and java.util.Date.
- when we get a naming conflict, we need to use the fully qualified class name for at least 1 of the classes
