# The SOLID Principles
## What it stands for
- S: Single responsibility principle
- O: Open-closed principle
- L: Liskov substitution principle
- I: Interface segregation principle
- D: Dependency inversion principle

## What they mean:
### Single responsibility principle
- a class should have a single responsibility.
- it should have one, and only one, reason to change
- example:
    - consider a module that compiles and prints a report:
    - such a module can be changed for 2 reasons
    - the content of the report could change AND the format of the report could change
    - these 2 things change for 2 very different reasons: one substantive and the other cosmetic
    - the single responsibility principle says that these are actually two separate responsibilities and should be 
    separated out into 2 different classes.
    - if there is a change in the compilation process, there is greater danger of the printing code breaking if it is
    part of the same class.

### Open-closed principle
- software entities should be open for extension, but closed to modification
- the entity can allow its behaviour to be extended without modifying its source code
- polymorphic open/closed principle
    - refers to the use of abstraced interfaces
    - implementations can be changed and multiple implementations could be created and polymorphically substituted for 
    each other
    - advocates inheritance from abstract base classes

### Liskov substitution principle
- objects in a program should be replaceable with instances of their subtypes without altering the correctness of 
that program
- if *S* is a subtype of *T*, then objects of type *T* may be replaced (or substituted) with objects of type *S* without
altering any of the desireable properties of the program (correctness, task performed, etc)
- in simple terms:
    - can replace objects of a parent class with objects of a subclass without breaking the application
    - requires that all subclasses behave in the same way as the parent class
    - To do this:
    - don't implement any stricter validation rules on input parameters than implemented by the superclass
    - apply at least the same rules to all output parameters as applied by the parent class
    
### Interface segregation 
- many client-specific interfaces are better than one general-purpose interface
- no client should be forced to depend on methods it does not use
- in simple terms, avoid interfaces and classes that have multiple responsibiities as they are bloated, change often, 
and make the software hard to maintain

### Dependency inversion
- one should depend on abstractions, not concretions
- high-level modules should not depend on low-level modules. Both should depend on abstractions (interfaces)
- abstractions should not depend on details. Details (concrete implementations) should depend on abstractions
- by dictating that both high- and low-level objects should depend on the same abstraction, this design principle 
*inverts* the way some people think about OO programming
    
