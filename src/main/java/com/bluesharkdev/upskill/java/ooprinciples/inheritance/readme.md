# Inheritance

## Definition
- a feature that allows a class to inherit behaviour and data from another class

## Difference between inheritance and encapsulation
- inheritance is an OO concept that creates a child-parent relationship
- encapsulation is an OO concept which is used to hide the internal details of the class

## Overview
- a class can extend only one class , but implement multiple interfaces
- an interface can extend multiple interfaces

## The need for inheritance
- with inheritance, we can create a class with basic features and behaviours, and then create specialised versions that
inherit from this base class

## Inheritance with classes
### Extending a class
- a class can inherit from another class, and then define additional members:
```
public class Car {
    int wheels;
    String model;
    void start() {
        // Check essential parts
    }
}
```
- the ArmoredCar class inherits from Car:
```
public class ArmoredCar extends Car {
    int bulletProofWindows;
    void remoteStartCar() {
    // this vehicle can be started by using a remote control
    }
}
```
- classes in Java do not support multiple inheritance, although all classes implicitly inherit from java.lang.Object

### What is inherited?
- a subclass inherits the non-static public and protected members from the super class.
- members with default and package access are inherited if the 2 classes are in the same package
- private and static members of a super class are not inherited by a subclass.
- if the super class and subclass are in different packages, members with default or package access aren't inherited

### Accessing parent class members from a subclass
- you can just use them:
```
public class ArmoredCar extends Car {
    public String registerModel() {
        return model;
    }
}
```

### Hidden super class instance members
- if both super class and subclass have a variable or method with the same name, we can just use the *this* and *super*
keywords
```
public class ArmoredCar extends Car {
    private String model;
    public String getAValue() {
        return super.model;   // returns value of model defined in base class Car
        // return this.model;   // will return value of model defined in ArmoredCar
        // return model;   // will return value of model defined in ArmoredCar
    }
}
```

### Hidden base class static members
- can we access a static member of a base class from within the subclass:
```
public class Car {
    public static String msg() {
        return "Car";
    }
}

public class ArmoredCar extends Car {
    public static String msg() {
        return super.msg(); // this won't compile.
    }
}
```
- so, we can't
- static members are assigned to classes, not instances. We can fix the above by doing this:
```
return Car.msg();
```
- if both the super- and sub-class have the same static method, we can get some interesting behaviour:
```
public class Car {
    public static String msg() {
        return "Car";
    }
}

public class ArmoredCar extends Car {
    public static String msg() {
        return "ArmoredCar";
    }
}

Car first = new ArmoredCar();
ArmoredCar second = new ArmoredCar();
```
- first.msg() will print out "Car", while second.msg() will output "ArmoredCar"

## Inheritance with interfaces
### Implementing multiple interfaces
- classes can inherit only one class, but they can implement multiple interfaces
- in our example, a Car subclass needs to have flying and floating capabilities
```
public interface Floatable {
    void floatOnWater();
}

public interface Flyable {
    void fly();
}

public class ArmoredCar extends Car implements Floatable, Flyable {
    public void floatOnWater() {
        System.out.println("I can float!");
    }
  
    public void fly() {
        System.out.println("I can fly!");
    }
}
```

### Issues with multiple inheritance
- multiple inheritance with interfaces is permitted in Java
- with Java 8, interfaces could start defining default implementations for its methods
    - if a class implements multiple interfaces, which define methods with the same signature, the child class
    would inherit separate implementations
    - This is not permitted:
```
public interface Floatable {
    default void repair() {
        System.out.println("Repairing Floatable object");   
    }
}

public interface Flyable {
    default void repair() {
        System.out.println("Repairing Flyable object"); 
    }
}

public class ArmoredCar extends Car implements Floatable, Flyable {
    // this won't compile
}
```

- if we want to implement both interfaces, we'll have to override the *repair()* method
- if the interfaces in the preceding examples define variables with the same name, we can't access them without preceding
the variable name with the interface name
```
public interface Floatable {
    int duration = 10;
}

public interface Flyable {
    int duration = 20;
}

public class ArmoredCar extends Car implements Floatable, Flyable {
  
    public void aMethod() {
        System.out.println(duration); // won't compile
        System.out.println(Floatable.duration); // outputs 10
        System.out.println(Flyable.duration); // outputs 20
    }
}
```

### Interfaces extending other interfaces
- an interface can inherit from multiple interfaces:
```
public interface Floatable {
    void floatOnWater();
}


interface interface Flyable {
    void fly();
}

public interface SpaceTraveller extends Floatable, Flyable {
    void remoteControl();
}
```

## Inheriting type
- when a class inherits from a class or interface, it also inherits that class or interface's type
- allows developers to code to an interface, rather than an implementation
- imagine a situation where an organization keeps a list of cars owned by its employees
    - all employess might have different car models
    - how do we refer to different car instances
```
public class Employee {
    private String name;
    private Car car;
     
    // standard constructor
}

Employee e1 = new Employee("Shreya", new ArmoredCar());
Employee e2 = new Employee("Paul", new SpaceCar());
Employee e3 = new Employee("Pavni", new BMW());
```