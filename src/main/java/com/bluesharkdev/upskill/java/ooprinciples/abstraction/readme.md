# Abstraction
## Definition
- the process of hiding internal details of an application from a user
- used to create a boundary between application and client programs
- real life example: 
    - a car: start the car by turning a key. You have no idea how the engine is getting started. The internal 
    implementation and complex logic is hidden.

## Abstraction in OOP
- objects are the building blocks in OOP
- objects contain properties and methods
- can hide these methods and properties using access modifiers
- provide access only for required functions and properties
- this is the general procedure to implement abstraction

## Different abstraction types

### Data abstraction
- when an object's data is not visible to the outside world, this is known as data abstraction
- access to the data is provided via methods
- marking an instance variable private and providing getter and setter methods for it.

### Process abstraction
- when we hide the internal implementation of different functions in a user operation, it creates process abstraction
- in our car example:
    - user would just see *turn_on()*
    - behind the scenes, we have *fill_fuel()*, *create_spark()* and *change_piston_speed()*

## Abstraction in Java
- implemented through interfaces and abstract classes
-   
