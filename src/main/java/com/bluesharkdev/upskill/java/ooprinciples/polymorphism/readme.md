# Polymorphism
## Definition
- ability of an object to take multiple forms
- most common use-case of polymorphism is using a parent class to reference a subclass
- an example:
```
public interface Vegetarian{}
public class Animal{}
public class Deer extends Animal implements Vegetarian{}
```
- here, Deer is considered polymorphic since it is a Vegetarian and an Animal
- Java employs the subtyping version of polymorphism.