package com.bluesharkdev.upskill.java.ooprinciples.abstraction.client;

import com.bluesharkdev.upskill.java.ooprinciples.abstraction.implementation.AutomaticCar;
import com.bluesharkdev.upskill.java.ooprinciples.abstraction.implementation.Car;
import com.bluesharkdev.upskill.java.ooprinciples.abstraction.implementation.ManualCar;

public class Client {
    public static void main(String... args) {
        Car car1 = new ManualCar();
        Car car2 = new AutomaticCar();

        car1.turnOnCar();
        car1.turnOffCar();
        System.out.println(car1.getCarType());

        car2.turnOnCar();
        car2.turnOffCar();
        System.out.println(car2.getCarType());
    }
}
