# Encapsulation
## Definition
- the process of combining data and functions into a single unit; the class
- it is also the restriction of access to the object's components

## Encapsulation vs Inheritance
- authors of *Design Patterns* state there is tension between encapsulation and inheritance
- inheritance breaks encapsulation because inheritance exposes subclasses to the details of its parent's implementation

## What's the difference between encapsulation and abstraction
- Encapsulation makes the concept of data hiding possible
- Abstraction: process where you show only *relevant* data and *hide* unnecessary details of an object from the user
- Encapsulation: process of combining data and functions into a single unit
    - from here, you can abstract away the "unnecessary" details.