package com.bluesharkdev.upskill.java.ooprinciples.abstraction.implementation;

public interface Car {

    void turnOnCar();

    void turnOffCar();

    String getCarType();

}
